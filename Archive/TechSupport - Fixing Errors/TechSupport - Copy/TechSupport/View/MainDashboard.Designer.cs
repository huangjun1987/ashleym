﻿namespace TechSupport.View
{
    partial class MainDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMainForm = new System.Windows.Forms.TabControl();
            this.Display_Open_Incidents = new System.Windows.Forms.TabPage();
            this.addIncidentTab = new System.Windows.Forms.TabPage();
            this.Update = new System.Windows.Forms.TabPage();
            this.View_Incidents_By_Technician = new System.Windows.Forms.TabPage();
            this.Report_Tab = new System.Windows.Forms.TabPage();
            this.userNameLabelMainDash = new System.Windows.Forms.Label();
            this.Logout = new System.Windows.Forms.LinkLabel();
            this.displayOpenIncidentUserControl1 = new TechSupport.UserControls.DisplayOpenIncidentUserControl();
            this.addIncidentUserControl1 = new TechSupport.UserControls.AddIncidentUserControl();
            this.updateControl1 = new TechSupport.UserControls.UpdateControl();
            this.viewIncidentsByTechnician1 = new TechSupport.UserControls.ViewIncidentsByTechnician();
            this.reportUserControl1 = new TechSupport.UserControls.ReportUserControl();
            this.tabControlMainForm.SuspendLayout();
            this.Display_Open_Incidents.SuspendLayout();
            this.addIncidentTab.SuspendLayout();
            this.Update.SuspendLayout();
            this.View_Incidents_By_Technician.SuspendLayout();
            this.Report_Tab.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlMainForm
            // 
            this.tabControlMainForm.Controls.Add(this.Display_Open_Incidents);
            this.tabControlMainForm.Controls.Add(this.addIncidentTab);
            this.tabControlMainForm.Controls.Add(this.Update);
            this.tabControlMainForm.Controls.Add(this.View_Incidents_By_Technician);
            this.tabControlMainForm.Controls.Add(this.Report_Tab);
            this.tabControlMainForm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControlMainForm.Location = new System.Drawing.Point(0, 60);
            this.tabControlMainForm.Name = "tabControlMainForm";
            this.tabControlMainForm.SelectedIndex = 0;
            this.tabControlMainForm.Size = new System.Drawing.Size(1196, 483);
            this.tabControlMainForm.TabIndex = 2;
            this.tabControlMainForm.SelectedIndexChanged += new System.EventHandler(this.tabControlMainForm_SelectedIndexChanged);
            // 
            // Display_Open_Incidents
            // 
            this.Display_Open_Incidents.Controls.Add(this.displayOpenIncidentUserControl1);
            this.Display_Open_Incidents.Location = new System.Drawing.Point(4, 25);
            this.Display_Open_Incidents.Name = "Display_Open_Incidents";
            this.Display_Open_Incidents.Padding = new System.Windows.Forms.Padding(3);
            this.Display_Open_Incidents.Size = new System.Drawing.Size(1188, 454);
            this.Display_Open_Incidents.TabIndex = 3;
            this.Display_Open_Incidents.Text = "Display Open Incidents";
            this.Display_Open_Incidents.UseVisualStyleBackColor = true;
            // 
            // addIncidentTab
            // 
            this.addIncidentTab.Controls.Add(this.addIncidentUserControl1);
            this.addIncidentTab.Location = new System.Drawing.Point(4, 25);
            this.addIncidentTab.Name = "addIncidentTab";
            this.addIncidentTab.Padding = new System.Windows.Forms.Padding(3);
            this.addIncidentTab.Size = new System.Drawing.Size(1188, 454);
            this.addIncidentTab.TabIndex = 0;
            this.addIncidentTab.Text = "Add Incident";
            this.addIncidentTab.UseVisualStyleBackColor = true;
            // 
            // Update
            // 
            this.Update.Controls.Add(this.updateControl1);
            this.Update.Location = new System.Drawing.Point(4, 25);
            this.Update.Name = "Update";
            this.Update.Padding = new System.Windows.Forms.Padding(3);
            this.Update.Size = new System.Drawing.Size(1188, 454);
            this.Update.TabIndex = 4;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = true;
            // 
            // View_Incidents_By_Technician
            // 
            this.View_Incidents_By_Technician.Controls.Add(this.viewIncidentsByTechnician1);
            this.View_Incidents_By_Technician.Location = new System.Drawing.Point(4, 25);
            this.View_Incidents_By_Technician.Name = "View_Incidents_By_Technician";
            this.View_Incidents_By_Technician.Padding = new System.Windows.Forms.Padding(3);
            this.View_Incidents_By_Technician.Size = new System.Drawing.Size(1188, 454);
            this.View_Incidents_By_Technician.TabIndex = 5;
            this.View_Incidents_By_Technician.Text = "View Incidents by Technician";
            this.View_Incidents_By_Technician.UseVisualStyleBackColor = true;
            // 
            // Report_Tab
            // 
            this.Report_Tab.Controls.Add(this.reportUserControl1);
            this.Report_Tab.Location = new System.Drawing.Point(4, 25);
            this.Report_Tab.Name = "Report_Tab";
            this.Report_Tab.Padding = new System.Windows.Forms.Padding(3);
            this.Report_Tab.Size = new System.Drawing.Size(1188, 454);
            this.Report_Tab.TabIndex = 6;
            this.Report_Tab.Text = "Report";
            this.Report_Tab.UseVisualStyleBackColor = true;
            // 
            // userNameLabelMainDash
            // 
            this.userNameLabelMainDash.AutoSize = true;
            this.userNameLabelMainDash.Location = new System.Drawing.Point(693, 21);
            this.userNameLabelMainDash.Name = "userNameLabelMainDash";
            this.userNameLabelMainDash.Size = new System.Drawing.Size(0, 17);
            this.userNameLabelMainDash.TabIndex = 0;
            // 
            // Logout
            // 
            this.Logout.AutoSize = true;
            this.Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Logout.Location = new System.Drawing.Point(1069, 32);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(72, 25);
            this.Logout.TabIndex = 1;
            this.Logout.TabStop = true;
            this.Logout.Text = "Logout";
            this.Logout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Logout_LinkClicked);
            // 
            // displayOpenIncidentUserControl1
            // 
            this.displayOpenIncidentUserControl1.Location = new System.Drawing.Point(0, 4);
            this.displayOpenIncidentUserControl1.Name = "displayOpenIncidentUserControl1";
            this.displayOpenIncidentUserControl1.Size = new System.Drawing.Size(1185, 385);
            this.displayOpenIncidentUserControl1.TabIndex = 0;
            // 
            // addIncidentUserControl1
            // 
            this.addIncidentUserControl1.Location = new System.Drawing.Point(22, 27);
            this.addIncidentUserControl1.Name = "addIncidentUserControl1";
            this.addIncidentUserControl1.Size = new System.Drawing.Size(751, 307);
            this.addIncidentUserControl1.TabIndex = 0;
            // 
            // updateControl1
            // 
            this.updateControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateControl1.Location = new System.Drawing.Point(3, 3);
            this.updateControl1.Name = "updateControl1";
            this.updateControl1.Size = new System.Drawing.Size(1182, 448);
            this.updateControl1.TabIndex = 0;
            // 
            // viewIncidentsByTechnician1
            // 
            this.viewIncidentsByTechnician1.Location = new System.Drawing.Point(26, 20);
            this.viewIncidentsByTechnician1.Name = "viewIncidentsByTechnician1";
            this.viewIncidentsByTechnician1.Size = new System.Drawing.Size(1154, 428);
            this.viewIncidentsByTechnician1.TabIndex = 0;
            // 
            // reportUserControl1
            // 
            this.reportUserControl1.Location = new System.Drawing.Point(8, 6);
            this.reportUserControl1.Name = "reportUserControl1";
            this.reportUserControl1.Size = new System.Drawing.Size(1156, 442);
            this.reportUserControl1.TabIndex = 0;
            // 
            // MainDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 543);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.userNameLabelMainDash);
            this.Controls.Add(this.tabControlMainForm);
            this.Name = "MainDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Dashboard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDashboard_FormClosing);
            this.Load += new System.EventHandler(this.MainDashboard_Load);
            this.tabControlMainForm.ResumeLayout(false);
            this.Display_Open_Incidents.ResumeLayout(false);
            this.addIncidentTab.ResumeLayout(false);
            this.Update.ResumeLayout(false);
            this.View_Incidents_By_Technician.ResumeLayout(false);
            this.Report_Tab.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMainForm;
        private System.Windows.Forms.TabPage addIncidentTab;
        private System.Windows.Forms.Label userNameLabelMainDash;
        private System.Windows.Forms.LinkLabel Logout;
      
        private UserControls.AddIncidentUserControl addIncidentUserControl1;
        private System.Windows.Forms.TabPage Display_Open_Incidents;
        private UserControls.DisplayOpenIncidentUserControl displayOpenIncidentUserControl1;
        private System.Windows.Forms.TabPage Update;
        private UserControls.UpdateControl updateControl1;
        private System.Windows.Forms.TabPage View_Incidents_By_Technician;
        private UserControls.ViewIncidentsByTechnician viewIncidentsByTechnician1;
        private System.Windows.Forms.TabPage Report_Tab;
        private UserControls.ReportUserControl reportUserControl1;
    }
}