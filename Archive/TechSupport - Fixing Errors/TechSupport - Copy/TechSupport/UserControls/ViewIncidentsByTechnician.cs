﻿using System;
using System.Linq;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;

namespace TechSupport.UserControls
{
    /// <summary>
    /// This is the userControl class for viewing incidents by technician.
    /// </summary>
    public partial class ViewIncidentsByTechnician : UserControl
    {
        private readonly TechSupportController controller;

        public ViewIncidentsByTechnician()
        {
            InitializeComponent();
            this.controller = new TechSupportController();

        }

        /// <summary>
        /// This method gets technicians with incidents.
        /// </summary>
        public void GetTechniciansWithIncidents()
        {

            try
            {


                var technicianList = controller.GetTechniciansWithIncidents();
                technicianBindingSource.DataSource = technicianList;
                emailTextBox.ReadOnly = true;
                phoneTextBox.ReadOnly = true;

                if (technicianList.Count > 0)
                {

                    var technician = technicianList.First();
                    nameComboBox.SelectedItem = technician;
                    LoadIncidentsByTechID(technician.TechID);
                    emailTextBox.Text = technician.Email;
                    phoneTextBox.Text = technician.Phone;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show($"failed to get technicians with open incidents{ex.Message}");
            }
        }

        private void nameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedTechnician = (Technician)nameComboBox.SelectedItem;
                if (selectedTechnician != null)
                {
                    emailTextBox.Text = selectedTechnician.Email;
                    phoneTextBox.Text = selectedTechnician.Phone;
                }
                LoadIncidentsByTechID(selectedTechnician.TechID);

            }
            catch (Exception)
            {

                MessageBox.Show("select a technician");

            }
        }

        private void LoadIncidentsByTechID(int techID)
        {
            var incidents = this.controller.GetIncidentsByTechID(techID);
            openIncidentByTechnicianBindingSource.Clear();
            openIncidentByTechnicianBindingSource.DataSource = incidents;
        }

    }
}
