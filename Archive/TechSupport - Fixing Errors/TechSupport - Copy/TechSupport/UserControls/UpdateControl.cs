﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;

namespace TechSupport.UserControls
{
    /// <summary>
    /// This user control deals with updating an incident
    /// </summary>
    public partial class UpdateControl : UserControl
    {
        private readonly TechSupportController controller;

        /// public Update update;
        public Update oldUpdate;

        public UpdateControl()
        {
            InitializeComponent();
            this.controller = new TechSupportController();
            this.DisplayUpdateIncidentTab();
            this.oldUpdate = new Update();
        }

        /// <summary>
        /// This method displays the update incident tab
        /// </summary>
        public void DisplayUpdateIncidentTab()
        {
            incidentIDTextBox.Text = "";
            customerTextBox.Text = "";
            productTextBox.Text = "";
            titleTextBox.Text = "";
            dateOpenedTextBox.Text = "";
            descriptionRichTextBox.Text = "";
            technicianComboBox.DataSource = null;
            UpdateButton.Enabled = false;
            CloseButton.Enabled = false;
            textToAddRichTextBox.Enabled = false;
            customerTextBox.ReadOnly = true;
            productTextBox.ReadOnly = true;
            titleTextBox.ReadOnly = true;
            dateOpenedTextBox.ReadOnly = true;
            descriptionRichTextBox.ReadOnly = true;
        }


        private void UpdateButton_Click(object sender, EventArgs e)
        {

            try
            {
                var selectedTechnician = (Technician)technicianComboBox.SelectedItem;

                if (selectedTechnician.Name == oldUpdate.Technician && string.IsNullOrWhiteSpace(textToAddRichTextBox.Text))
                {
                    MessageBox.Show("Please change Technician or update Text To Add");
                    return;
                }
                int? techID = selectedTechnician.TechID==0 ? (int?)null : selectedTechnician.TechID;
                UpdateIncident(int.Parse(incidentIDTextBox.Text), techID, descriptionRichTextBox.Text, textToAddRichTextBox.Text, null, oldUpdate);

                GetButton_Click(sender, e);
                MessageBox.Show("Update Successful");
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to update incident. You must select a technician.");
            }
        }

        ///private void UpdateIncident(int incidentID, int? techID, string description, string textToAdd, DateTime? closeDate)
        private void UpdateIncident(int incidentID, int? techID, string description, string textToAdd, DateTime? closeDate, Update oldUpdate)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(textToAdd))
                {
                    description += $"{Environment.NewLine}<{DateTime.Now.Month}/{DateTime.Now.Day}/{DateTime.Now.Year}> {textToAdd}";
                }
                if (description.Length > 200)
                {
                    var result = MessageBox.Show("Message is too long and will be truncated", "Warning", MessageBoxButtons.OKCancel);
                    if (result == DialogResult.Cancel)
                    {
                        return;
                    }
                    else
                    {
                        description = description.Substring(0, 200);
                    }
                }
                textToAddRichTextBox.Text = "";

                var res = controller.UpdateIncident(incidentID, techID, description, textToAdd, closeDate, oldUpdate) ;
                if (!res)
                {
                    MessageBox.Show("This incident has been update by someone else. Please click Get again before update.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to update incident.");
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(technicianComboBox.SelectedValue.ToString()))
            {
                MessageBox.Show("Please assign a Technician before closing the incident.");
                return;
            }
            var result = MessageBox.Show("Are you sure you want to close the incident? Closed incident can no longer be edited.", "Warning", MessageBoxButtons.OKCancel);
            if (result == DialogResult.Cancel)
            {
                return;
            }
            UpdateIncident(int.Parse(incidentIDTextBox.Text), int.Parse(technicianComboBox.SelectedValue.ToString()), descriptionRichTextBox.Text, textToAddRichTextBox.Text, DateTime.Now, oldUpdate);
            GetButton_Click(sender, e);

        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            DisplayUpdateIncidentTab();

        }

        private void GetButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!int.TryParse(incidentIDTextBox.Text, out int incidentId))
                {
                    MessageBox.Show("Please enter a valid Incident ID!");
                    return;
                }

                oldUpdate = this.controller.GetUpdateIncident(incidentId);
                if (oldUpdate == null)
                {
                    MessageBox.Show("Incident not found!");
                    return;
                }

                customerTextBox.Text = oldUpdate.Customer;
                productTextBox.Text = oldUpdate.ProductCode;
                titleTextBox.Text = oldUpdate.Title;
                descriptionRichTextBox.Text = oldUpdate.Description;
                dateOpenedTextBox.Text = oldUpdate.DateOpened;
                var technicians = controller.GetTechnicians();
                technicians.Add(new Technician());
                technicians = technicians.OrderBy(t => t.TechID).ToList();
                var selectedTechnicianIndex = GetSelectedTechnicianIndex(technicians, oldUpdate.Technician);
                technicianComboBox.DataSource = technicians;
                technicianComboBox.DisplayMember = "Name";
                technicianComboBox.ValueMember = "TechID";
                technicianComboBox.SelectedIndex = selectedTechnicianIndex;

                UpdateButton.Enabled = true;
                CloseButton.Enabled = true;
                textToAddRichTextBox.Enabled = true;

                if (oldUpdate.Description.Length == 200)
                {
                    MessageBox.Show("Description is already at maximum length, you cannot add any more text.");
                }
                if (!String.IsNullOrEmpty(oldUpdate.DateClosed))
                {
                    UpdateButton.Enabled = false;
                    CloseButton.Enabled = false;
                    technicianComboBox.Enabled = false;
                    MessageBox.Show("Incident is closed. You can not update it.");

                }
                else
                {
                    UpdateButton.Enabled = true;
                    CloseButton.Enabled = true;
                    technicianComboBox.Enabled = true;

                }


            }

            catch (FormatException)
            {
                MessageBox.Show("There is no incident associated with that ID",
                   "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private int GetSelectedTechnicianIndex(List<Technician> technicians, string technician)
        {
            for (int i = 0; i < technicians.Count; i++)
            {
                if (technicians[i].Name == technician)
                {
                    return i;
                }
            }
            return -1;
        }

    }
}
