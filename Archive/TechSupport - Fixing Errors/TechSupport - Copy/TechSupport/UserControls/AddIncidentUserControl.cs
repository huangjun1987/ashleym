﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;

namespace TechSupport.UserControls
{
    /// <summary>
    /// This is the user control for adding an incident
    /// </summary>
    public partial class AddIncidentUserControl : UserControl
    {
        private readonly TechSupportController controller;

        private List<Customer> customerList;
        private List<Product> productList;

        public OpenIncident incident;

        public AddIncidentUserControl()
        {
            this.InitializeComponent();
            this.controller = new TechSupportController();
        }

        private void AddIncidentUserControl_Load(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
            CustomerComboBox.SelectedIndex = 0;
            ProductComboBox.SelectedIndex = 0;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {

            try
            {

                if (!IsValidData())
                {
                    MessageBox.Show("The data is invalid");
                }


                else if (!IsRegistered())
                {
                    MessageBox.Show("There is no registration associated with the product");
                }

                else
                {

                    this.PutIncidentData();
                    MessageBox.Show("New Incident Created");

                }



            }
            catch (Exception)
            {
                MessageBox.Show("All information must be filled out");
            }

        }


        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.messageLabel.Text = "";
            this.titleTextBox.Text = "";
            this.DescriptionTextBox.Text = "";
            CustomerComboBox.SelectedIndex = 0;
            ProductComboBox.SelectedIndex = 0;
        }


        private void LoadComboBoxes()
        {
            try
            {

                customerList = this.controller.GetCustomers();
                CustomerComboBox.DataSource = customerList;
                CustomerComboBox.DisplayMember = "Name";
                CustomerComboBox.ValueMember = "CustomerID";

                productList = this.controller.GetProducts();
                ProductComboBox.DataSource = productList;
                ProductComboBox.DisplayMember = "Name";
                ProductComboBox.ValueMember = "ProductCode";
            }
            catch (Exception)
            {
                MessageBox.Show("There is an error in loading the data");
            }

        }

        private void PutIncidentData()
        {

            var title = this.titleTextBox.Text;
            var description = this.DescriptionTextBox.Text;
            var customerID = int.Parse(this.CustomerComboBox.SelectedValue.ToString());
            var productCode = ProductComboBox.SelectedValue.ToString();
            this.controller.Add(new Incident(title, description, customerID, productCode));
        }

        /// <summary>
        /// This method is to get if the customerID is registered
        /// </summary>
        /// <returns></returns>
        private bool IsRegistered()
        {
            return controller.IsRegistered(int.Parse(CustomerComboBox.SelectedValue.ToString()), ProductComboBox.SelectedValue.ToString().Trim());
        }


        /// <summary>
        /// This method checks for valid data.
        /// </summary>
        /// <returns></returns>
        private bool IsValidData()
        {
            if (Validator.IsPresent(titleTextBox) &&
                Validator.IsPresent(DescriptionTextBox) &&

                Validator.IsPresent(CustomerComboBox) &&
                Validator.IsPresent(ProductComboBox))
            {
                return true;
            }

            else
                return false;
        }
    }
}

