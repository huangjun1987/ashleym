﻿using System;

namespace TechSupport.Model
{
    /// <summary>
    /// Model class for Registration
    /// </summary>
    public class Registration
    {
        public String ProductCode { get; set; }
        public int CustomerID { get; set; }

    }
}
