﻿using System;

namespace TechSupport.Model
{
    /// <summary>
    /// This class is a model for open incident by technician
    /// </summary>
    public class IncidentByTechnician
    {
        public String Product { get; set; }
        public String DateOpened { get; set; }
        public String Customer { get; set; }
        public String Title { get; set; }

    }
}
