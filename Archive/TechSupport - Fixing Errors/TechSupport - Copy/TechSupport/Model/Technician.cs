﻿namespace TechSupport.Model
{
    /// <summary>
    /// This is a model class for technician.
    /// </summary>
    public class Technician
    {
        public int TechID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public string Phone { get; set; }
    }
}
