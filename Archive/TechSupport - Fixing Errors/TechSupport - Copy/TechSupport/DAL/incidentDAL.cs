﻿using TechSupport.Model;
using System;
using System.Collections.Generic;


namespace TechSupport.DAL
{
    /// <summary>
    /// This the DAL class that deals with incidents.
    /// </summary>
    public class IncidentDAL
    {

        private static List<Incident> incidents = new List<Incident>();


        /// <summary>
        /// This method gets a list of incidents.
        /// </summary>
        /// <returns>A list of incidents</returns>
        public List<Incident> GetIncidents()
        {
            return incidents;
        }

        /// <summary>
        /// This method gets a list of incidents by customerID
        /// </summary>
        /// <param name="customerID">an integer of the customer's ID</param>
        /// <returns>A list of customers whose ID matches the parameter</returns>
        public List<Incident> GetIncidentsByCustomerID(int customerID)
        {
            List<Incident> returnList = new List<Incident>();
            foreach (Incident incident in incidents)
            {
                if (incident.CustomerID == customerID)
                {
                    returnList.Add(incident);
                }
            }
            return returnList;
        }

        /// <summary>
        /// This method adds and incident to the list of incidents.
        /// </summary>
        /// <param name="incident">A incident object based on the Model of incident</param>
        public void Add(Incident incident)
        {

            if (incident == null)
            {
                throw new ArgumentNullException("Incident cannot be null");
            }

            incidents.Add(incident);

        }

    }
}
