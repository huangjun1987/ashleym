﻿using System.Collections.Generic;
using TechSupport.Model;
using System.Data.SqlClient;

namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database products.
    public class ProductDBAL
    {
        /// <summary>
        /// This method gets a list of products from the database.
        /// </summary>
        /// <returns>A list of products</returns>
        public List<Product> GetProducts()
        {
            List<Product> productList = new List<Product>();

            string selectStatement =
                "SELECT p.ProductCode, p.Name " +
                "FROM dbo.Products p ";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Product product = new Product();
                            product.Name = reader["Name"].ToString();
                            product.ProductCode = reader["ProductCode"].ToString();

                            productList.Add(product);
                        }
                    }
                }

            }
            return productList;

        }
    }
}


