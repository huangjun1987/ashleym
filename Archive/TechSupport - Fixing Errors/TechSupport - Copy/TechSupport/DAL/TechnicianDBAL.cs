﻿using System.Collections.Generic;
using TechSupport.Model;
using System.Data.SqlClient;

namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database products.
    public class TechnicianDBAL
    {
        /// <summary>
        /// This method gets a list of technicians from the database.
        /// </summary>
        /// <returns>A list of products</returns>
        public List<Technician> GetTechnicians()
        {

            List<Technician> technicianList = new List<Technician>();

            string selectStatement =
               "SELECT TechID, Name, Email, Phone " +
                "FROM dbo.Technicians ";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Technician t = new Technician();
                            t.TechID = int.Parse(reader["TechID"].ToString());
                            t.Name = reader["Name"].ToString();
                            t.Email = reader["Email"].ToString();
                            t.Phone = reader["Phone"].ToString();

                            technicianList.Add(t);
                        }
                    }
                }

            }
            return technicianList;
        }

        /// <summary>
        /// This method gets a list of technicians with incidents from the database.
        /// </summary>
        /// <returns></returns>
        public List<Technician> GetTechniciansWithIncidents()
        {

            List<Technician> technicianList = new List<Technician>();

            string selectStatement =
                @"SELECT TechID, Name, Email, Phone
                FROM dbo.Technicians
                WHERE EXISTS (SELECT 1 FROM Incidents WHERE Incidents.TechID = Technicians.TechID ) ";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Technician t = new Technician();
                            t.TechID = int.Parse(reader["TechID"].ToString());
                            t.Name = reader["Name"].ToString();
                            t.Email = reader["Email"].ToString();
                            t.Phone = reader["Phone"].ToString();

                            technicianList.Add(t);
                        }
                    }
                }

            }
            return technicianList;
        }

    }
}



