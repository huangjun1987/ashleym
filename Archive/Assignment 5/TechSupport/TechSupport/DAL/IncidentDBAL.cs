﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TechSupport.Model;

namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database incidents.
    public class IncidentDBAL
    {
        
        /// <summary>
        /// This method gets a list from the database of incidents that are open.
        /// </summary>
        /// <returns>A list of open incidents</returns>
        public List<IncidentDetail> GetOpenIncidents()
        {
            List<IncidentDetail> incidentList = new List<IncidentDetail>();

            string selectStatement =
                "SELECT i.ProductCode, Convert(varchar(10), i.DateOpened, 120) AS 'Date', c.Name AS 'Customer', t.Name AS 'Technician', i.Title, i.Description " +
                "FROM dbo.Incidents i " +
                "INNER JOIN dbo.Customers c ON c.CustomerID = i.CustomerID " +
                "LEFT JOIN dbo.Technicians t ON t.TechID = i.TechID " +
                "WHERE i.DateClosed IS NULL";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            IncidentDetail openIncident = new IncidentDetail();
                            openIncident.ProductCode = reader["ProductCode"].ToString();
                            openIncident.DateOpened = reader["Date"].ToString();
                            openIncident.Customer = reader["Customer"].ToString(); ;
                            openIncident.Technician = reader["Technician"].ToString();
                            openIncident.Title = reader["Title"].ToString();
                            openIncident.Description = reader["Description"].ToString();

                            incidentList.Add(openIncident);
                        }
                    }
                }

            }
            return incidentList;


        }

        public void AddIncident(Incident incident)
        {
            string insertStatement =
                @"INSERT INTO [dbo].[Incidents] ([CustomerID] ,[TechID] ,[ProductCode] 
,[DateOpened] ,[DateClosed] ,[Title],[Description]) VALUES(@param1,@param2,@param3, @param4, @param5, @param6, @param7)";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(insertStatement, connection))
                {
                    cmd.Parameters.AddWithValue("@param1", incident.CustomerID);
                    cmd.Parameters.AddWithValue("@param2", DBNull.Value);
                    cmd.Parameters.AddWithValue("@param3", incident.ProductCode);
                    cmd.Parameters.AddWithValue("@param4", DateTime.Now);
                    cmd.Parameters.AddWithValue("@param5", DBNull.Value);
                    cmd.Parameters.AddWithValue("@param6", incident.Title);
                    cmd.Parameters.AddWithValue("@param7", incident.Description);

                    cmd.ExecuteNonQuery();

                    connection.Close();
                }

            }


        }
        internal IncidentDetail GetIncidentDetailByIncidentID(int incidentId)
        {
            IncidentDetail openIncident = null;
            string selectStatement =
            @"SELECT i.ID, i.ProductCode, Convert(varchar(10), i.DateOpened, 120) AS 'Date', c.Name AS 'Customer', t.Name AS 'Technician', i.Title, i.Description, i.DateClosed
                FROM dbo.Incidents i 
                INNER JOIN dbo.Customers c ON c.CustomerID = i.CustomerID 
                LEFT JOIN dbo.Technicians t ON t.TechID = i.TechID 
                WHERE  i.Id = @param1";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(selectStatement, connection))
                {
                    cmd.Parameters.AddWithValue("@param1", incidentId);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            openIncident = new IncidentDetail();
                            openIncident.IncidentID = int.Parse(reader["Id"].ToString());
                            openIncident.ProductCode = reader["ProductCode"].ToString();
                            openIncident.DateOpened = reader["Date"].ToString();
                            openIncident.Customer = reader["Customer"].ToString();
                            openIncident.Technician = reader["Technician"].ToString();
                            openIncident.Title = reader["Title"].ToString();
                            openIncident.Description = reader["Description"].ToString();
                            var dateClosed = reader["DateClosed"].ToString();
                            if (!string.IsNullOrWhiteSpace(dateClosed))
                            {
                                openIncident.DateClosed = DateTime.Parse(dateClosed);
                            }
                        }
                    }
                }

            }
            return openIncident;

        }

        //internal Incident GetIncidentByIncidentID(int incidentId)
        //{
        //    Incident incident = null;
        //    string selectStatement =
        //    @"SELECT *
        //        FROM dbo.Incidents 
        //        WHERE  Id = @param1";

        //    using (SqlConnection connection = TechSupportDBConnection.GetConnection())
        //    {
        //        connection.Open();

        //        using (SqlCommand cmd = new SqlCommand(selectStatement, connection))
        //        {
        //            cmd.Parameters.AddWithValue("@param1", incidentId);

        //            using (SqlDataReader reader = cmd.ExecuteReader())
        //            {
        //                while (reader.Read())
        //                {
        //                    var title = reader["Title"].ToString();
        //                    var description = reader["Description"].ToString();
        //                    var customerID = int.Parse(reader["CustomerID"].ToString());
        //                    var productCode = reader["ProductCode"].ToString();
        //                    incident = new Incident(title, description, customerID, productCode);
        //                    var techID = reader["TechID"].ToString();
        //                    if (!string.IsNullOrWhiteSpace(techID))
        //                    {
        //                        incident.TechID = int.Parse(techID);
        //                    }
        //                    incident.DateOpened = DateTime.Parse(reader["DateOpened"].ToString());

        //                    var dateClosed = reader["DateClosed"].ToString();
        //                    if (!string.IsNullOrWhiteSpace(dateClosed))
        //                    {
        //                        incident.DateClosed = DateTime.Parse(dateClosed);
        //                    }
        //                }
        //            }
        //        }

        //    }
        //    return incident;

        //}
        public void UpdateIncident(int incidentID, int? techID, string description, DateTime? closeDate)
        {
            string statement =
               @"Update [dbo].[Incidents] 
SET [TechID] = @param2,
[Description]=@param3,
[DateClosed]=@param4
Where Id = @param1";

            
            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(statement, connection))
                {
                    cmd.Parameters.AddWithValue("@param1", incidentID);
                    if (techID == null)
                    {
                        cmd.Parameters.AddWithValue("@param2", DBNull.Value);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@param2", techID);
                    }
                    cmd.Parameters.AddWithValue("@param3", description);
                    if (closeDate == null)
                    {
                        cmd.Parameters.AddWithValue("@param4", DBNull.Value);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@param4", closeDate);
                    }

                    cmd.ExecuteNonQuery();

                    connection.Close();
                }

            }
        }
    }
}

