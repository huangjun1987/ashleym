﻿using System.Collections.Generic;
using TechSupport.Model;
using System.Data.SqlClient;
using System;

namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database products.
    public class TechnicianDBAL
    {
        /// <summary>
        /// This method gets a list from the database of Technicians.
        /// </summary>
        /// <returns>A list of open incidents</returns>
        public List<Technician> GetTechnicians()
        {
            List<Technician> technicianList = new List<Technician>();

            string selectStatement =
                "SELECT TechID, Name " +
                "FROM dbo.Technicians ";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Technician t = new Technician();
                            t.TechID = int.Parse(reader["TechID"].ToString());
                            t.Name = reader["Name"].ToString();

                            technicianList.Add(t);
                        }
                    }
                }

            }
            return technicianList;
        }


    }
}


