﻿using System.Collections.Generic;
using System.Data.SqlClient;
using TechSupport.Model;


namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database customers.
    public class CustomerDBAL
    {
        /// <summary>
        /// This method gets a list from the database of incidents that are open.
        /// </summary>
        /// <returns>A list of open incidents</returns>
        public List<Customer> GetCustomers()
        {
            List<Customer> customerList = new List<Customer>();

            string selectStatement =
                "SELECT c.CustomerID, c.Name " +
                "FROM dbo.Customers c ";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Customer customer = new Customer();
                            customer.CustomerID = int.Parse(reader["CustomerID"].ToString());

                            customer.Name = reader["Name"].ToString();

                            customerList.Add(customer);
                        }
                    }
                }

            }
            return customerList;


        }



    }
}
