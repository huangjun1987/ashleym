﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;

namespace TechSupport.View
{
    public partial class MainDashboard : Form
    {
        //The controller interacts with the view and updates the view if the model  changes
        //and updates the model if the view initiates any change.
        //It also interacts with DAL and deals with getting the data from the DAL and sending data to the DAL

        Form previousForm = null;
        private readonly TechSupportController controller;
        private IncidentDetail openIncident = null;
        /// <summary>
        /// Initializes the component, the previous form, the userName and the controller. 
        /// </summary>
        /// <param name="userName">string of the user's name</param>
        /// <param name="form">previous login form that is passed in as a parameter</param>
        public MainDashboard(String userName, Form form)
        {
            InitializeComponent();
            userNameLabelMainDash.Text = userName;
            this.previousForm = form;
            this.controller = new TechSupportController();
            //  this.RefreshDataGrid();

        }


        /*  public void RefreshDataGrid()
          {

              this.dataGridViewIncidents.DataSource = null;
              this.dataGridViewIncidents.DataSource = this.controller.GetIncidents();
          }*/

        private void MainDashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            previousForm.Close();
        }

        private void Logout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Visible = false;
            previousForm.Visible = true;
        }



        private void tabControlMainForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabPage current = (sender as TabControl).SelectedTab;
            if (current.Name.Equals("Display_Open_Incidents"))
            {
                displayOpenIncidentUserControl1.Display();
            }
            if (current.Name.Equals("tabPageUpdate"))
            {
                DisplayUpdateIncidentTab();
            }
        }

        public void DisplayUpdateIncidentTab()
        {
            textBoxIncidentID.Text = "";
            textBoxCustomer.Text = "";
            textBoxProduct.Text = "";
            textBoxTitle.Text = "";
            textBoxDateOpened.Text = "";
            textBoxDescription.Text = "";
            listBoxTechnician.DataSource = null;
            buttonUpdateIncident.Enabled = false;
            buttonCloseIncident.Enabled = false;
            textBoxTextToAdd.Enabled = false;
            textBoxCustomer.ReadOnly = true;
            textBoxProduct.ReadOnly = true;
            textBoxTitle.ReadOnly = true;
            textBoxDateOpened.ReadOnly = true;
            textBoxDescription.ReadOnly = true;
        }


        private void MainDashboard_Load(object sender, EventArgs e)
        {
            displayOpenIncidentUserControl1.Display();

        }

        private void buttonGetIncident_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(textBoxIncidentID.Text, out int incidentId))
            {
                MessageBox.Show("Please enter a valid Incident ID!");
                return;
            }
            openIncident = controller.GetIncidentDetailByIncidentID(incidentId);
            if (openIncident == null)
            {
                MessageBox.Show("Incident not found!");
                return;
            }
            textBoxCustomer.Text = openIncident.Customer;
            textBoxProduct.Text = openIncident.ProductCode;
            textBoxTitle.Text = openIncident.Title;
            textBoxDateOpened.Text = openIncident.DateOpened;
            textBoxDescription.Text = openIncident.Description;
            var technicians = controller.GetTechnicians();
            var selectedTechnicianIndex = GetSelectedTechnicianIndex(technicians, openIncident.Technician);

            listBoxTechnician.DataSource = technicians;
            listBoxTechnician.DisplayMember = "Name";
            listBoxTechnician.ValueMember = "TechID";
            if (selectedTechnicianIndex > 0)
            {
                listBoxTechnician.SelectedIndex = selectedTechnicianIndex;
            }
            buttonUpdateIncident.Enabled = true;
            buttonCloseIncident.Enabled = true;
            textBoxTextToAdd.Enabled = true;

            if (openIncident.Description.Length == 200)
            {
                MessageBox.Show("Description is already at maximum length, you cannot add any more text.");
            }
            if (openIncident.DateClosed != null)
            {
                buttonUpdateIncident.Enabled = false;
                listBoxTechnician.Enabled = false;
                MessageBox.Show("Incident is closed. You can not update it.");

            }
            else
            {
                buttonUpdateIncident.Enabled = true;
                listBoxTechnician.Enabled = true;

            }
        }

        private int GetSelectedTechnicianIndex(List<Technician> technicians, string technician)
        {
            for (int i = 0; i < technicians.Count; i++)
            {
                if (technicians[i].Name == technician)
                {
                    return i;
                }
            }
            return -1;
        }

        private void buttonUpdateIncident_Click(object sender, EventArgs e)
        {
            var selectedTechnician = (Technician)listBoxTechnician.SelectedItem;
            if (selectedTechnician.Name == openIncident.Technician && string.IsNullOrWhiteSpace(textBoxTextToAdd.Text))
            {
                MessageBox.Show("Please change Technician or update Text To Add");
                return;
            }
            var techIDStr = listBoxTechnician.SelectedValue.ToString();
            int? techID = string.IsNullOrWhiteSpace(techIDStr) ? (int?)null : int.Parse(techIDStr);
            UpdateIncident(int.Parse(textBoxIncidentID.Text), techID, textBoxDescription.Text, textBoxTextToAdd.Text, null);
            buttonGetIncident_Click(sender, e);
        }

        private void buttonCloseIncident_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(listBoxTechnician.SelectedValue.ToString()))
            {
                MessageBox.Show("Please assign a Technician before closing the incident.");
                return;
            }
            var result = MessageBox.Show("Are you sure you want to close the incident? Closed incident can no longer be edited.", "Warning", MessageBoxButtons.OKCancel);
            if (result == DialogResult.Cancel)
            {
                return;
            }
            UpdateIncident(int.Parse(textBoxIncidentID.Text), int.Parse(listBoxTechnician.SelectedValue.ToString()), textBoxDescription.Text, textBoxTextToAdd.Text, DateTime.Now);
            buttonGetIncident_Click(sender, e);
        }

        private void UpdateIncident(int incidentID, int? techID, string description, string textToAdd, DateTime? closeDate)
        {
            var openIncidentInDb = controller.GetIncidentDetailByIncidentID(incidentID);
            if (openIncident.Technician != openIncidentInDb.Technician || openIncident.Description != openIncidentInDb.Description || openIncident.DateClosed != openIncidentInDb.DateClosed)
            {
                MessageBox.Show("This incident has been update by someone else. Please click Get again before update.");
                return;
            }
            if (!string.IsNullOrWhiteSpace(textToAdd))
            {
                description += $"{Environment.NewLine}<{DateTime.Now.Month}/{DateTime.Now.Day}/{DateTime.Now.Year} {textToAdd}";
            }
            if (description.Length > 200)
            {
                var result = MessageBox.Show("Message is too long and will be truncated", "Warning", MessageBoxButtons.OKCancel);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    description = description.Substring(0, 200);
                }
            }
            textBoxTextToAdd.Text = "";
            controller.UpdateIncident(incidentID, techID, description, closeDate);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            DisplayUpdateIncidentTab();
        }
    }
}

