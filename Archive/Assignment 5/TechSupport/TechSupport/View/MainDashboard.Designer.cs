﻿namespace TechSupport.View
{
    partial class MainDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMainForm = new System.Windows.Forms.TabControl();
            this.addIncidentTab = new System.Windows.Forms.TabPage();
            this.loadAllIncidentsTab = new System.Windows.Forms.TabPage();
            this.dataGridViewIncidents = new System.Windows.Forms.DataGridView();
            this.searchIncidentsTab = new System.Windows.Forms.TabPage();
            this.Display_Open_Incidents = new System.Windows.Forms.TabPage();
            this.tabPageUpdate = new System.Windows.Forms.TabPage();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonCloseIncident = new System.Windows.Forms.Button();
            this.buttonUpdateIncident = new System.Windows.Forms.Button();
            this.textBoxTextToAdd = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxDateOpened = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.listBoxTechnician = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxProduct = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxCustomer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonGetIncident = new System.Windows.Forms.Button();
            this.textBoxIncidentID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.userNameLabelMainDash = new System.Windows.Forms.Label();
            this.Logout = new System.Windows.Forms.LinkLabel();
            this.addIncidentUserControl1 = new TechSupport.UserControls.AddIncidentUserControl();
            this.searchUserControl1 = new TechSupport.UserControls.SearchUserControl();
            this.displayOpenIncidentUserControl1 = new TechSupport.UserControls.DisplayOpenIncidentUserControl();
            this.tabControlMainForm.SuspendLayout();
            this.addIncidentTab.SuspendLayout();
            this.loadAllIncidentsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIncidents)).BeginInit();
            this.searchIncidentsTab.SuspendLayout();
            this.Display_Open_Incidents.SuspendLayout();
            this.tabPageUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlMainForm
            // 
            this.tabControlMainForm.Controls.Add(this.addIncidentTab);
            this.tabControlMainForm.Controls.Add(this.loadAllIncidentsTab);
            this.tabControlMainForm.Controls.Add(this.searchIncidentsTab);
            this.tabControlMainForm.Controls.Add(this.Display_Open_Incidents);
            this.tabControlMainForm.Controls.Add(this.tabPageUpdate);
            this.tabControlMainForm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControlMainForm.Location = new System.Drawing.Point(0, 41);
            this.tabControlMainForm.Margin = new System.Windows.Forms.Padding(6);
            this.tabControlMainForm.Name = "tabControlMainForm";
            this.tabControlMainForm.SelectedIndex = 0;
            this.tabControlMainForm.Size = new System.Drawing.Size(1948, 995);
            this.tabControlMainForm.TabIndex = 2;
            this.tabControlMainForm.SelectedIndexChanged += new System.EventHandler(this.tabControlMainForm_SelectedIndexChanged);
            // 
            // addIncidentTab
            // 
            this.addIncidentTab.Controls.Add(this.addIncidentUserControl1);
            this.addIncidentTab.Location = new System.Drawing.Point(10, 48);
            this.addIncidentTab.Margin = new System.Windows.Forms.Padding(6);
            this.addIncidentTab.Name = "addIncidentTab";
            this.addIncidentTab.Padding = new System.Windows.Forms.Padding(6);
            this.addIncidentTab.Size = new System.Drawing.Size(1928, 661);
            this.addIncidentTab.TabIndex = 0;
            this.addIncidentTab.Text = "Add Incident";
            this.addIncidentTab.UseVisualStyleBackColor = true;
            // 
            // loadAllIncidentsTab
            // 
            this.loadAllIncidentsTab.Controls.Add(this.dataGridViewIncidents);
            this.loadAllIncidentsTab.Location = new System.Drawing.Point(10, 48);
            this.loadAllIncidentsTab.Margin = new System.Windows.Forms.Padding(6);
            this.loadAllIncidentsTab.Name = "loadAllIncidentsTab";
            this.loadAllIncidentsTab.Padding = new System.Windows.Forms.Padding(6);
            this.loadAllIncidentsTab.Size = new System.Drawing.Size(1928, 661);
            this.loadAllIncidentsTab.TabIndex = 1;
            this.loadAllIncidentsTab.Text = "Load All Incidents";
            this.loadAllIncidentsTab.UseVisualStyleBackColor = true;
            // 
            // dataGridViewIncidents
            // 
            this.dataGridViewIncidents.AllowUserToAddRows = false;
            this.dataGridViewIncidents.AllowUserToDeleteRows = false;
            this.dataGridViewIncidents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIncidents.Location = new System.Drawing.Point(212, 99);
            this.dataGridViewIncidents.Margin = new System.Windows.Forms.Padding(6);
            this.dataGridViewIncidents.Name = "dataGridViewIncidents";
            this.dataGridViewIncidents.ReadOnly = true;
            this.dataGridViewIncidents.RowHeadersWidth = 51;
            this.dataGridViewIncidents.RowTemplate.Height = 24;
            this.dataGridViewIncidents.Size = new System.Drawing.Size(1160, 467);
            this.dataGridViewIncidents.TabIndex = 5;
            // 
            // searchIncidentsTab
            // 
            this.searchIncidentsTab.Controls.Add(this.searchUserControl1);
            this.searchIncidentsTab.Location = new System.Drawing.Point(10, 48);
            this.searchIncidentsTab.Margin = new System.Windows.Forms.Padding(6);
            this.searchIncidentsTab.Name = "searchIncidentsTab";
            this.searchIncidentsTab.Size = new System.Drawing.Size(1928, 661);
            this.searchIncidentsTab.TabIndex = 2;
            this.searchIncidentsTab.Text = "Search Incident";
            this.searchIncidentsTab.UseVisualStyleBackColor = true;
            // 
            // Display_Open_Incidents
            // 
            this.Display_Open_Incidents.Controls.Add(this.displayOpenIncidentUserControl1);
            this.Display_Open_Incidents.Location = new System.Drawing.Point(10, 48);
            this.Display_Open_Incidents.Margin = new System.Windows.Forms.Padding(6);
            this.Display_Open_Incidents.Name = "Display_Open_Incidents";
            this.Display_Open_Incidents.Padding = new System.Windows.Forms.Padding(6);
            this.Display_Open_Incidents.Size = new System.Drawing.Size(1928, 661);
            this.Display_Open_Incidents.TabIndex = 3;
            this.Display_Open_Incidents.Text = "Display Open Incidents";
            this.Display_Open_Incidents.UseVisualStyleBackColor = true;
            // 
            // tabPageUpdate
            // 
            this.tabPageUpdate.Controls.Add(this.buttonClear);
            this.tabPageUpdate.Controls.Add(this.buttonCloseIncident);
            this.tabPageUpdate.Controls.Add(this.buttonUpdateIncident);
            this.tabPageUpdate.Controls.Add(this.textBoxTextToAdd);
            this.tabPageUpdate.Controls.Add(this.label8);
            this.tabPageUpdate.Controls.Add(this.textBoxDescription);
            this.tabPageUpdate.Controls.Add(this.label7);
            this.tabPageUpdate.Controls.Add(this.textBoxDateOpened);
            this.tabPageUpdate.Controls.Add(this.label6);
            this.tabPageUpdate.Controls.Add(this.textBoxTitle);
            this.tabPageUpdate.Controls.Add(this.label5);
            this.tabPageUpdate.Controls.Add(this.listBoxTechnician);
            this.tabPageUpdate.Controls.Add(this.label4);
            this.tabPageUpdate.Controls.Add(this.textBoxProduct);
            this.tabPageUpdate.Controls.Add(this.label3);
            this.tabPageUpdate.Controls.Add(this.textBoxCustomer);
            this.tabPageUpdate.Controls.Add(this.label2);
            this.tabPageUpdate.Controls.Add(this.buttonGetIncident);
            this.tabPageUpdate.Controls.Add(this.textBoxIncidentID);
            this.tabPageUpdate.Controls.Add(this.label1);
            this.tabPageUpdate.Location = new System.Drawing.Point(10, 48);
            this.tabPageUpdate.Name = "tabPageUpdate";
            this.tabPageUpdate.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUpdate.Size = new System.Drawing.Size(1928, 937);
            this.tabPageUpdate.TabIndex = 4;
            this.tabPageUpdate.Text = "Update";
            this.tabPageUpdate.UseVisualStyleBackColor = true;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(571, 832);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(133, 60);
            this.buttonClear.TabIndex = 19;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonCloseIncident
            // 
            this.buttonCloseIncident.Location = new System.Drawing.Point(319, 832);
            this.buttonCloseIncident.Name = "buttonCloseIncident";
            this.buttonCloseIncident.Size = new System.Drawing.Size(133, 60);
            this.buttonCloseIncident.TabIndex = 18;
            this.buttonCloseIncident.Text = "Close";
            this.buttonCloseIncident.UseVisualStyleBackColor = true;
            this.buttonCloseIncident.Click += new System.EventHandler(this.buttonCloseIncident_Click);
            // 
            // buttonUpdateIncident
            // 
            this.buttonUpdateIncident.Location = new System.Drawing.Point(64, 832);
            this.buttonUpdateIncident.Name = "buttonUpdateIncident";
            this.buttonUpdateIncident.Size = new System.Drawing.Size(133, 60);
            this.buttonUpdateIncident.TabIndex = 17;
            this.buttonUpdateIncident.Text = "Update";
            this.buttonUpdateIncident.UseVisualStyleBackColor = true;
            this.buttonUpdateIncident.Click += new System.EventHandler(this.buttonUpdateIncident_Click);
            // 
            // textBoxTextToAdd
            // 
            this.textBoxTextToAdd.Location = new System.Drawing.Point(267, 638);
            this.textBoxTextToAdd.Multiline = true;
            this.textBoxTextToAdd.Name = "textBoxTextToAdd";
            this.textBoxTextToAdd.Size = new System.Drawing.Size(1218, 169);
            this.textBoxTextToAdd.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(58, 638);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(167, 32);
            this.label8.TabIndex = 15;
            this.label8.Text = "Text to Add:";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(267, 458);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(1218, 151);
            this.textBoxDescription.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(58, 458);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(166, 32);
            this.label7.TabIndex = 13;
            this.label7.Text = "Description:";
            // 
            // textBoxDateOpened
            // 
            this.textBoxDateOpened.Location = new System.Drawing.Point(267, 396);
            this.textBoxDateOpened.Name = "textBoxDateOpened";
            this.textBoxDateOpened.Size = new System.Drawing.Size(306, 38);
            this.textBoxDateOpened.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(58, 396);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(192, 32);
            this.label6.TabIndex = 11;
            this.label6.Text = "Date Opened:";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(261, 320);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(306, 38);
            this.textBoxTitle.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 320);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 32);
            this.label5.TabIndex = 9;
            this.label5.Text = "Title:";
            // 
            // listBoxTechnician
            // 
            this.listBoxTechnician.FormattingEnabled = true;
            this.listBoxTechnician.ItemHeight = 31;
            this.listBoxTechnician.Location = new System.Drawing.Point(267, 220);
            this.listBoxTechnician.Name = "listBoxTechnician";
            this.listBoxTechnician.Size = new System.Drawing.Size(306, 66);
            this.listBoxTechnician.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 32);
            this.label4.TabIndex = 7;
            this.label4.Text = "Technician:";
            // 
            // textBoxProduct
            // 
            this.textBoxProduct.Location = new System.Drawing.Point(267, 148);
            this.textBoxProduct.Name = "textBoxProduct";
            this.textBoxProduct.Size = new System.Drawing.Size(306, 38);
            this.textBoxProduct.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 32);
            this.label3.TabIndex = 5;
            this.label3.Text = "Product:";
            // 
            // textBoxCustomer
            // 
            this.textBoxCustomer.Location = new System.Drawing.Point(267, 83);
            this.textBoxCustomer.Name = "textBoxCustomer";
            this.textBoxCustomer.Size = new System.Drawing.Size(306, 38);
            this.textBoxCustomer.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "Customer:";
            // 
            // buttonGetIncident
            // 
            this.buttonGetIncident.Location = new System.Drawing.Point(440, 6);
            this.buttonGetIncident.Name = "buttonGetIncident";
            this.buttonGetIncident.Size = new System.Drawing.Size(133, 60);
            this.buttonGetIncident.TabIndex = 2;
            this.buttonGetIncident.Text = "Get";
            this.buttonGetIncident.UseVisualStyleBackColor = true;
            this.buttonGetIncident.Click += new System.EventHandler(this.buttonGetIncident_Click);
            // 
            // textBoxIncidentID
            // 
            this.textBoxIncidentID.Location = new System.Drawing.Point(267, 18);
            this.textBoxIncidentID.Name = "textBoxIncidentID";
            this.textBoxIncidentID.Size = new System.Drawing.Size(100, 38);
            this.textBoxIncidentID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Incident ID: ";
            // 
            // userNameLabelMainDash
            // 
            this.userNameLabelMainDash.AutoSize = true;
            this.userNameLabelMainDash.Location = new System.Drawing.Point(1386, 41);
            this.userNameLabelMainDash.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.userNameLabelMainDash.Name = "userNameLabelMainDash";
            this.userNameLabelMainDash.Size = new System.Drawing.Size(0, 32);
            this.userNameLabelMainDash.TabIndex = 0;
            // 
            // Logout
            // 
            this.Logout.AutoSize = true;
            this.Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Logout.Location = new System.Drawing.Point(1352, 50);
            this.Logout.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(143, 46);
            this.Logout.TabIndex = 1;
            this.Logout.TabStop = true;
            this.Logout.Text = "Logout";
            this.Logout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Logout_LinkClicked);
            // 
            // addIncidentUserControl1
            // 
            this.addIncidentUserControl1.Location = new System.Drawing.Point(44, 52);
            this.addIncidentUserControl1.Margin = new System.Windows.Forms.Padding(12);
            this.addIncidentUserControl1.Name = "addIncidentUserControl1";
            this.addIncidentUserControl1.Size = new System.Drawing.Size(1502, 595);
            this.addIncidentUserControl1.TabIndex = 0;
            // 
            // searchUserControl1
            // 
            this.searchUserControl1.Location = new System.Drawing.Point(16, 23);
            this.searchUserControl1.Margin = new System.Windows.Forms.Padding(12);
            this.searchUserControl1.Name = "searchUserControl1";
            this.searchUserControl1.Size = new System.Drawing.Size(1576, 668);
            this.searchUserControl1.TabIndex = 0;
            // 
            // displayOpenIncidentUserControl1
            // 
            this.displayOpenIncidentUserControl1.Location = new System.Drawing.Point(-8, 6);
            this.displayOpenIncidentUserControl1.Margin = new System.Windows.Forms.Padding(12);
            this.displayOpenIncidentUserControl1.Name = "displayOpenIncidentUserControl1";
            this.displayOpenIncidentUserControl1.Size = new System.Drawing.Size(1928, 665);
            this.displayOpenIncidentUserControl1.TabIndex = 0;
            // 
            // MainDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1948, 1036);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.userNameLabelMainDash);
            this.Controls.Add(this.tabControlMainForm);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "MainDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Dashboard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDashboard_FormClosing);
            this.Load += new System.EventHandler(this.MainDashboard_Load);
            this.tabControlMainForm.ResumeLayout(false);
            this.addIncidentTab.ResumeLayout(false);
            this.loadAllIncidentsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIncidents)).EndInit();
            this.searchIncidentsTab.ResumeLayout(false);
            this.Display_Open_Incidents.ResumeLayout(false);
            this.tabPageUpdate.ResumeLayout(false);
            this.tabPageUpdate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMainForm;
        private System.Windows.Forms.TabPage addIncidentTab;
        private System.Windows.Forms.TabPage loadAllIncidentsTab;
        private System.Windows.Forms.TabPage searchIncidentsTab;
        private System.Windows.Forms.Label userNameLabelMainDash;
        private System.Windows.Forms.LinkLabel Logout;
      
        private UserControls.AddIncidentUserControl addIncidentUserControl1;
        private System.Windows.Forms.DataGridView dataGridViewIncidents;
        private UserControls.SearchUserControl searchUserControl1;
        private System.Windows.Forms.TabPage Display_Open_Incidents;
        private UserControls.DisplayOpenIncidentUserControl displayOpenIncidentUserControl1;
        private System.Windows.Forms.TabPage tabPageUpdate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxIncidentID;
        private System.Windows.Forms.Button buttonGetIncident;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxProduct;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxCustomer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listBoxTechnician;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonCloseIncident;
        private System.Windows.Forms.Button buttonUpdateIncident;
        private System.Windows.Forms.TextBox textBoxTextToAdd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxDateOpened;
        private System.Windows.Forms.Label label6;
    }
}