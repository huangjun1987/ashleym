﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupport.Model
{
   public class Technician
    {
        public int TechID { get; set; }
        public string Name { get; set; }
    }
}
