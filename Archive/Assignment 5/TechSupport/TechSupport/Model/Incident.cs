﻿using System;


namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for an incident
    /// </summary>
    public class Incident
    {
        public string Title { get; }
        public string Description { get; }
        public int CustomerID { get; }
        public string ProductCode { get;  }
        public int? TechID { get; set; }
        public DateTime DateOpened { get; set; }
        public DateTime? DateClosed { get; set; }

        public Incident(string title, string description, int customerID, string productCode)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("Title cannot be null or empty", "title");

            }

            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentException("Description cannot be null or empty", "description");

            }

            if (customerID < 0)
            {
                throw new ArgumentOutOfRangeException("customerID", "CustomerID has to be greater than 0");
            }

            this.Title = title;
            this.Description = description;
            this.CustomerID = customerID;
            ProductCode = productCode;
        }
    }
}

