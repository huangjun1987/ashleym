﻿using System;

namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for an open incident.
    /// </summary>
    public class IncidentDetail
    {
        public int IncidentID { get; set; }
        public String ProductCode { get; set; }
        public String Title { get; set; }
        public String Customer { get; set; }

        public String DateOpened { get; set; }
        //public DateTime DateOpened { get; set; }
        public String Technician { get; set; }
        public String Description { get; set; }

        public DateTime? DateClosed { get; set; }

    }
}
