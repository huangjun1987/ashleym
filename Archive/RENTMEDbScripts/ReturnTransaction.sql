/****** Object:  Table [dbo].[ReturnTransaction]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReturnTransaction](
	[ReturnID] [int] IDENTITY(1000,1) NOT NULL,
	[ReturnDate] [date] NOT NULL,
	[FineDueTotal] [decimal](9, 2) NOT NULL,
	[RefundDueTotal] [decimal](9, 2) NOT NULL,
	[EmployeeID] [int] NOT NULL,
 CONSTRAINT [PK_ReturnTransaction] PRIMARY KEY CLUSTERED 
(
	[ReturnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ReturnTransaction]  WITH CHECK ADD  CONSTRAINT [FK_ReturnTransaction_Employees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ReturnTransaction] CHECK CONSTRAINT [FK_ReturnTransaction_Employees]
GO

SET IDENTITY_INSERT [dbo].[ReturnTransaction] ON 
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1000, CAST(N'2019-02-16' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1009)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1001, CAST(N'2020-01-05' AS Date), CAST(58.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1011)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1002, CAST(N'2020-02-20' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(200.00 AS Decimal(9, 2)), 1011)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1003, CAST(N'2020-01-14' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1012)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1004, CAST(N'2020-01-16' AS Date), CAST(24.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1011)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1005, CAST(N'2020-01-07' AS Date), CAST(70.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1012)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1006, CAST(N'2019-08-10' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(560.00 AS Decimal(9, 2)), 1013)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1007, CAST(N'2019-08-18' AS Date), CAST(5.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1013)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1008, CAST(N'2020-03-01' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1000)
SET IDENTITY_INSERT [dbo].[ReturnTransaction] OFF