/****** Object:  Table [dbo].[RentedItem]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RentedItem](
	[Serial#] [varchar](50) NOT NULL,
	[RentalID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_RentedItem] PRIMARY KEY CLUSTERED 
(
	[Serial#] ASC,
	[RentalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RentedItem]  WITH CHECK ADD  CONSTRAINT [FK_RentedItem_FurnitureItem] FOREIGN KEY([Serial#])
REFERENCES [dbo].[FurnitureItem] ([Serial#])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RentedItem] CHECK CONSTRAINT [FK_RentedItem_FurnitureItem]
GO
ALTER TABLE [dbo].[RentedItem]  WITH CHECK ADD  CONSTRAINT [FK_RentedItem_RentalTransaction] FOREIGN KEY([RentalID])
REFERENCES [dbo].[RentalTransaction] ([RentalID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RentedItem] CHECK CONSTRAINT [FK_RentedItem_RentalTransaction]
GO

INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Bcygec1d', 1000, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Bgeilv5c', 1001, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Bkjkjc2v', 1002, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BkscGy5d', 1002, 3)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BkshEr3q', 1003, 4)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BksVks3m', 1004, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BsvIL7dk', 1000, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BsvIL7dk', 1007, 3)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chckee3d', 1000, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'ChDdjk7c', 1008, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'ChDdjk7c', 1009, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'ChDdjk7c', 1010, 8)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chdkjk7k', 1011, 6)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chdokd4r', 1013, 8)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chrgkk42', 1015, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chrkjk3v', 1007, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chrklk5m', 1016, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chwbkj8d', 1014, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Colebv6c', 1020, 3)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Covgkb1x', 1019, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'CTglsv2b', 1019, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Dbkljj3ld', 1008, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Dcdslf9d', 1010, 8)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'DcksfY1g', 1009, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Drwgye6s', 1012, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Ecblcd0w', 1017, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'HtchCD2e', 1018, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'TmcUrc9z', 1013, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'ToUtgy7c', 1011, 1)