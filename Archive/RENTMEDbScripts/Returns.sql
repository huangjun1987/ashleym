/****** Object:  Table [dbo].[Returns]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Returns](
	[Serial#] [varchar](50) NOT NULL,
	[RentalID] [int] NOT NULL,
	[ReturnID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[FineDue] [decimal](9, 2) NOT NULL,
	[RefundDue] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_Returns] PRIMARY KEY CLUSTERED 
(
	[Serial#] ASC,
	[RentalID] ASC,
	[ReturnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Returns]  WITH CHECK ADD  CONSTRAINT [FK_Returns_RentedItem] FOREIGN KEY([Serial#], [RentalID])
REFERENCES [dbo].[RentedItem] ([Serial#], [RentalID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Returns] CHECK CONSTRAINT [FK_Returns_RentedItem]
GO
ALTER TABLE [dbo].[Returns]  WITH CHECK ADD  CONSTRAINT [FK_Returns_ReturnTransaction] FOREIGN KEY([ReturnID])
REFERENCES [dbo].[ReturnTransaction] ([ReturnID])
GO
ALTER TABLE [dbo].[Returns] CHECK CONSTRAINT [FK_Returns_ReturnTransaction]
GO


INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Bgeilv5c', 1001, 1000, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Bkjkjc2v', 1002, 1001, 1, CAST(16.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'BkscGy5d', 1002, 1001, 3, CAST(42.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'BkshEr3q', 1003, 1002, 4, CAST(0.00 AS Decimal(9, 2)), CAST(200.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Chdkjk7k', 1011, 1004, 6, CAST(24.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Chdokd4r', 1013, 1006, 8, CAST(0.00 AS Decimal(9, 2)), CAST(560.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Chwbkj8d', 1014, 1008, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Drwgye6s', 1012, 1005, 2, CAST(70.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'TmcUrc9z', 1013, 1007, 1, CAST(5.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'ToUtgy7c', 1011, 1003, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
