USE [RentMe]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (1, N'desk')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (2, N'chair')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (3, N'table')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (4, N'dresser')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (5, N'bed')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (6, N'couch')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (7, N'entertainment center')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (8, N'bookshelf')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (9, N'hutch')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (10, N'coffee table')
