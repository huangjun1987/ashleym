USE [RentMe]
GO

/****** Object:  Table [dbo].[Employees]    Script Date: 4/5/2020 4:48:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employees](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[FName] [nchar](10) NULL,
	[LName] [nchar](20) NULL,
	[Sex] [nchar](10) NULL,
	[DOB] [datetime] NULL,
	[Phone] [nchar](12) NULL,
	[Address1] [nchar](100) NULL,
	[Address2] [nchar](100) NULL,
	[City] [nchar](30) NULL,
	[State] [nchar](10) NULL,
	[ZipCode] [nchar](10) NULL,
	[IsActive] [bit] NULL,
	[IsAdmin] [bit] NULL,
	[Username] [nchar](10) NULL,
	[Password] [nchar](100) NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [RentMe]
GO

INSERT INTO [dbo].[Employees]
           ([FName]
           ,[LName]
           ,[Sex]
           ,[DOB]
           ,[Phone]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[ZipCode]
           ,[IsActive]
           ,[IsAdmin]
           ,[Username]
           ,[Password])
     VALUES
           ('Ashley'
           ,'Student'
           ,'F'
           ,'1989-01-01'
           ,'111-111-1111'
           ,'Address1'
           ,'Address2'
           ,'Seattle'
           ,'WA'
           ,'111111'
           ,1
           ,1
           ,'Ashley'
           ,'Vz9RN0M97WOWRX20B3jpnQ==')
GO

INSERT INTO [dbo].[Employees]
           ([FName]
           ,[LName]
           ,[Sex]
           ,[DOB]
           ,[Phone]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[ZipCode]
           ,[IsActive]
           ,[IsAdmin]
           ,[Username]
           ,[Password])
     VALUES
           ('Chuck'
           ,'Tutor'
           ,'M'
           ,'1982-02-02'
           ,'222-222-2222'
           ,'2229 S Canal'
           ,'Address2'
           ,'Chicago'
           ,'IL'
           ,'22222'
           ,1
           ,0
           ,'Chuck'
           ,'w6dXyJUoEAleuCLqYefSCQ==')
GO
SET IDENTITY_INSERT [dbo].[Employees] ON 
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1000, N'Sarah', N'Smith', N'Female', CAST(N'1965-01-01' AS Date), N'605-311-2198', N'345 Pathfinder Dr.', NULL, N'Meridian', N'ID', N'83642', N'sSmith1234', N'tyer865!', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1001, N'John', N'Doe', N'Male', CAST(N'1975-02-02' AS Date), N'605-245-6754', N'9898 State St.', N'P.O. Box 43', N'Meridian', N'ID', N'83642', N'jDoe', N'admin', 1, 1)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1002, N'Sam', N'Clark', N'Male', CAST(N'1970-03-03' AS Date), N'208-456-7654', N'677 Black Cat Rd.', NULL, N'Meridian', N'ID', N'83646', N'SClark987', N'kjdslf!!8', 0, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1003, N'Vi', N'Hartup', N'Female', CAST(N'1978-09-23' AS Date), N'330-202-1900', N'33 Pleasanton Circle', N'Apt. 33', N'Meridian', N'ID', N'83646', N'vhartup00', N'Y182TqCp', 1, 1)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1008, N'Olga', N'Wilbor', N'Female', CAST(N'1986-10-07' AS Date), N'562-873-3373', N'560 Debs Alley', NULL, N'San Francisco', N'CA', N'94110', N'owilbor1', N'dklafjiweuhfwekj78', 0, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1009, N'Roley', N'Connaughton', N'Male', CAST(N'1982-12-13' AS Date), N'602-545-5364', N'9 Butterfield Crossing', N'Apt. 69', N'Meridian', N'ID', N'83642', N'rconnaughton2', N'sdklfjsdlkj87', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1010, N'Karlie', N'Treva', N'Female', CAST(N'1996-03-14' AS Date), N'313-491-4580', N'39984 Eagle Crest Parkway', NULL, N'Boise', N'ID', N'83701', N'KTrev01', N'kdjsflk897!!', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1011, N'Oliver', N'Steynor', N'Male', CAST(N'1991-04-15' AS Date), N'757-245-8860', N'2753 Cottonwood Center', NULL, N'Boise', N'ID', N'83704', N'osteynor5', N'kdj8DDK', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1012, N'Mariya', N'Ducarne', N'Female', CAST(N'1980-07-25' AS Date), N'772-413-0562', N'454 Melrose Ct.', N'Apt. 876', N'Star', N'ID', N'83669', N'mDucarne9', N'dkjf8YYY', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1013, N'Nancy', N'Ortega', N'Female', CAST(N'1992-07-05' AS Date), N'202-431-5678', N'13245 Mark Pl.', NULL, N'Star', N'ID', N'83669', N'nnortega', N'YJJkles8', 1, 0)
SET IDENTITY_INSERT [dbo].[Employees] OFF
