
/****** Object:  Table [dbo].[Style]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Style](
	[StyleID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Style] PRIMARY KEY CLUSTERED 
(
	[StyleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (1, N'Traditional')
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (2, N'Modern')
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (3, N'Transitional')
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (4, N'Mid-Centry Modern')
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (5, N'Contemporary')