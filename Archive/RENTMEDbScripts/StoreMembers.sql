/****** Object:  Table [dbo].[StoreMembers]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreMembers](
	[MemberID] [int] IDENTITY(1000,1) NOT NULL,
	[FName] [varchar](50) NOT NULL,
	[LName] [varchar](50) NOT NULL,
	[DOB] [date] NOT NULL,
	[Phone] [varchar](20) NOT NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NULL,
	[City] [nvarchar](50) NOT NULL,
	[State] [char](2) NOT NULL,
	[ZipCode] [varchar](9) NOT NULL,
 CONSTRAINT [PK_StoreMembers] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[StoreMembers] ON 
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1000, N'Timothea', N'Tolumello', CAST(N'1980-01-09' AS Date), N'317-888-6214', N'90 Hoepker Park', N'Apt. 5', N'Indianapolis', N'IN', N'46295')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1001, N'Carmon', N'Log', CAST(N'1963-01-05' AS Date), N'785-610-7779', N'3665 Vahlen Lane', NULL, N'Topeka', N'KS', N'66606')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1002, N'Jonell', N'Chicotti', CAST(N'1970-06-21' AS Date), N'563-562-8120', N'5 Boyd Road', NULL, N'Davenport', N'IA', N'52809')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1003, N'Fey', N'Payne', CAST(N'1999-07-10' AS Date), N'910-185-7166', N'35499 Crowly Point', N'Apt. 432', N'Boise', N'ID', N'83701')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1004, N'Dorella', N'Cawkwell', CAST(N'1981-12-09' AS Date), N'208-997-0342', N'7 Vahlen Way', N'P.O. Box 832', N'Star', N'ID', N'83699')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1005, N'Glori', N'Turnock', CAST(N'1987-03-16' AS Date), N'915-590-4366', N'4687 Florence Court', NULL, N'Boise', N'ID', N'83701')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1006, N'Dave', N'Tilt', CAST(N'2000-08-28' AS Date), N'417-564-3265', N'6 Nevada Dr', NULL, N'Kuna', N'ID', N'83634')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1007, N'Sibella', N'Murby', CAST(N'1997-05-09' AS Date), N'212-433-2455', N'3658 Elmside Dr.', NULL, N'Kuna', N'ID', N'83634')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1008, N'Catlin', N'Noor', CAST(N'1975-01-23' AS Date), N'407-876-5467', N'56 Cottonwood Terrace', NULL, N'Kuna', N'ID', N'83634')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1009, N'Nat', N'Simmans', CAST(N'1965-06-18' AS Date), N'510-874-0967', N'37 Idaho Terrace', NULL, N'Boise', N'ID', N'83701')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1010, N'Zondra', N'Gavini', CAST(N'1981-04-29' AS Date), N'408-543-2100', N'10 Mariners Cove Ave.', NULL, N'Meridian', N'ID', N'83646')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1011, N'Mellie', N'Friar', CAST(N'1990-09-24' AS Date), N'309-879-0858', N'98 Meadow Trail', NULL, N'Meridian', N'ID', N'83642')
SET IDENTITY_INSERT [dbo].[StoreMembers] OFF