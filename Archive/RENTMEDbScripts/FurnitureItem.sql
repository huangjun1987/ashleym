
/****** Object:  Table [dbo].[FurnitureItem]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FurnitureItem](
	[Serial#] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[FineRate] [decimal](9, 2) NOT NULL,
	[DailyRentalRate] [decimal](9, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[StyleID] [int] NOT NULL,
 CONSTRAINT [PK_FurnitureItem] PRIMARY KEY CLUSTERED 
(
	[Serial#] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FurnitureItem]  WITH CHECK ADD  CONSTRAINT [FK_FurnitureItem_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[FurnitureItem] CHECK CONSTRAINT [FK_FurnitureItem_Category]
GO
ALTER TABLE [dbo].[FurnitureItem]  WITH CHECK ADD  CONSTRAINT [FK_FurnitureItem_Style] FOREIGN KEY([StyleID])
REFERENCES [dbo].[Style] ([StyleID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[FurnitureItem] CHECK CONSTRAINT [FK_FurnitureItem_Style]
GO

INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bcygec1d', N'Cherry Queen Bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 7, 5, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bgeilv5c', N'Walnut Queen Bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 5, 5, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bkjkjc2v', N'Black King Bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 7, 5, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'BkscGy5d', N'Black and Glass Bookshelf', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 4, 8, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'BkshEr3q', N'Mahogany Bookshelf', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 12, 8, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'BksVks3m', N'White Bookshelf', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 43, 8, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'BsvIL7dk', N'Cherry Bookshelf', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 18, 8, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bvdfjr7x', N'Pine King Bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 4, 5, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bzmldf9y', N'White twin bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 5, 5, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chckee3d', N'Leather Club Chair', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 9, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'ChDdjk7c', N'Desk Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 50, 2, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chdkjk7k', N'White Wood Dining Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 40, 2, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chdlkj8d', N'Oak Dining Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 40, 2, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chdokd4r', N'Flower Fabric Dining Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 32, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chdyuk6e', N'Clear Plastic Dining Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 32, 2, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chrgkk42', N'Pine Rocking Chair', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 5, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chrkjk3v', N'Leather Recliner', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 7, 2, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chrklk5m', N'Blue Recliner', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 10, 2, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chwbkj8d', N'Blue Wingback Chair', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 4, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chwi5tgy', N'Cherry Wood Windsor Chair', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 4, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Cobrfh0r', N'Black Couch', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 15, 6, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Cogfkc7d', N'Grey Fabric Couch', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 11, 6, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Colebv6c', N'Leather Couch', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 11, 6, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Covgkb1x', N'Green Velvet Couch', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 2, 6, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTglsv2b', N'Glass Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 12, 10, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTrerd1n', N'Mahogany Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 6, 10, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTvegw1u', N'Pine Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 16, 10, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTwhvk6r', N'White and Black Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 10, 10, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTwhvo7b', N'White Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 17, 10, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Dbkljj3ld', N'Black Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 8, 1, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Dcdslf9d', N'Cherry Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 15, 1, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'DcksfY1g', N'Computer Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 6, 1, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Dekljjl5d', N'Espresso Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 4, 1, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drblcb9k', N'Black 3 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 14, 4, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drcyuy7q', N'Cherry 6 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 10, 4, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drplgh3b', N'Pine 3 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 23, 4, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drwgye6s', N'Walnut 9 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 19, 4, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drwhnm5w', N'White 6 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 29, 4, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Dwdklf2k', N'White Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 9, 1, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Ecblcd0w', N'Black entertainment center', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 16, 7, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Echwba9j', N'Cherry Wood Entertainment Center', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 9, 7, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Ecwhwb5t', N'White entertainment center', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 21, 7, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'HtchCD2e', N'Walnut Hutch', CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 7, 9, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'HtchVT1u', N'Blue Hutch', CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 3, 9, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'HtchWH2q', N'White Hutch', CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 9, 9, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'HtchXZ4w', N'Cherry Hutch', CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 7, 9, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'TgyuTc8b', N'Glass Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 3, 3, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'TmcUrc9z', N'Mohogany Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 12, 3, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'TmkfdU4t', N'Maple Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 5, 3, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'ToUtgy7c', N'Oak Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 13, 3, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'TsuiEik5y', N'Steel Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 1, 3, 5)
