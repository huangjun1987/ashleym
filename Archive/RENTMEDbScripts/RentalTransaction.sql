/****** Object:  Table [dbo].[RentalTransaction]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RentalTransaction](
	[RentalID] [int] IDENTITY(1000,1) NOT NULL,
	[DateOfRental] [date] NOT NULL,
	[ScheduledReturn] [date] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
 CONSTRAINT [PK_RentalTransaction] PRIMARY KEY CLUSTERED 
(
	[RentalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[RentalTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RentalTransaction_Employees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RentalTransaction] CHECK CONSTRAINT [FK_RentalTransaction_Employees]
GO
ALTER TABLE [dbo].[RentalTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RentalTransaction_StoreMembers] FOREIGN KEY([MemberID])
REFERENCES [dbo].[StoreMembers] ([MemberID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RentalTransaction] CHECK CONSTRAINT [FK_RentalTransaction_StoreMembers]
GO

SET IDENTITY_INSERT [dbo].[RentalTransaction] ON 
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1000, CAST(N'2020-01-18' AS Date), CAST(N'2020-05-07' AS Date), 1000, 1000)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1001, CAST(N'2018-07-16' AS Date), CAST(N'2019-02-16' AS Date), 1000, 1000)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1002, CAST(N'2019-02-13' AS Date), CAST(N'2020-01-03' AS Date), 1010, 1002)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1003, CAST(N'2018-11-17' AS Date), CAST(N'2020-02-22' AS Date), 1010, 1001)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1004, CAST(N'2017-08-27' AS Date), CAST(N'2021-02-20' AS Date), 1010, 1001)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1007, CAST(N'2020-03-14' AS Date), CAST(N'2020-05-04' AS Date), 1008, 1000)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1008, CAST(N'2020-02-01' AS Date), CAST(N'2020-09-08' AS Date), 1009, 1010)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1009, CAST(N'2019-08-06' AS Date), CAST(N'2020-04-09' AS Date), 1011, 1002)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1010, CAST(N'2019-12-13' AS Date), CAST(N'2020-12-13' AS Date), 1012, 1003)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1011, CAST(N'2019-11-23' AS Date), CAST(N'2020-01-14' AS Date), 1013, 1004)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1012, CAST(N'2019-01-02' AS Date), CAST(N'2020-01-02' AS Date), 1009, 1005)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1013, CAST(N'2018-06-05' AS Date), CAST(N'2019-08-17' AS Date), 1009, 1006)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1014, CAST(N'2019-07-04' AS Date), CAST(N'2020-03-01' AS Date), 1002, 1007)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1015, CAST(N'2019-10-06' AS Date), CAST(N'2020-06-19' AS Date), 1011, 1008)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1016, CAST(N'2019-10-07' AS Date), CAST(N'2020-06-18' AS Date), 1012, 1009)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1017, CAST(N'2020-02-14' AS Date), CAST(N'2020-06-15' AS Date), 1013, 1011)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1018, CAST(N'2019-04-05' AS Date), CAST(N'2020-09-11' AS Date), 1009, 1010)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1019, CAST(N'2019-04-29' AS Date), CAST(N'2020-09-11' AS Date), 1000, 1010)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1020, CAST(N'2019-04-29' AS Date), CAST(N'2020-08-10' AS Date), 1012, 1005)
SET IDENTITY_INSERT [dbo].[RentalTransaction] OFF