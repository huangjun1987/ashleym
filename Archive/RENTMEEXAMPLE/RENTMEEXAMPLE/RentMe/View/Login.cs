﻿using RentMe.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace RentMe.View
{
    public partial class Login : Form
    {
        private readonly LoginController controller;
        public Login()
        {
            InitializeComponent();
            this.controller = new LoginController();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(usernameTextBox.Text))
                {
                    MessageBox.Show("Please enter username");
                    return;
                }
                if (string.IsNullOrWhiteSpace(passwordTextBox.Text))
                {
                    MessageBox.Show("Please enter password");
                    return;
                }
                var employee = controller.GetEmployeeByUserNamePassword(usernameTextBox.Text, passwordTextBox.Text);
                if (employee == null)
                {
                    MessageBox.Show("Incorrect username or password");
                    usernameTextBox.Text = "";
                    passwordTextBox.Text = "";
                }
                else if (employee.IsAdmin)
                {
                    this.Hide();
                    AdminDashboard adminDashboard = new AdminDashboard(usernameTextBox.Text);
                    adminDashboard.Show();
                }
                else
                {
                    this.Hide();
                    MainDashboard mainDashboard = new MainDashboard(usernameTextBox.Text);
                    mainDashboard.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Login failed");
            }
        }

        private void usernameLabel_Click(object sender, EventArgs e)
        {

        }

        private void passwordLabel_Click(object sender, EventArgs e)
        {

        }

        private void usernameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void employeeLabel_Click(object sender, EventArgs e)
        {

        }

        private void passwordTextBox_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
