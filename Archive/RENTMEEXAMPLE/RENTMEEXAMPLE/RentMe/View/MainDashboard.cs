﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentMe
{
    public partial class MainDashboard : Form
    {
        public MainDashboard(string username)
        {
            InitializeComponent();
            labelUsername.Text = username;
        }
    }
}
