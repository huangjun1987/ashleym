﻿using RentMe.Model;
using RentMe.Utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentMe.DAL
{
    public class LoginDAL
    {
        public Employee GetEmployeeByUserNamePassword(String username, String password)
        {
          
            string selectStatement =
                @"SELECT *
                FROM dbo.Employees
                WHERE Username = @username and Password = @password" ;

            using (SqlConnection connection = RentMeDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@username", username);
                    selectCommand.Parameters.AddWithValue("@password", EncryptAndDecryptUtil.Encrypt(password));

                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int employeeID = int.Parse(reader["EmployeeID"].ToString());
                            string fName = reader["FName"].ToString();
                            string lName = reader["LName"].ToString();
                            string sex = reader["Sex"].ToString();
                            DateTime dob = DateTime.Parse(reader["DOB"].ToString());
                            string phone = reader["Phone"].ToString();
                            string address1 = reader["Address1"].ToString();
                            string address2 = reader["Address2"].ToString();
                            string city = reader["City"].ToString();
                            string state = reader["State"].ToString();
                            string zipCode = reader["ZipCode"].ToString();                           
                            Boolean isActive = bool.Parse(reader["IsActive"].ToString());
                            Boolean isAdmin = bool.Parse(reader["IsAdmin"].ToString());
                            var employee = new Employee(employeeID, fName,lName,sex,dob,
                                phone, address1,address2, city, state, zipCode, username, password, isActive, isAdmin);
                            return employee;
                        }
                    }
                }
            }
            return null;
        }
    }
}
