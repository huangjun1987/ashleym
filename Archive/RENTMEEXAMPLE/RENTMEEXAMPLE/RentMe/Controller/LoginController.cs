﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentMe.DAL;
using RentMe.Model;

namespace RentMe.Controller
{
   
    class LoginController
    {
        private LoginDAL loginDAL;

        public LoginController()
        {
            this.loginDAL = new LoginDAL();
        }

        internal Employee GetEmployeeByUserNamePassword(string username, string password)
        {
            return loginDAL.GetEmployeeByUserNamePassword(username, password);
        }
    }
}
