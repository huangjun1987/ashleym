USE [master]
GO
/****** Object:  Database [CS6232-g2]    Script Date: 3/22/2020 4:00:34 PM ******/
CREATE DATABASE [CS6232-g2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CS6232-g2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\CS6232-g2.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CS6232-g2_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\CS6232-g2_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [CS6232-g2] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CS6232-g2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CS6232-g2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CS6232-g2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CS6232-g2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CS6232-g2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CS6232-g2] SET ARITHABORT OFF 
GO
ALTER DATABASE [CS6232-g2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CS6232-g2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CS6232-g2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CS6232-g2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CS6232-g2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CS6232-g2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CS6232-g2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CS6232-g2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CS6232-g2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CS6232-g2] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CS6232-g2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CS6232-g2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CS6232-g2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CS6232-g2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CS6232-g2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CS6232-g2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CS6232-g2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CS6232-g2] SET RECOVERY FULL 
GO
ALTER DATABASE [CS6232-g2] SET  MULTI_USER 
GO
ALTER DATABASE [CS6232-g2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CS6232-g2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CS6232-g2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CS6232-g2] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CS6232-g2] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CS6232-g2', N'ON'
GO
ALTER DATABASE [CS6232-g2] SET QUERY_STORE = OFF
GO
USE [CS6232-g2]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeID] [int] IDENTITY(1000,1) NOT NULL,
	[FName] [varchar](50) NOT NULL,
	[LName] [varchar](50) NOT NULL,
	[Sex] [varchar](20) NOT NULL,
	[DOB] [date] NOT NULL,
	[Phone] [varchar](20) NOT NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NOT NULL,
	[State] [char](2) NOT NULL,
	[ZipCode] [varchar](9) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FurnitureItem]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FurnitureItem](
	[Serial#] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[FineRate] [decimal](9, 2) NOT NULL,
	[DailyRentalRate] [decimal](9, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[StyleID] [int] NOT NULL,
 CONSTRAINT [PK_FurnitureItem] PRIMARY KEY CLUSTERED 
(
	[Serial#] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RentalTransaction]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RentalTransaction](
	[RentalID] [int] IDENTITY(1000,1) NOT NULL,
	[DateOfRental] [date] NOT NULL,
	[ScheduledReturn] [date] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
 CONSTRAINT [PK_RentalTransaction] PRIMARY KEY CLUSTERED 
(
	[RentalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RentedItem]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RentedItem](
	[Serial#] [varchar](50) NOT NULL,
	[RentalID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_RentedItem] PRIMARY KEY CLUSTERED 
(
	[Serial#] ASC,
	[RentalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Returns]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Returns](
	[Serial#] [varchar](50) NOT NULL,
	[RentalID] [int] NOT NULL,
	[ReturnID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[FineDue] [decimal](9, 2) NOT NULL,
	[RefundDue] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_Returns] PRIMARY KEY CLUSTERED 
(
	[Serial#] ASC,
	[RentalID] ASC,
	[ReturnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReturnTransaction]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReturnTransaction](
	[ReturnID] [int] IDENTITY(1000,1) NOT NULL,
	[ReturnDate] [date] NOT NULL,
	[FineDueTotal] [decimal](9, 2) NOT NULL,
	[RefundDueTotal] [decimal](9, 2) NOT NULL,
	[EmployeeID] [int] NOT NULL,
 CONSTRAINT [PK_ReturnTransaction] PRIMARY KEY CLUSTERED 
(
	[ReturnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StoreMembers]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreMembers](
	[MemberID] [int] IDENTITY(1000,1) NOT NULL,
	[FName] [varchar](50) NOT NULL,
	[LName] [varchar](50) NOT NULL,
	[DOB] [date] NOT NULL,
	[Phone] [varchar](20) NOT NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NULL,
	[City] [nvarchar](50) NOT NULL,
	[State] [char](2) NOT NULL,
	[ZipCode] [varchar](9) NOT NULL,
 CONSTRAINT [PK_StoreMembers] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Style]    Script Date: 3/22/2020 4:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Style](
	[StyleID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Style] PRIMARY KEY CLUSTERED 
(
	[StyleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (1, N'desk')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (2, N'chair')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (3, N'table')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (4, N'dresser')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (5, N'bed')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (6, N'couch')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (7, N'entertainment center')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (8, N'bookshelf')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (9, N'hutch')
INSERT [dbo].[Category] ([CategoryID], [Description]) VALUES (10, N'coffee table')
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1000, N'Sarah', N'Smith', N'Female', CAST(N'1965-01-01' AS Date), N'605-311-2198', N'345 Pathfinder Dr.', NULL, N'Meridian', N'ID', N'83642', N'sSmith1234', N'tyer865!', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1001, N'John', N'Doe', N'Male', CAST(N'1975-02-02' AS Date), N'605-245-6754', N'9898 State St.', N'P.O. Box 43', N'Meridian', N'ID', N'83642', N'jDoe', N'admin', 1, 1)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1002, N'Sam', N'Clark', N'Male', CAST(N'1970-03-03' AS Date), N'208-456-7654', N'677 Black Cat Rd.', NULL, N'Meridian', N'ID', N'83646', N'SClark987', N'kjdslf!!8', 0, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1003, N'Vi', N'Hartup', N'Female', CAST(N'1978-09-23' AS Date), N'330-202-1900', N'33 Pleasanton Circle', N'Apt. 33', N'Meridian', N'ID', N'83646', N'vhartup00', N'Y182TqCp', 1, 1)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1008, N'Olga', N'Wilbor', N'Female', CAST(N'1986-10-07' AS Date), N'562-873-3373', N'560 Debs Alley', NULL, N'San Francisco', N'CA', N'94110', N'owilbor1', N'dklafjiweuhfwekj78', 0, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1009, N'Roley', N'Connaughton', N'Male', CAST(N'1982-12-13' AS Date), N'602-545-5364', N'9 Butterfield Crossing', N'Apt. 69', N'Meridian', N'ID', N'83642', N'rconnaughton2', N'sdklfjsdlkj87', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1010, N'Karlie', N'Treva', N'Female', CAST(N'1996-03-14' AS Date), N'313-491-4580', N'39984 Eagle Crest Parkway', NULL, N'Boise', N'ID', N'83701', N'KTrev01', N'kdjsflk897!!', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1011, N'Oliver', N'Steynor', N'Male', CAST(N'1991-04-15' AS Date), N'757-245-8860', N'2753 Cottonwood Center', NULL, N'Boise', N'ID', N'83704', N'osteynor5', N'kdj8DDK', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1012, N'Mariya', N'Ducarne', N'Female', CAST(N'1980-07-25' AS Date), N'772-413-0562', N'454 Melrose Ct.', N'Apt. 876', N'Star', N'ID', N'83669', N'mDucarne9', N'dkjf8YYY', 1, 0)
INSERT [dbo].[Employees] ([EmployeeID], [FName], [LName], [Sex], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode], [Username], [Password], [IsActive], [IsAdmin]) VALUES (1013, N'Nancy', N'Ortega', N'Female', CAST(N'1992-07-05' AS Date), N'202-431-5678', N'13245 Mark Pl.', NULL, N'Star', N'ID', N'83669', N'nnortega', N'YJJkles8', 1, 0)
SET IDENTITY_INSERT [dbo].[Employees] OFF
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bcygec1d', N'Cherry Queen Bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 7, 5, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bgeilv5c', N'Walnut Queen Bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 5, 5, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bkjkjc2v', N'Black King Bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 7, 5, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'BkscGy5d', N'Black and Glass Bookshelf', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 4, 8, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'BkshEr3q', N'Mahogany Bookshelf', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 12, 8, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'BksVks3m', N'White Bookshelf', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 43, 8, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'BsvIL7dk', N'Cherry Bookshelf', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 18, 8, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bvdfjr7x', N'Pine King Bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 4, 5, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Bzmldf9y', N'White twin bed', CAST(8.00 AS Decimal(9, 2)), CAST(30.00 AS Decimal(9, 2)), 5, 5, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chckee3d', N'Leather Club Chair', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 9, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'ChDdjk7c', N'Desk Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 50, 2, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chdkjk7k', N'White Wood Dining Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 40, 2, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chdlkj8d', N'Oak Dining Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 40, 2, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chdokd4r', N'Flower Fabric Dining Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 32, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chdyuk6e', N'Clear Plastic Dining Chair', CAST(2.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), 32, 2, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chrgkk42', N'Pine Rocking Chair', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 5, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chrkjk3v', N'Leather Recliner', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 7, 2, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chrklk5m', N'Blue Recliner', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 10, 2, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chwbkj8d', N'Blue Wingback Chair', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 4, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Chwi5tgy', N'Cherry Wood Windsor Chair', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 4, 2, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Cobrfh0r', N'Black Couch', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 15, 6, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Cogfkc7d', N'Grey Fabric Couch', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 11, 6, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Colebv6c', N'Leather Couch', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 11, 6, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Covgkb1x', N'Green Velvet Couch', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 2, 6, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTglsv2b', N'Glass Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 12, 10, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTrerd1n', N'Mahogany Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 6, 10, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTvegw1u', N'Pine Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 16, 10, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTwhvk6r', N'White and Black Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 10, 10, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'CTwhvo7b', N'White Coffee Table', CAST(4.00 AS Decimal(9, 2)), CAST(18.00 AS Decimal(9, 2)), 17, 10, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Dbkljj3ld', N'Black Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 8, 1, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Dcdslf9d', N'Cherry Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 15, 1, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'DcksfY1g', N'Computer Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 6, 1, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Dekljjl5d', N'Espresso Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 4, 1, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drblcb9k', N'Black 3 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 14, 4, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drcyuy7q', N'Cherry 6 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 10, 4, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drplgh3b', N'Pine 3 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 23, 4, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drwgye6s', N'Walnut 9 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 19, 4, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Drwhnm5w', N'White 6 drawer dresser', CAST(7.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 29, 4, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Dwdklf2k', N'White Desk', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 9, 1, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Ecblcd0w', N'Black entertainment center', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 16, 7, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Echwba9j', N'Cherry Wood Entertainment Center', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 9, 7, 2)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'Ecwhwb5t', N'White entertainment center', CAST(3.00 AS Decimal(9, 2)), CAST(15.00 AS Decimal(9, 2)), 21, 7, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'HtchCD2e', N'Walnut Hutch', CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 7, 9, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'HtchVT1u', N'Blue Hutch', CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 3, 9, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'HtchWH2q', N'White Hutch', CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 9, 9, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'HtchXZ4w', N'Cherry Hutch', CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), 7, 9, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'TgyuTc8b', N'Glass Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 3, 3, 5)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'TmcUrc9z', N'Mohogany Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 12, 3, 1)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'TmkfdU4t', N'Maple Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 5, 3, 4)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'ToUtgy7c', N'Oak Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 13, 3, 3)
INSERT [dbo].[FurnitureItem] ([Serial#], [Description], [FineRate], [DailyRentalRate], [Quantity], [CategoryID], [StyleID]) VALUES (N'TsuiEik5y', N'Steel Table', CAST(5.00 AS Decimal(9, 2)), CAST(20.00 AS Decimal(9, 2)), 1, 3, 5)
SET IDENTITY_INSERT [dbo].[RentalTransaction] ON 

INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1000, CAST(N'2020-01-18' AS Date), CAST(N'2020-05-07' AS Date), 1000, 1000)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1001, CAST(N'2018-07-16' AS Date), CAST(N'2019-02-16' AS Date), 1000, 1000)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1002, CAST(N'2019-02-13' AS Date), CAST(N'2020-01-03' AS Date), 1010, 1002)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1003, CAST(N'2018-11-17' AS Date), CAST(N'2020-02-22' AS Date), 1010, 1001)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1004, CAST(N'2017-08-27' AS Date), CAST(N'2021-02-20' AS Date), 1010, 1001)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1007, CAST(N'2020-03-14' AS Date), CAST(N'2020-05-04' AS Date), 1008, 1000)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1008, CAST(N'2020-02-01' AS Date), CAST(N'2020-09-08' AS Date), 1009, 1010)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1009, CAST(N'2019-08-06' AS Date), CAST(N'2020-04-09' AS Date), 1011, 1002)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1010, CAST(N'2019-12-13' AS Date), CAST(N'2020-12-13' AS Date), 1012, 1003)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1011, CAST(N'2019-11-23' AS Date), CAST(N'2020-01-14' AS Date), 1013, 1004)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1012, CAST(N'2019-01-02' AS Date), CAST(N'2020-01-02' AS Date), 1009, 1005)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1013, CAST(N'2018-06-05' AS Date), CAST(N'2019-08-17' AS Date), 1009, 1006)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1014, CAST(N'2019-07-04' AS Date), CAST(N'2020-03-01' AS Date), 1002, 1007)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1015, CAST(N'2019-10-06' AS Date), CAST(N'2020-06-19' AS Date), 1011, 1008)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1016, CAST(N'2019-10-07' AS Date), CAST(N'2020-06-18' AS Date), 1012, 1009)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1017, CAST(N'2020-02-14' AS Date), CAST(N'2020-06-15' AS Date), 1013, 1011)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1018, CAST(N'2019-04-05' AS Date), CAST(N'2020-09-11' AS Date), 1009, 1010)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1019, CAST(N'2019-04-29' AS Date), CAST(N'2020-09-11' AS Date), 1000, 1010)
INSERT [dbo].[RentalTransaction] ([RentalID], [DateOfRental], [ScheduledReturn], [EmployeeID], [MemberID]) VALUES (1020, CAST(N'2019-04-29' AS Date), CAST(N'2020-08-10' AS Date), 1012, 1005)
SET IDENTITY_INSERT [dbo].[RentalTransaction] OFF
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Bcygec1d', 1000, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Bgeilv5c', 1001, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Bkjkjc2v', 1002, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BkscGy5d', 1002, 3)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BkshEr3q', 1003, 4)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BksVks3m', 1004, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BsvIL7dk', 1000, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'BsvIL7dk', 1007, 3)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chckee3d', 1000, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'ChDdjk7c', 1008, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'ChDdjk7c', 1009, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'ChDdjk7c', 1010, 8)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chdkjk7k', 1011, 6)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chdokd4r', 1013, 8)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chrgkk42', 1015, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chrkjk3v', 1007, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chrklk5m', 1016, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Chwbkj8d', 1014, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Colebv6c', 1020, 3)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Covgkb1x', 1019, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'CTglsv2b', 1019, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Dbkljj3ld', 1008, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Dcdslf9d', 1010, 8)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'DcksfY1g', 1009, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Drwgye6s', 1012, 2)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'Ecblcd0w', 1017, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'HtchCD2e', 1018, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'TmcUrc9z', 1013, 1)
INSERT [dbo].[RentedItem] ([Serial#], [RentalID], [Quantity]) VALUES (N'ToUtgy7c', 1011, 1)
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Bgeilv5c', 1001, 1000, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Bkjkjc2v', 1002, 1001, 1, CAST(16.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'BkscGy5d', 1002, 1001, 3, CAST(42.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'BkshEr3q', 1003, 1002, 4, CAST(0.00 AS Decimal(9, 2)), CAST(200.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Chdkjk7k', 1011, 1004, 6, CAST(24.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Chdokd4r', 1013, 1006, 8, CAST(0.00 AS Decimal(9, 2)), CAST(560.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Chwbkj8d', 1014, 1008, 2, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'Drwgye6s', 1012, 1005, 2, CAST(70.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'TmcUrc9z', 1013, 1007, 1, CAST(5.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
INSERT [dbo].[Returns] ([Serial#], [RentalID], [ReturnID], [Quantity], [FineDue], [RefundDue]) VALUES (N'ToUtgy7c', 1011, 1003, 1, CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))
SET IDENTITY_INSERT [dbo].[ReturnTransaction] ON 

INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1000, CAST(N'2019-02-16' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1009)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1001, CAST(N'2020-01-05' AS Date), CAST(58.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1011)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1002, CAST(N'2020-02-20' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(200.00 AS Decimal(9, 2)), 1011)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1003, CAST(N'2020-01-14' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1012)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1004, CAST(N'2020-01-16' AS Date), CAST(24.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1011)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1005, CAST(N'2020-01-07' AS Date), CAST(70.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1012)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1006, CAST(N'2019-08-10' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(560.00 AS Decimal(9, 2)), 1013)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1007, CAST(N'2019-08-18' AS Date), CAST(5.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1013)
INSERT [dbo].[ReturnTransaction] ([ReturnID], [ReturnDate], [FineDueTotal], [RefundDueTotal], [EmployeeID]) VALUES (1008, CAST(N'2020-03-01' AS Date), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1000)
SET IDENTITY_INSERT [dbo].[ReturnTransaction] OFF
SET IDENTITY_INSERT [dbo].[StoreMembers] ON 

INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1000, N'Timothea', N'Tolumello', CAST(N'1980-01-09' AS Date), N'317-888-6214', N'90 Hoepker Park', N'Apt. 5', N'Indianapolis', N'IN', N'46295')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1001, N'Carmon', N'Log', CAST(N'1963-01-05' AS Date), N'785-610-7779', N'3665 Vahlen Lane', NULL, N'Topeka', N'KS', N'66606')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1002, N'Jonell', N'Chicotti', CAST(N'1970-06-21' AS Date), N'563-562-8120', N'5 Boyd Road', NULL, N'Davenport', N'IA', N'52809')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1003, N'Fey', N'Payne', CAST(N'1999-07-10' AS Date), N'910-185-7166', N'35499 Crowly Point', N'Apt. 432', N'Boise', N'ID', N'83701')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1004, N'Dorella', N'Cawkwell', CAST(N'1981-12-09' AS Date), N'208-997-0342', N'7 Vahlen Way', N'P.O. Box 832', N'Star', N'ID', N'83699')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1005, N'Glori', N'Turnock', CAST(N'1987-03-16' AS Date), N'915-590-4366', N'4687 Florence Court', NULL, N'Boise', N'ID', N'83701')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1006, N'Dave', N'Tilt', CAST(N'2000-08-28' AS Date), N'417-564-3265', N'6 Nevada Dr', NULL, N'Kuna', N'ID', N'83634')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1007, N'Sibella', N'Murby', CAST(N'1997-05-09' AS Date), N'212-433-2455', N'3658 Elmside Dr.', NULL, N'Kuna', N'ID', N'83634')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1008, N'Catlin', N'Noor', CAST(N'1975-01-23' AS Date), N'407-876-5467', N'56 Cottonwood Terrace', NULL, N'Kuna', N'ID', N'83634')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1009, N'Nat', N'Simmans', CAST(N'1965-06-18' AS Date), N'510-874-0967', N'37 Idaho Terrace', NULL, N'Boise', N'ID', N'83701')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1010, N'Zondra', N'Gavini', CAST(N'1981-04-29' AS Date), N'408-543-2100', N'10 Mariners Cove Ave.', NULL, N'Meridian', N'ID', N'83646')
INSERT [dbo].[StoreMembers] ([MemberID], [FName], [LName], [DOB], [Phone], [Address1], [Address2], [City], [State], [ZipCode]) VALUES (1011, N'Mellie', N'Friar', CAST(N'1990-09-24' AS Date), N'309-879-0858', N'98 Meadow Trail', NULL, N'Meridian', N'ID', N'83642')
SET IDENTITY_INSERT [dbo].[StoreMembers] OFF
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (1, N'Traditional')
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (2, N'Modern')
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (3, N'Transitional')
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (4, N'Mid-Centry Modern')
INSERT [dbo].[Style] ([StyleID], [Description]) VALUES (5, N'Contemporary')
ALTER TABLE [dbo].[FurnitureItem]  WITH CHECK ADD  CONSTRAINT [FK_FurnitureItem_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[FurnitureItem] CHECK CONSTRAINT [FK_FurnitureItem_Category]
GO
ALTER TABLE [dbo].[FurnitureItem]  WITH CHECK ADD  CONSTRAINT [FK_FurnitureItem_Style] FOREIGN KEY([StyleID])
REFERENCES [dbo].[Style] ([StyleID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[FurnitureItem] CHECK CONSTRAINT [FK_FurnitureItem_Style]
GO
ALTER TABLE [dbo].[RentalTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RentalTransaction_Employees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RentalTransaction] CHECK CONSTRAINT [FK_RentalTransaction_Employees]
GO
ALTER TABLE [dbo].[RentalTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RentalTransaction_StoreMembers] FOREIGN KEY([MemberID])
REFERENCES [dbo].[StoreMembers] ([MemberID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RentalTransaction] CHECK CONSTRAINT [FK_RentalTransaction_StoreMembers]
GO
ALTER TABLE [dbo].[RentedItem]  WITH CHECK ADD  CONSTRAINT [FK_RentedItem_FurnitureItem] FOREIGN KEY([Serial#])
REFERENCES [dbo].[FurnitureItem] ([Serial#])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RentedItem] CHECK CONSTRAINT [FK_RentedItem_FurnitureItem]
GO
ALTER TABLE [dbo].[RentedItem]  WITH CHECK ADD  CONSTRAINT [FK_RentedItem_RentalTransaction] FOREIGN KEY([RentalID])
REFERENCES [dbo].[RentalTransaction] ([RentalID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RentedItem] CHECK CONSTRAINT [FK_RentedItem_RentalTransaction]
GO
ALTER TABLE [dbo].[Returns]  WITH CHECK ADD  CONSTRAINT [FK_Returns_RentedItem] FOREIGN KEY([Serial#], [RentalID])
REFERENCES [dbo].[RentedItem] ([Serial#], [RentalID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Returns] CHECK CONSTRAINT [FK_Returns_RentedItem]
GO
ALTER TABLE [dbo].[Returns]  WITH CHECK ADD  CONSTRAINT [FK_Returns_ReturnTransaction] FOREIGN KEY([ReturnID])
REFERENCES [dbo].[ReturnTransaction] ([ReturnID])
GO
ALTER TABLE [dbo].[Returns] CHECK CONSTRAINT [FK_Returns_ReturnTransaction]
GO
ALTER TABLE [dbo].[ReturnTransaction]  WITH CHECK ADD  CONSTRAINT [FK_ReturnTransaction_Employees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ReturnTransaction] CHECK CONSTRAINT [FK_ReturnTransaction_Employees]
GO
USE [master]
GO
ALTER DATABASE [CS6232-g2] SET  READ_WRITE 
GO
