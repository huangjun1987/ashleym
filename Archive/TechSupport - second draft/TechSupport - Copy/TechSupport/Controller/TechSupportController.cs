﻿using TechSupport.DAL;
using System;
using System.Collections.Generic;
using TechSupport.Model;
using System.Windows.Forms;


namespace TechSupport.Controller
{
    /// The controller class deals with the DAL and delegates the work to DAL
    public class TechSupportController
    {
        private IncidentDAL incidentSource;
        private IncidentDBAL incidentDBAL;
        private ProductDBAL productDBAL;
        private TechnicianDBAL technicianDBAL;
        private CustomerDBAL customerDBAL;
        private RegistrationDBAL registrationDBAL;

        public TechSupportController()
        {
            this.incidentSource = new IncidentDAL();
            this.incidentDBAL = new IncidentDBAL();
            this.productDBAL = new ProductDBAL();
            this.technicianDBAL = new TechnicianDBAL();
            this.customerDBAL = new CustomerDBAL();
            this.registrationDBAL = new RegistrationDBAL();
        }

        /// <summary>
        /// This method delegates getsting a list of incidents to the DAL.
        /// </summary>
        /// <returns>A list of all incidents</returns>
        public List<Incident> GetIncidents()
        {
            return this.incidentSource.GetIncidents();
        }

        public void UpdateIncident(Update update, int incidentID, int? techID, string description, DateTime? closeDate)
        {
            incidentDBAL.UpdateIncident(update, incidentID, techID, description, closeDate);
        }


        /// <summary>
        /// This method delegates getting a list of all the open incidents to the database DAL.
        /// </summary>
        /// <returns>A list of open incidents</returns>
        public List<OpenIncident> GetOpenIncidents()
        {
            return this.incidentDBAL.GetOpenIncidents();
        }
        public List<OpenIncidentByTechnician> GetOpenIncidentsByTechID(int techID)
        {
            return this.incidentDBAL.GetOpenIncidentsByTechID(techID);
        }
        public Update GetUpdateIncident(int incidentID)
        {
            return this.incidentDBAL.GetUpdateIncident(incidentID);
        }

        public List<Technician> GetTechnicians()
        {
            return this.technicianDBAL.GetTechnicians();
        }
        public List<Technician> GetTechniciansWithIncidents()
        {
            return this.technicianDBAL.GetTechnicians();
        }
        

        /// <summary>
        /// This method delegates getting a list of all the customers to the database DAL.
        /// </summary>
        ///  /// <returns>A list of customers</returns>

        public List<Customer> GetCustomers()
        {
            return this.customerDBAL.GetCustomers();
        }

        /// <summary>
        /// This method delegates getting a list of all the products to the database DAL.
        /// </summary>
        ///  /// <returns>A list of products</returns>

        public List<Product> GetProducts()
        {
            return this.productDBAL.GetProducts();
        }

        /// <summary>
        /// This method delegates getting a list of all the products to the database DAL.
        /// </summary>
        ///  /// <returns>A list of products</returns>

        public List<Registration> GetRegistration()
        {
            return this.registrationDBAL.GetRegistration();
        }
        /// <summary>
        /// This method delegates adding an incident to the DAL.
        /// </summary>
        /// <param name="incident"></param>
        public void Add(Incident incident)
        {

            if (incident == null)
            {
                throw new ArgumentNullException("Incident cannot be null");
            }

            this.incidentDBAL.AddNewIncident(incident);


        }

        /// <summary>
        /// This method delegates getting a list of customers by customerID to the DAL.
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns>A list of customers</returns>
        public List<Incident> GetIncidentsByCustomerID(int customerID)
        {
            if (this.incidentSource.GetIncidentsByCustomerID(customerID).Count == 0)
            {
                MessageBox.Show("Customer does not exist",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return this.incidentSource.GetIncidentsByCustomerID(customerID);

        }
    }
}



