﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;

namespace TechSupport.UserControls
{
    public partial class ViewIncidentsByTechnicianControl : UserControl
    {
        private readonly TechSupportController controller;

        public ViewIncidentsByTechnicianControl()
        {
            InitializeComponent();
            this.controller = new TechSupportController();
            LoadTechnicians();
        }

        private void LoadTechnicians()
        {
            var techniciansWithIncidents = controller.GetTechniciansWithIncidents();
            comboBoxTechnician.Items.Clear();
            comboBoxTechnician.DataSource = techniciansWithIncidents;
            comboBoxTechnician.DisplayMember = "Name";
            comboBoxTechnician.ValueMember = "TechID";
            if (techniciansWithIncidents.Count>0)
            {
                comboBoxTechnician.SelectedItem = techniciansWithIncidents.First();
            }
        }

        private void comboBoxTechnician_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedTechnician = (Technician)comboBoxTechnician.SelectedItem;
            if (selectedTechnician!=null)
            {
                textBoxEmail.Text = selectedTechnician.Email;
                textBoxPhone.Text = selectedTechnician.Phone;
            }
            LoadIncidentsByTechID(selectedTechnician.TechID);
        }

        private void LoadIncidentsByTechID(int techID)
        {
            var openIncidents = controller.GetOpenIncidentsByTechID(techID);
            dataGridViewOpenIncidents.DataSource = openIncidents;         
        }
    }
}
