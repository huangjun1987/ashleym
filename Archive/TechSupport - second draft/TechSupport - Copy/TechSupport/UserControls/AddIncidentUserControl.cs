﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;

namespace TechSupport.UserControls
{
    /// <summary>
    /// This is the user control for adding an incident
    /// </summary>
    public partial class AddIncidentUserControl : UserControl
    {
        private readonly TechSupportController controller;
        private readonly DisplayOpenIncidentUserControl displayIncident;
        private List<Customer> customerList;
        private List<Product> productList;
        private List<Registration> registrationList;
        public OpenIncident incident;

        public AddIncidentUserControl()
        {
            this.InitializeComponent();
            this.controller = new TechSupportController();
            this.displayIncident = new DisplayOpenIncidentUserControl();

        }

        private void AddIncidentUserControl_Load(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
            CustomerComboBox.SelectedIndex = -1;
            ProductComboBox.SelectedIndex = -1;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {

            try
            {

                if (!IsValidData())
                {
                    MessageBox.Show("The data is invalid");
                }


                else if (!IsRegistered())
                {
                    MessageBox.Show("There is no registration associated with the product");
                }

                else
                {

                    this.PutIncidentData();
                    MessageBox.Show("New Incident Created");
                    this.displayIncident.Display();
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("All information must be filled out");
            }

        }


        //old add code
        // var title = this.titleTextBox.Text;
        //var description = this.DescriptionTextBox.Text;
        //var customerID = int.Parse(this.customerIDTextBox.Text);

        // this.controller.Add(new Incident(title, description, customerID));
        // this.messageLabel.Text = "Incident was added";


        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.messageLabel.Text = "";
            this.titleTextBox.Text = "";
            this.DescriptionTextBox.Text = "";
            CustomerComboBox.SelectedIndex = -1;
            ProductComboBox.SelectedIndex = -1;
        }


        private void LoadComboBoxes()
        {
            try
            {

                customerList = this.controller.GetCustomers();
                CustomerComboBox.DataSource = customerList;
                CustomerComboBox.DisplayMember = "Name";
                CustomerComboBox.ValueMember = "CustomerID";

                productList = this.controller.GetProducts();
                ProductComboBox.DataSource = productList;
                ProductComboBox.DisplayMember = "Name";
                ProductComboBox.ValueMember = "ProductCode";
            }
            catch (Exception ex)
            {
                MessageBox.Show("There is an error in loading the data");
            }

        }

        private void PutIncidentData()
        {

            var title = this.titleTextBox.Text;
            var description = this.DescriptionTextBox.Text;
            var customerID = int.Parse(this.CustomerComboBox.SelectedValue.ToString());
            var productCode = ProductComboBox.SelectedValue.ToString();
            this.controller.Add(new Incident(title, description, customerID, productCode));
        }

        /// <summary>
        /// This method is to get if the customerID is registered
        /// 1. Have to get the customer ID by matching the customer name to the ID in the customer table
        /// 2. Have to check if that ID is registered
        /// 3. Have to get the productName and see if it matches the customerID that is registered
        /// </summary>
        /// <returns></returns>
        private bool IsRegistered()
        {

            registrationList = controller.GetRegistration();


            try
            {
                foreach (Registration registration in registrationList)
                {
                    {
                        if (int.Parse(CustomerComboBox.SelectedValue.ToString()) == registration.CustomerID && ProductComboBox.SelectedValue.ToString().Trim() == registration.ProductCode.Trim())
                        {

                            return true;
                        }


                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("no registration entered");
                return false;
            }





        }

        private bool IsValidData()
        {
            if (Validator.IsPresent(titleTextBox) &&
                Validator.IsPresent(DescriptionTextBox) &&

                Validator.IsPresent(CustomerComboBox) &&
                Validator.IsPresent(ProductComboBox))
            {
                return true;
            }

            else
                return false;
        }
    }
}

