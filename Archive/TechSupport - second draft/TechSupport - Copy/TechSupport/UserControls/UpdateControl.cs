﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;

namespace TechSupport.UserControls
{
    public partial class UpdateControl : UserControl
    {
        private readonly TechSupportController controller;
        public Update update;
        public UpdateControl()
        {
            InitializeComponent();
            this.controller = new TechSupportController();
            this.DisplayUpdateIncidentTab();

        }

        public void DisplayUpdateIncidentTab()
        {
            incidentIDTextBox.Text = "";
            customerTextBox.Text = "";
            productTextBox.Text = "";
            titleTextBox.Text = "";
            dateOpenedTextBox.Text = "";
            descriptionRichTextBox.Text = "";
            technicianComboBox.DataSource = null;
            UpdateButton.Enabled = false;
            CloseButton.Enabled = false;
            textToAddRichTextBox.Enabled = false;
            customerTextBox.ReadOnly = true;
            productTextBox.ReadOnly = true;
            titleTextBox.ReadOnly = true;
            dateOpenedTextBox.ReadOnly = true;
            descriptionRichTextBox.ReadOnly = true;
        }


        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedTechnician = (Technician)technicianComboBox.SelectedItem;
                if (selectedTechnician.Name == null)
                {
                    selectedTechnician.Name = "";
                }
                if (selectedTechnician.Name == update.Technician && string.IsNullOrWhiteSpace(textToAddRichTextBox.Text))
                {
                    MessageBox.Show("Please change Technician or update Text To Add");
                    return;
                }
                var techIDStr = technicianComboBox.SelectedValue.ToString();
                int? techID = string.IsNullOrWhiteSpace(techIDStr) ? (int?)null : int.Parse(techIDStr);
                UpdateIncident(int.Parse(incidentIDTextBox.Text), techID, descriptionRichTextBox.Text, textToAddRichTextBox.Text, null);
                GetButton_Click(sender, e);
            }
            catch (Exception ex)
            {

                MessageBox.Show($"Failed to update incident. {ex.Message}");
            }          
        }

        private void UpdateIncident(int incidentID, int? techID, string description, string textToAdd, DateTime? closeDate)
        {
           
            if (!string.IsNullOrWhiteSpace(textToAdd))
            {
                description += $"{Environment.NewLine}<{DateTime.Now.Month}/{DateTime.Now.Day}/{DateTime.Now.Year}> {textToAdd}";
            }
            if (description.Length > 200)
            {
                var result = MessageBox.Show("Message is too long and will be truncated", "Warning", MessageBoxButtons.OKCancel);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    description = description.Substring(0, 200);
                }
            }
            textToAddRichTextBox.Text = "";
            controller.UpdateIncident(update, incidentID, techID, description, closeDate);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(technicianComboBox.SelectedValue.ToString()))
            {
                MessageBox.Show("Please assign a Technician before closing the incident.");
                return;
            }
            var result = MessageBox.Show("Are you sure you want to close the incident? Closed incident can no longer be edited.", "Warning", MessageBoxButtons.OKCancel);
            if (result == DialogResult.Cancel)
            {
                return;
            }
            UpdateIncident(int.Parse(incidentIDTextBox.Text), int.Parse(technicianComboBox.SelectedValue.ToString()), descriptionRichTextBox.Text, textToAddRichTextBox.Text, DateTime.Now);
            GetButton_Click(sender, e);

        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            DisplayUpdateIncidentTab();

        }

        private void GetButton_Click(object sender, EventArgs e)
        {

            update = new Update();
            try
            {
                if (!int.TryParse(incidentIDTextBox.Text, out int incidentId))
                {
                    MessageBox.Show("Please enter a valid Incident ID!");
                    return;
                }

                update = this.controller.GetUpdateIncident(incidentId);
                if (update == null)
                {
                    MessageBox.Show("Incident not found!");
                    return;
                }

                customerTextBox.Text = update.Customer;
                productTextBox.Text = update.ProductCode;
                titleTextBox.Text = update.Title;
                descriptionRichTextBox.Text = update.Description;
                dateOpenedTextBox.Text = update.DateOpened;
                var technicians = controller.GetTechnicians();
                var selectedTechnicianIndex = GetSelectedTechnicianIndex(technicians, update.Technician);
                if (selectedTechnicianIndex < 0)
                {
                    technicians.Add(new Technician());
                }
                technicians = technicians.OrderBy(t => t.Name).ToList();
                technicianComboBox.DataSource = technicians;
                technicianComboBox.DisplayMember = "Name";
                technicianComboBox.ValueMember = "TechID";
                if (selectedTechnicianIndex > 0)
                {
                    technicianComboBox.SelectedIndex = selectedTechnicianIndex;
                }
                UpdateButton.Enabled = true;
                CloseButton.Enabled = true;
                textToAddRichTextBox.Enabled = true;

                if (update.Description.Length == 200)
                {
                    MessageBox.Show("Description is already at maximum length, you cannot add any more text.");
                }
                if (!String.IsNullOrEmpty(update.DateClosed))
                {
                    UpdateButton.Enabled = false;
                    CloseButton.Enabled = false;
                    technicianComboBox.Enabled = false;
                    MessageBox.Show("Incident is closed. You can not update it.");

                }
                else
                {
                    UpdateButton.Enabled = true;
                    CloseButton.Enabled = true;
                    technicianComboBox.Enabled = true;

                }


            }

            catch (FormatException ex)
            {
                MessageBox.Show("Something is wrong with the input!!!!" + Environment.NewLine + ex.Message,
                   "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private int GetSelectedTechnicianIndex(List<Technician> technicians, string technician)
        {
            for (int i = 0; i < technicians.Count; i++)
            {
                if (technicians[i].Name == technician)
                {
                    return i;
                }
            }
            return -1;
        }

    }
}
