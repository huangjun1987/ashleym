﻿namespace TechSupport.UserControls
{
    partial class ViewIncidentsByTechnicianControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxTechnician = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.dataGridViewOpenIncidents = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOpenIncidents)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // comboBoxTechnician
            // 
            this.comboBoxTechnician.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTechnician.FormattingEnabled = true;
            this.comboBoxTechnician.Location = new System.Drawing.Point(177, 47);
            this.comboBoxTechnician.Margin = new System.Windows.Forms.Padding(6);
            this.comboBoxTechnician.Name = "comboBoxTechnician";
            this.comboBoxTechnician.Size = new System.Drawing.Size(1026, 39);
            this.comboBoxTechnician.TabIndex = 6;
            this.comboBoxTechnician.SelectedIndexChanged += new System.EventHandler(this.comboBoxTechnician_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 32);
            this.label2.TabIndex = 7;
            this.label2.Text = "Email:";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Enabled = false;
            this.textBoxEmail.Location = new System.Drawing.Point(177, 130);
            this.textBoxEmail.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(1026, 38);
            this.textBoxEmail.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 32);
            this.label3.TabIndex = 9;
            this.label3.Text = "Phone:";
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Enabled = false;
            this.textBoxPhone.Location = new System.Drawing.Point(177, 198);
            this.textBoxPhone.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(1026, 38);
            this.textBoxPhone.TabIndex = 10;
            // 
            // dataGridViewOpenIncidents
            // 
            this.dataGridViewOpenIncidents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridViewOpenIncidents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOpenIncidents.Location = new System.Drawing.Point(65, 284);
            this.dataGridViewOpenIncidents.Name = "dataGridViewOpenIncidents";
            this.dataGridViewOpenIncidents.RowHeadersWidth = 102;
            this.dataGridViewOpenIncidents.RowTemplate.Height = 40;
            this.dataGridViewOpenIncidents.Size = new System.Drawing.Size(1265, 322);
            this.dataGridViewOpenIncidents.TabIndex = 11;
            // 
            // ViewIncidentsByTechnicianControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridViewOpenIncidents);
            this.Controls.Add(this.textBoxPhone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxEmail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxTechnician);
            this.Controls.Add(this.label1);
            this.Name = "ViewIncidentsByTechnicianControl";
            this.Size = new System.Drawing.Size(1369, 624);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOpenIncidents)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTechnician;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.DataGridView dataGridViewOpenIncidents;
    }
}
