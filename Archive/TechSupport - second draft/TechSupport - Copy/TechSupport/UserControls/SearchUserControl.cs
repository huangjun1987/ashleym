﻿using System;
using System.Windows.Forms;
using TechSupport.Controller;

namespace TechSupport.UserControls
{
    /// <summary>
    /// This is a user control for the search function
    /// </summary>
    public partial class SearchUserControl : UserControl
    {
        private TechSupportController controller;
        public SearchUserControl()
        {
            InitializeComponent();
            this.controller = new TechSupportController();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            searchGrid.DataSource = null;
            try
            {
                searchGrid.DataSource = this.controller.GetIncidentsByCustomerID(int.Parse(searchTextBox.Text));
            }

            catch (FormatException ex)
            {
                MessageBox.Show("Something is wrong with the input!!!!" + Environment.NewLine + ex.Message,
                   "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
