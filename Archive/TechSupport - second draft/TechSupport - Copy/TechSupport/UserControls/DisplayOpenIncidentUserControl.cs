﻿using System;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;


namespace TechSupport.UserControls
{
    public partial class DisplayOpenIncidentUserControl : UserControl
    {
        private readonly TechSupportController controller;
        public DisplayOpenIncidentUserControl()
        {
            InitializeComponent();
            this.controller = new TechSupportController();
        }

        public void Display()
        {

            try
            {

                var incidentList = this.controller.GetOpenIncidents();
                lvOpenIncidents.Clear();
                lvOpenIncidents.View = System.Windows.Forms.View.Details;
                lvOpenIncidents.Columns.Add("ProductCode");
                lvOpenIncidents.Columns.Add("Title");
                lvOpenIncidents.Columns.Add("Customer");
                lvOpenIncidents.Columns.Add("DateOpened");
                lvOpenIncidents.Columns.Add("Technician");


                if (incidentList.Count > 0)
                {
                    OpenIncident incident;
                    for (int i = 0; i < incidentList.Count; i++)
                    {

                        incident = incidentList[i];
                        lvOpenIncidents.Items.Add(incident.ProductCode);
                        lvOpenIncidents.Items[i].SubItems.Add(incident.DateOpened.ToString());
                        lvOpenIncidents.Items[i].SubItems.Add(incident.Customer.ToString());
                        lvOpenIncidents.Items[i].SubItems.Add(incident.Technician.ToString());
                        lvOpenIncidents.Items[i].SubItems.Add(incident.Title.ToString());
                    }
        
                }
                else
                {
                    MessageBox.Show("Incidents cannot be shown.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
    }
}





