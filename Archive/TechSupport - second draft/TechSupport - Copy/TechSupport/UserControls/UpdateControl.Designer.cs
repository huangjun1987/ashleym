﻿namespace TechSupport.UserControls
{
    partial class UpdateControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.incidentIDLabel = new System.Windows.Forms.Label();
            this.incidentIDTextBox = new System.Windows.Forms.TextBox();
            this.GetButton = new System.Windows.Forms.Button();
            this.customerTextBox = new System.Windows.Forms.TextBox();
            this.productTextBox = new System.Windows.Forms.TextBox();
            this.technicianComboBox = new System.Windows.Forms.ComboBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.dateOpenedTextBox = new System.Windows.Forms.TextBox();
            this.descriptionRichTextBox = new System.Windows.Forms.RichTextBox();
            this.textToAddRichTextBox = new System.Windows.Forms.RichTextBox();
            this.customerLabel = new System.Windows.Forms.Label();
            this.productLabel = new System.Windows.Forms.Label();
            this.technicianLabel = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.dateOpenedLabel = new System.Windows.Forms.Label();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.textToAdd = new System.Windows.Forms.Label();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // incidentIDLabel
            // 
            this.incidentIDLabel.AutoSize = true;
            this.incidentIDLabel.Location = new System.Drawing.Point(62, 16);
            this.incidentIDLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.incidentIDLabel.Name = "incidentIDLabel";
            this.incidentIDLabel.Size = new System.Drawing.Size(149, 32);
            this.incidentIDLabel.TabIndex = 0;
            this.incidentIDLabel.Text = "Incident ID";
            // 
            // incidentIDTextBox
            // 
            this.incidentIDTextBox.Location = new System.Drawing.Point(274, 10);
            this.incidentIDTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.incidentIDTextBox.Name = "incidentIDTextBox";
            this.incidentIDTextBox.Size = new System.Drawing.Size(350, 38);
            this.incidentIDTextBox.TabIndex = 1;
            // 
            // GetButton
            // 
            this.GetButton.Location = new System.Drawing.Point(716, 10);
            this.GetButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.GetButton.Name = "GetButton";
            this.GetButton.Size = new System.Drawing.Size(150, 45);
            this.GetButton.TabIndex = 2;
            this.GetButton.Text = "Get";
            this.GetButton.UseVisualStyleBackColor = true;
            this.GetButton.Click += new System.EventHandler(this.GetButton_Click);
            // 
            // customerTextBox
            // 
            this.customerTextBox.Location = new System.Drawing.Point(274, 78);
            this.customerTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.customerTextBox.Name = "customerTextBox";
            this.customerTextBox.Size = new System.Drawing.Size(1026, 38);
            this.customerTextBox.TabIndex = 3;
            // 
            // productTextBox
            // 
            this.productTextBox.Location = new System.Drawing.Point(274, 163);
            this.productTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.productTextBox.Name = "productTextBox";
            this.productTextBox.Size = new System.Drawing.Size(1026, 38);
            this.productTextBox.TabIndex = 4;
            // 
            // technicianComboBox
            // 
            this.technicianComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.technicianComboBox.FormattingEnabled = true;
            this.technicianComboBox.Location = new System.Drawing.Point(274, 238);
            this.technicianComboBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.technicianComboBox.Name = "technicianComboBox";
            this.technicianComboBox.Size = new System.Drawing.Size(1026, 39);
            this.technicianComboBox.TabIndex = 5;
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(274, 316);
            this.titleTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(1026, 38);
            this.titleTextBox.TabIndex = 6;
            // 
            // dateOpenedTextBox
            // 
            this.dateOpenedTextBox.Location = new System.Drawing.Point(274, 384);
            this.dateOpenedTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.dateOpenedTextBox.Name = "dateOpenedTextBox";
            this.dateOpenedTextBox.Size = new System.Drawing.Size(448, 38);
            this.dateOpenedTextBox.TabIndex = 7;
            // 
            // descriptionRichTextBox
            // 
            this.descriptionRichTextBox.Location = new System.Drawing.Point(274, 461);
            this.descriptionRichTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.descriptionRichTextBox.Name = "descriptionRichTextBox";
            this.descriptionRichTextBox.Size = new System.Drawing.Size(1026, 130);
            this.descriptionRichTextBox.TabIndex = 8;
            this.descriptionRichTextBox.Text = "";
            // 
            // textToAddRichTextBox
            // 
            this.textToAddRichTextBox.Location = new System.Drawing.Point(274, 630);
            this.textToAddRichTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textToAddRichTextBox.Name = "textToAddRichTextBox";
            this.textToAddRichTextBox.Size = new System.Drawing.Size(1026, 134);
            this.textToAddRichTextBox.TabIndex = 9;
            this.textToAddRichTextBox.Text = "";
            // 
            // customerLabel
            // 
            this.customerLabel.AutoSize = true;
            this.customerLabel.Location = new System.Drawing.Point(66, 87);
            this.customerLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.customerLabel.Name = "customerLabel";
            this.customerLabel.Size = new System.Drawing.Size(145, 32);
            this.customerLabel.TabIndex = 10;
            this.customerLabel.Text = "Customer:";
            // 
            // productLabel
            // 
            this.productLabel.AutoSize = true;
            this.productLabel.Location = new System.Drawing.Point(66, 163);
            this.productLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.productLabel.Name = "productLabel";
            this.productLabel.Size = new System.Drawing.Size(121, 32);
            this.productLabel.TabIndex = 11;
            this.productLabel.Text = "Product:";
            // 
            // technicianLabel
            // 
            this.technicianLabel.AutoSize = true;
            this.technicianLabel.Location = new System.Drawing.Point(66, 244);
            this.technicianLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.technicianLabel.Name = "technicianLabel";
            this.technicianLabel.Size = new System.Drawing.Size(162, 32);
            this.technicianLabel.TabIndex = 12;
            this.technicianLabel.Text = "Technician:";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(66, 322);
            this.titleLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(78, 32);
            this.titleLabel.TabIndex = 13;
            this.titleLabel.Text = "Title:";
            // 
            // dateOpenedLabel
            // 
            this.dateOpenedLabel.AutoSize = true;
            this.dateOpenedLabel.Location = new System.Drawing.Point(62, 393);
            this.dateOpenedLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.dateOpenedLabel.Name = "dateOpenedLabel";
            this.dateOpenedLabel.Size = new System.Drawing.Size(192, 32);
            this.dateOpenedLabel.TabIndex = 14;
            this.dateOpenedLabel.Text = "Date Opened:";
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(66, 510);
            this.descriptionLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(166, 32);
            this.descriptionLabel.TabIndex = 15;
            this.descriptionLabel.Text = "Description:";
            // 
            // textToAdd
            // 
            this.textToAdd.AutoSize = true;
            this.textToAdd.Location = new System.Drawing.Point(62, 682);
            this.textToAdd.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.textToAdd.Name = "textToAdd";
            this.textToAdd.Size = new System.Drawing.Size(176, 32);
            this.textToAdd.TabIndex = 16;
            this.textToAdd.Text = "Text To Add:";
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(296, 779);
            this.UpdateButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(274, 72);
            this.UpdateButton.TabIndex = 17;
            this.UpdateButton.Text = "Update";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(694, 779);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(262, 72);
            this.CloseButton.TabIndex = 18;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(1054, 779);
            this.ClearButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(220, 72);
            this.ClearButton.TabIndex = 19;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // UpdateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.textToAdd);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.dateOpenedLabel);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.technicianLabel);
            this.Controls.Add(this.productLabel);
            this.Controls.Add(this.customerLabel);
            this.Controls.Add(this.textToAddRichTextBox);
            this.Controls.Add(this.descriptionRichTextBox);
            this.Controls.Add(this.dateOpenedTextBox);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(this.technicianComboBox);
            this.Controls.Add(this.productTextBox);
            this.Controls.Add(this.customerTextBox);
            this.Controls.Add(this.GetButton);
            this.Controls.Add(this.incidentIDTextBox);
            this.Controls.Add(this.incidentIDLabel);
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "UpdateControl";
            this.Size = new System.Drawing.Size(1410, 856);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label incidentIDLabel;
        private System.Windows.Forms.TextBox incidentIDTextBox;
        private System.Windows.Forms.Button GetButton;
        private System.Windows.Forms.TextBox customerTextBox;
        private System.Windows.Forms.TextBox productTextBox;
        private System.Windows.Forms.ComboBox technicianComboBox;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.TextBox dateOpenedTextBox;
        private System.Windows.Forms.RichTextBox descriptionRichTextBox;
        private System.Windows.Forms.RichTextBox textToAddRichTextBox;
        private System.Windows.Forms.Label customerLabel;
        private System.Windows.Forms.Label productLabel;
        private System.Windows.Forms.Label technicianLabel;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label dateOpenedLabel;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Label textToAdd;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button ClearButton;
    }
}
