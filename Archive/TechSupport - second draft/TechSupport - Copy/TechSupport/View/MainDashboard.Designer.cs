﻿namespace TechSupport.View
{
    partial class MainDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMainForm = new System.Windows.Forms.TabControl();
            this.addIncidentTab = new System.Windows.Forms.TabPage();
            this.Display_Open_Incidents = new System.Windows.Forms.TabPage();
            this.Update = new System.Windows.Forms.TabPage();
            this.tabPageViewIncidentsByTechnician = new System.Windows.Forms.TabPage();
            this.userNameLabelMainDash = new System.Windows.Forms.Label();
            this.Logout = new System.Windows.Forms.LinkLabel();
            this.addIncidentUserControl1 = new TechSupport.UserControls.AddIncidentUserControl();
            this.displayOpenIncidentUserControl1 = new TechSupport.UserControls.DisplayOpenIncidentUserControl();
            this.updateControl1 = new TechSupport.UserControls.UpdateControl();
            this.viewIncidentsByTechnicianControl1 = new TechSupport.UserControls.ViewIncidentsByTechnicianControl();
            this.tabControlMainForm.SuspendLayout();
            this.addIncidentTab.SuspendLayout();
            this.Display_Open_Incidents.SuspendLayout();
            this.Update.SuspendLayout();
            this.tabPageViewIncidentsByTechnician.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlMainForm
            // 
            this.tabControlMainForm.Controls.Add(this.addIncidentTab);
            this.tabControlMainForm.Controls.Add(this.Display_Open_Incidents);
            this.tabControlMainForm.Controls.Add(this.Update);
            this.tabControlMainForm.Controls.Add(this.tabPageViewIncidentsByTechnician);
            this.tabControlMainForm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControlMainForm.Location = new System.Drawing.Point(0, 116);
            this.tabControlMainForm.Margin = new System.Windows.Forms.Padding(6);
            this.tabControlMainForm.Name = "tabControlMainForm";
            this.tabControlMainForm.SelectedIndex = 0;
            this.tabControlMainForm.Size = new System.Drawing.Size(2392, 936);
            this.tabControlMainForm.TabIndex = 2;
            this.tabControlMainForm.SelectedIndexChanged += new System.EventHandler(this.tabControlMainForm_SelectedIndexChanged);
            // 
            // addIncidentTab
            // 
            this.addIncidentTab.Controls.Add(this.addIncidentUserControl1);
            this.addIncidentTab.Location = new System.Drawing.Point(10, 48);
            this.addIncidentTab.Margin = new System.Windows.Forms.Padding(6);
            this.addIncidentTab.Name = "addIncidentTab";
            this.addIncidentTab.Padding = new System.Windows.Forms.Padding(6);
            this.addIncidentTab.Size = new System.Drawing.Size(2372, 878);
            this.addIncidentTab.TabIndex = 0;
            this.addIncidentTab.Text = "Add Incident";
            this.addIncidentTab.UseVisualStyleBackColor = true;
            // 
            // Display_Open_Incidents
            // 
            this.Display_Open_Incidents.Controls.Add(this.displayOpenIncidentUserControl1);
            this.Display_Open_Incidents.Location = new System.Drawing.Point(10, 48);
            this.Display_Open_Incidents.Margin = new System.Windows.Forms.Padding(6);
            this.Display_Open_Incidents.Name = "Display_Open_Incidents";
            this.Display_Open_Incidents.Padding = new System.Windows.Forms.Padding(6);
            this.Display_Open_Incidents.Size = new System.Drawing.Size(2372, 878);
            this.Display_Open_Incidents.TabIndex = 3;
            this.Display_Open_Incidents.Text = "Display Open Incidents";
            this.Display_Open_Incidents.UseVisualStyleBackColor = true;
            // 
            // Update
            // 
            this.Update.Controls.Add(this.updateControl1);
            this.Update.Location = new System.Drawing.Point(10, 48);
            this.Update.Margin = new System.Windows.Forms.Padding(6);
            this.Update.Name = "Update";
            this.Update.Padding = new System.Windows.Forms.Padding(6);
            this.Update.Size = new System.Drawing.Size(2372, 878);
            this.Update.TabIndex = 4;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = true;
            // 
            // tabPageViewIncidentsByTechnician
            // 
            this.tabPageViewIncidentsByTechnician.Controls.Add(this.viewIncidentsByTechnicianControl1);
            this.tabPageViewIncidentsByTechnician.Location = new System.Drawing.Point(10, 48);
            this.tabPageViewIncidentsByTechnician.Name = "tabPageViewIncidentsByTechnician";
            this.tabPageViewIncidentsByTechnician.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageViewIncidentsByTechnician.Size = new System.Drawing.Size(2372, 878);
            this.tabPageViewIncidentsByTechnician.TabIndex = 5;
            this.tabPageViewIncidentsByTechnician.Text = "View Incidents by Technician";
            this.tabPageViewIncidentsByTechnician.UseVisualStyleBackColor = true;
            // 
            // userNameLabelMainDash
            // 
            this.userNameLabelMainDash.AutoSize = true;
            this.userNameLabelMainDash.Location = new System.Drawing.Point(1386, 41);
            this.userNameLabelMainDash.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.userNameLabelMainDash.Name = "userNameLabelMainDash";
            this.userNameLabelMainDash.Size = new System.Drawing.Size(0, 32);
            this.userNameLabelMainDash.TabIndex = 0;
            // 
            // Logout
            // 
            this.Logout.AutoSize = true;
            this.Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Logout.Location = new System.Drawing.Point(2138, 62);
            this.Logout.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(143, 46);
            this.Logout.TabIndex = 1;
            this.Logout.TabStop = true;
            this.Logout.Text = "Logout";
            this.Logout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Logout_LinkClicked);
            // 
            // addIncidentUserControl1
            // 
            this.addIncidentUserControl1.Location = new System.Drawing.Point(44, 52);
            this.addIncidentUserControl1.Margin = new System.Windows.Forms.Padding(12);
            this.addIncidentUserControl1.Name = "addIncidentUserControl1";
            this.addIncidentUserControl1.Size = new System.Drawing.Size(1502, 595);
            this.addIncidentUserControl1.TabIndex = 0;
            // 
            // displayOpenIncidentUserControl1
            // 
            this.displayOpenIncidentUserControl1.Location = new System.Drawing.Point(0, 8);
            this.displayOpenIncidentUserControl1.Margin = new System.Windows.Forms.Padding(12);
            this.displayOpenIncidentUserControl1.Name = "displayOpenIncidentUserControl1";
            this.displayOpenIncidentUserControl1.Size = new System.Drawing.Size(2370, 746);
            this.displayOpenIncidentUserControl1.TabIndex = 0;
            // 
            // updateControl1
            // 
            this.updateControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateControl1.Location = new System.Drawing.Point(6, 6);
            this.updateControl1.Margin = new System.Windows.Forms.Padding(12);
            this.updateControl1.Name = "updateControl1";
            this.updateControl1.Size = new System.Drawing.Size(2360, 866);
            this.updateControl1.TabIndex = 0;
            // 
            // viewIncidentsByTechnicianControl1
            // 
            this.viewIncidentsByTechnicianControl1.Location = new System.Drawing.Point(16, 20);
            this.viewIncidentsByTechnicianControl1.Name = "viewIncidentsByTechnicianControl1";
            this.viewIncidentsByTechnicianControl1.Size = new System.Drawing.Size(1369, 624);
            this.viewIncidentsByTechnicianControl1.TabIndex = 0;
            // 
            // MainDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2392, 1052);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.userNameLabelMainDash);
            this.Controls.Add(this.tabControlMainForm);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "MainDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Dashboard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDashboard_FormClosing);
            this.Load += new System.EventHandler(this.MainDashboard_Load);
            this.tabControlMainForm.ResumeLayout(false);
            this.addIncidentTab.ResumeLayout(false);
            this.Display_Open_Incidents.ResumeLayout(false);
            this.Update.ResumeLayout(false);
            this.tabPageViewIncidentsByTechnician.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMainForm;
        private System.Windows.Forms.TabPage addIncidentTab;
        private System.Windows.Forms.Label userNameLabelMainDash;
        private System.Windows.Forms.LinkLabel Logout;
      
        private UserControls.AddIncidentUserControl addIncidentUserControl1;
        private System.Windows.Forms.TabPage Display_Open_Incidents;
        private UserControls.DisplayOpenIncidentUserControl displayOpenIncidentUserControl1;
        private System.Windows.Forms.TabPage Update;
        private UserControls.UpdateControl updateControl1;
        private System.Windows.Forms.TabPage tabPageViewIncidentsByTechnician;
        private UserControls.ViewIncidentsByTechnicianControl viewIncidentsByTechnicianControl1;
    }
}