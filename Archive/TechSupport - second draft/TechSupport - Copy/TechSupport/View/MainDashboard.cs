﻿using System;
using System.Windows.Forms;
using TechSupport.Controller;

namespace TechSupport.View
{
    public partial class MainDashboard : Form
    {
        //The controller interacts with the view and updates the view if the model  changes
        //and updates the model if the view initiates any change.
        //It also interacts with DAL and deals with getting the data from the DAL and sending data to the DAL

        Form previousForm = null;
        private readonly TechSupportController controller;

        /// <summary>
        /// Initializes the component, the previous form, the userName and the controller. 
        /// </summary>
        /// <param name="userName">string of the user's name</param>
        /// <param name="form">previous login form that is passed in as a parameter</param>
        public MainDashboard(String userName, Form form)
        {
            InitializeComponent();
            userNameLabelMainDash.Text = userName;
            this.previousForm = form;
            this.controller = new TechSupportController();


        }


        private void MainDashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            previousForm.Close();
        }

        private void Logout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Visible = false;
            previousForm.Visible = true;
            
        }



        private void tabControlMainForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            displayOpenIncidentUserControl1.Display();

        }




        private void MainDashboard_Load(object sender, EventArgs e)
        {
            displayOpenIncidentUserControl1.Display();

        }
    }
}

