﻿using System;

namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for an open incident.
    /// </summary>
    public class OpenIncidentByTechnician
    {
        public String Product { get; set; }
        public String DateOpened { get; set; }
        public String Customer { get; set; }
        public String Title { get; set; }
    }
}
