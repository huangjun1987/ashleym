﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupport.Model
{
    /// <summary>
    /// Model class for Registration
    /// </summary>
   public class Registration
    {
        public String ProductCode { get; set; }
        public int CustomerID { get; set; }
    }
}
