﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupport.Model
{
    public class Update
    {
        public String ProductCode { get; set; }
        public String DateOpened { get; set; }
        public String DateClosed { get; set; }
        public String Customer { get; set; }
        public String Technician { get; set; }
        public String Title { get; set; }
        public int IncidentID { get; set; }
        public String Description { get; set; }
    }
}
