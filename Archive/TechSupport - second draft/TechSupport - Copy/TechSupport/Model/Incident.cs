﻿using System;


namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for an incident
    /// </summary>
    public class Incident
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ProductCode { get; set; }
        public int CustomerID { get; set; }

        public Incident(string title, string description, int customerID, string productCode)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("Title cannot be null or empty", "title");

            }

            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentException("Description cannot be null or empty", "description");

            }

            if (customerID < 0)
            {
                throw new ArgumentOutOfRangeException("customerID", "CustomerID has to be greater than 0");
            }

            this.Title = title;
            this.Description = description;
            this.CustomerID = customerID;
            this.ProductCode = productCode;

        }
    }
}

