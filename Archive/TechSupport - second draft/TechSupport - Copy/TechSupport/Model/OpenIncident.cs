﻿using System;

namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for an open incident.
    /// </summary>
    public class OpenIncident
    {

        public String ProductCode { get; set; }
        public String DateOpened { get; set; }
        public String Customer { get; set; }
        public String Technician { get; set; }
        public String Title { get; set; }


    }
}
