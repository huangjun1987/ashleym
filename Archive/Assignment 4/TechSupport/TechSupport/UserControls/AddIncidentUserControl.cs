﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.DAL;
using TechSupport.Model;

namespace TechSupport.UserControls
{
    /// <summary>
    /// This is the user control for adding an incident
    /// </summary>
    public partial class AddIncidentUserControl : UserControl
    {
        private readonly TechSupportController controller;
        private List<Customer> customerList;
        private List<Product> productList;
        private List<Registration> registrationList;
        public OpenIncident incident;
        private UserControls.DisplayOpenIncidentUserControl displayOpenIncidentUserControl1;
        public AddIncidentUserControl()
        {
            this.InitializeComponent();
            this.controller = new TechSupportController();
            //  this.customerList = new List<Customer>();
            //  this.productList = new List<Product>();
            displayOpenIncidentUserControl1 = new DisplayOpenIncidentUserControl();
        }

        private void AddIncidentUserControl_Load(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
            CustomerComboBox.SelectedIndex = -1;
            ProductComboBox.SelectedIndex = -1;
        }

        /*  private void AddButton_Click(object sender, EventArgs e)
          {
              incident = new OpenIncident();

              try
              {

                  if (IsValidData && IsRegistered())
                  {
                          incident = new OpenIncident();
                          this.PutIncidentData(incident);
                  }


              }
              catch (Exception ex)
              {
                  MessageBox.Show("Something is wrong with the input!!!!" + Environment.NewLine + ex.Message,
                      "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
              }

          }
          */

        //old add code
        // var title = this.titleTextBox.Text;
        //var description = this.DescriptionTextBox.Text;
        //var customerID = int.Parse(this.customerIDTextBox.Text);

        // this.controller.Add(new Incident(title, description, customerID));
        // this.messageLabel.Text = "Incident was added";


        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.messageLabel.Text = "";
            this.titleTextBox.Text = "";
            this.DescriptionTextBox.Text = "";
            CustomerComboBox.SelectedIndex = -1;
            ProductComboBox.SelectedIndex = -1;
        }

        /* private void AddIncident_FormClosed(object sender, FormClosedEventArgs e)
         {
              this.previousForm.Visible = true;
         }
         */


        private void LoadComboBoxes()
        {
            try
            {

                customerList = this.controller.GetCustomers();
                CustomerComboBox.DataSource = customerList;
                CustomerComboBox.DisplayMember = "Name";
                CustomerComboBox.ValueMember = "CustomerID";

                productList = this.controller.GetProducts();
                ProductComboBox.DataSource = productList;
                ProductComboBox.DisplayMember = "Name";
                ProductComboBox.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }

        private void PutIncidentData(OpenIncident incident)
        {
            incident.Customer = CustomerComboBox.SelectedValue.ToString();
            incident.ProductCode = ProductComboBox.SelectedValue.ToString();
            incident.Title = titleTextBox.Text;
            incident.Description = DescriptionTextBox.Text;
        }

        /// <summary>
        /// This method is to get if the customerID is registered
        /// 1. Have to get the customer ID by matching the customer name to the ID in the customer table
        /// 2. Have to check if that ID is registered
        /// 3. Have to get the productName and see if it matches the customerID that is registered
        /// </summary>
        /// <returns></returns>
        private bool IsRegistered()
        {
            registrationList = controller.GetRegistration();

            try
            {
                foreach (var registration in registrationList)
                {
                    if (registration.CustomerID == int.Parse(CustomerComboBox.SelectedValue.ToString()) &&
                        registration.ProductCode == int.Parse(ProductComboBox.SelectedValue.ToString()))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                return false;
            }





        }

        private bool IsValidData()
        {
            if (Validator.IsPresent(titleTextBox) &&
                Validator.IsPresent(DescriptionTextBox) &&

                Validator.IsPresent(CustomerComboBox) &&
                Validator.IsPresent(ProductComboBox))
            {
                return true;
            }

            else
                return false;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsRegistered())
                {
                    var title = this.titleTextBox.Text;
                    var description = this.DescriptionTextBox.Text;
                    var customerID = int.Parse(this.CustomerComboBox.SelectedValue.ToString());
                    this.controller.Add(new Incident(title, description, customerID, ProductComboBox.SelectedValue.ToString()));
                }
                else
                {
                    MessageBox.Show("there is no registration associated with the product", "Error!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }

        }
    }
}

