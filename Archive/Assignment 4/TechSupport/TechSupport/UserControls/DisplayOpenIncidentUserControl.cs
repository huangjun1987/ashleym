﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;


namespace TechSupport.UserControls
{
    public partial class DisplayOpenIncidentUserControl : UserControl
    {
        private readonly TechSupportController controller;
        public DisplayOpenIncidentUserControl()
        {
            InitializeComponent();
            this.controller = new TechSupportController();
        }

        /// <summary>
        /// refreshes the datagrid
        /// </summary>
        public void RefreshDataGrid()
        {

         ///  this.dataGridViewIncidents.DataSource = null;
           /// this.dataGridViewIncidents.DataSource = this.controller.GetIncidents();
        }

       
        public void Display()
        {
            try
            {

               var incidentList = this.controller.GetOpenIncidents();
                listView1.Clear();
                listView1.View = System.Windows.Forms.View.Details;              
                listView1.Columns.Add("ProductCode");
                listView1.Columns.Add("Title");
                listView1.Columns.Add("Customer");
                listView1.Columns.Add("DateOpened");
                listView1.Columns.Add("Technician");
                listView1.Columns.Add("Description");
                

                if (incidentList.Count > 0)
                {
                    for (int i = 0; i < incidentList.Count; i++)
                    {
                        var item = new ListViewItem(incidentList[i].ProductCode);
                        item.SubItems.Add(incidentList[i].Title);
                        item.SubItems.Add(incidentList[i].Customer);
                        item.SubItems.Add(incidentList[i].DateOpened);
                        item.SubItems.Add(incidentList[i].Technician);
                        item.SubItems.Add(incidentList[i].Description);
                        listView1.Items.Add(item);                  
                    }
                    listView1.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.ColumnContent);
                    listView1.AutoResizeColumn(1, ColumnHeaderAutoResizeStyle.ColumnContent);
                    listView1.AutoResizeColumn(2, ColumnHeaderAutoResizeStyle.ColumnContent);
                    listView1.AutoResizeColumn(3, ColumnHeaderAutoResizeStyle.ColumnContent);
                    listView1.AutoResizeColumn(4, ColumnHeaderAutoResizeStyle.ColumnContent);
                    listView1.AutoResizeColumn(5, ColumnHeaderAutoResizeStyle.ColumnContent);
                }
                else
                {
                    MessageBox.Show("All invoices are paid in full.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        public void Display2()
        {
            try
            {

                var incidentList = this.controller.GetOpenIncidents();
                listView1.Clear();
                listView1.View = System.Windows.Forms.View.Details;
                listView1.Columns.Add("ProductCode");
                listView1.Columns.Add("Title");
                listView1.Columns.Add("Customer");
                listView1.Columns.Add("DateOpened");
                listView1.Columns.Add("Technician");
                listView1.Columns.Add("Description");


                if (incidentList.Count > 0)
                {
                    for (int i = 0; i < incidentList.Count; i++)
                    {
                        var array = new string[6];
                        array[0] = incidentList[i].ProductCode;
                        array[1] = incidentList[i].Title;
                        array[2] = incidentList[i].Customer;
                        array[3] = incidentList[i].DateOpened;
                        array[4] = incidentList[i].Technician;
                        array[5] = incidentList[i].Description;
                        var item = new ListViewItem(array);
                        listView1.Items.Add(item);


                    }

                }
                else
                {
                    MessageBox.Show("All invoices are paid in full.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
    }
}
                




