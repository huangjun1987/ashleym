﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for product
    /// </summary>
   public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
