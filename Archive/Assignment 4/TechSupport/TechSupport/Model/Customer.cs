﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for customer
    /// </summary>
   public class Customer
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }
    }
}
