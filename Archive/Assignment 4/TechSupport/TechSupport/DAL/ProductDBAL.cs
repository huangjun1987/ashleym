﻿using System.Collections.Generic;
using TechSupport.Model;
using System.Data.SqlClient;
using System;

namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database products.
    public class ProductDBAL
    {
        /// <summary>
        /// This method gets a list from the database of incidents that are open.
        /// </summary>
        /// <returns>A list of open incidents</returns>
        public List<Product> GetProducts()
        {
            List<Product> productList = new List<Product>();

            string selectStatement =
                "SELECT p.Id, p.Name " +
                "FROM dbo.Products p ";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                           Product product = new Product();
                            product.Id = int.Parse(reader["Id"].ToString());
                            product.Name = reader["Name"].ToString();
                            
                            productList.Add(product);
                        }
                    }
                }

            }
            return productList;


        }


    }
}


