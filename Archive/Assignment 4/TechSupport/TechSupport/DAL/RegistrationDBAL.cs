﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechSupport.Model;

namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database customers.
    public class RegistrationDBAL
    {
            /// <summary>
            /// This method gets a list from the database of registered products.
            /// </summary>
            /// <returns>A list of registrations</returns>
            public List<Registration> GetRegistration()
            {
                List<Registration> registrationList = new List<Registration>();

                string selectStatement =
                    "SELECT r.CustomerID, r.ProductCode " +
                    "FROM dbo.Registration r ";


                using (SqlConnection connection = TechSupportDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Registration registration = new Registration();
                                registration.CustomerID = reader.GetInt32(reader.GetOrdinal("CustomerID"));
                                registration.ProductCode = reader.GetInt32(reader.GetOrdinal("ProductCode"));

                            registrationList.Add(registration);
                            }
                        }
                    }

                }
                return registrationList;


            }



        
    }
}
