﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TechSupport.Model;

namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database incidents.
    public class IncidentDBAL
    {
        /// <summary>
        /// This method gets a list from the database of incidents that are open.
        /// </summary>
        /// <returns>A list of open incidents</returns>
        public List<OpenIncident> GetOpenIncidents()
        {
            List<OpenIncident> incidentList = new List<OpenIncident>();

            string selectStatement =
                "SELECT i.ProductCode, Convert(varchar(10), i.DateOpened, 120) AS 'Date', c.Name AS 'Customer', t.Name AS 'Technician', i.Title, i.Description " +
                "FROM dbo.Incidents i " +
                "INNER JOIN dbo.Customers c ON c.CustomerID = i.CustomerID " +
                "LEFT JOIN dbo.Technicians t ON t.TechID = i.TechID " +
                "WHERE i.DateClosed IS NULL";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            OpenIncident openIncident = new OpenIncident();
                            openIncident.ProductCode = reader["ProductCode"].ToString();
                            openIncident.DateOpened = reader["Date"].ToString();
                            openIncident.Customer = reader["Customer"].ToString(); ;
                            openIncident.Technician = reader["Technician"].ToString();
                            openIncident.Title = reader["Title"].ToString();
                            openIncident.Description = reader["Description"].ToString();

                            incidentList.Add(openIncident);
                        }
                    }
                }

            }
            return incidentList;


        }

        public void AddIncident(Incident incident)
        {
            string insertStatement =
                @"INSERT INTO [dbo].[Incidents] ([CustomerID] ,[TechID] ,[ProductCode] 
,[DateOpened] ,[DateClosed] ,[Title],[Description]) VALUES(@param1,@param2,@param3, @param4, @param5, @param6, @param7)";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(insertStatement, connection))
                {
                    cmd.Parameters.AddWithValue("@param1", incident.CustomerID);
                    cmd.Parameters.AddWithValue("@param2", DBNull.Value);
                    cmd.Parameters.AddWithValue("@param3", incident.ProductCode);
                    cmd.Parameters.AddWithValue("@param4", DateTime.Now);
                    cmd.Parameters.AddWithValue("@param5", DBNull.Value);
                    cmd.Parameters.AddWithValue("@param6", incident.Title);
                    cmd.Parameters.AddWithValue("@param7", incident.Description);

                    cmd.ExecuteNonQuery();

                    connection.Close();
                }

            }


        }
    }
}

