﻿using System.Data.SqlClient;

namespace TechSupport.DAL
{
    /// <summary>
    /// This class sets up getting the connection for the techSupport database
    /// </summary>
    public static class TechSupportDBConnection
    {
        public static SqlConnection GetConnection()
        {
            string connectionString =
                "Data Source=localhost;Initial Catalog=TechSupport;" +
                "Integrated Security=True";


            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }
    }
}
