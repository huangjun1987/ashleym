﻿namespace TechSupport.View
{
    partial class MainDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMainForm = new System.Windows.Forms.TabControl();
            this.addIncidentTab = new System.Windows.Forms.TabPage();
            this.addIncidentUserControl1 = new TechSupport.UserControls.AddIncidentUserControl();
            this.loadAllIncidentsTab = new System.Windows.Forms.TabPage();
            this.dataGridViewIncidents = new System.Windows.Forms.DataGridView();
            this.searchIncidentsTab = new System.Windows.Forms.TabPage();
            this.searchUserControl1 = new TechSupport.UserControls.SearchUserControl();
            this.Display_Open_Incidents = new System.Windows.Forms.TabPage();
            this.displayOpenIncidentUserControl1 = new TechSupport.UserControls.DisplayOpenIncidentUserControl();
            this.userNameLabelMainDash = new System.Windows.Forms.Label();
            this.Logout = new System.Windows.Forms.LinkLabel();
            this.tabControlMainForm.SuspendLayout();
            this.addIncidentTab.SuspendLayout();
            this.loadAllIncidentsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIncidents)).BeginInit();
            this.searchIncidentsTab.SuspendLayout();
            this.Display_Open_Incidents.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlMainForm
            // 
            this.tabControlMainForm.Controls.Add(this.addIncidentTab);
            this.tabControlMainForm.Controls.Add(this.loadAllIncidentsTab);
            this.tabControlMainForm.Controls.Add(this.searchIncidentsTab);
            this.tabControlMainForm.Controls.Add(this.Display_Open_Incidents);
            this.tabControlMainForm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControlMainForm.Location = new System.Drawing.Point(0, 144);
            this.tabControlMainForm.Name = "tabControlMainForm";
            this.tabControlMainForm.SelectedIndex = 0;
            this.tabControlMainForm.Size = new System.Drawing.Size(974, 371);
            this.tabControlMainForm.TabIndex = 2;
            this.tabControlMainForm.SelectedIndexChanged += new System.EventHandler(this.tabControlMainForm_SelectedIndexChanged);
            // 
            // addIncidentTab
            // 
            this.addIncidentTab.Controls.Add(this.addIncidentUserControl1);
            this.addIncidentTab.Location = new System.Drawing.Point(4, 25);
            this.addIncidentTab.Name = "addIncidentTab";
            this.addIncidentTab.Padding = new System.Windows.Forms.Padding(3);
            this.addIncidentTab.Size = new System.Drawing.Size(792, 342);
            this.addIncidentTab.TabIndex = 0;
            this.addIncidentTab.Text = "Add Incident";
            this.addIncidentTab.UseVisualStyleBackColor = true;
            // 
            // addIncidentUserControl1
            // 
            this.addIncidentUserControl1.Location = new System.Drawing.Point(22, 27);
            this.addIncidentUserControl1.Name = "addIncidentUserControl1";
            this.addIncidentUserControl1.Size = new System.Drawing.Size(751, 307);
            this.addIncidentUserControl1.TabIndex = 0;
            // 
            // loadAllIncidentsTab
            // 
            this.loadAllIncidentsTab.Controls.Add(this.dataGridViewIncidents);
            this.loadAllIncidentsTab.Location = new System.Drawing.Point(4, 25);
            this.loadAllIncidentsTab.Name = "loadAllIncidentsTab";
            this.loadAllIncidentsTab.Padding = new System.Windows.Forms.Padding(3);
            this.loadAllIncidentsTab.Size = new System.Drawing.Size(792, 342);
            this.loadAllIncidentsTab.TabIndex = 1;
            this.loadAllIncidentsTab.Text = "Load All Incidents";
            this.loadAllIncidentsTab.UseVisualStyleBackColor = true;
            // 
            // dataGridViewIncidents
            // 
            this.dataGridViewIncidents.AllowUserToAddRows = false;
            this.dataGridViewIncidents.AllowUserToDeleteRows = false;
            this.dataGridViewIncidents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIncidents.Location = new System.Drawing.Point(106, 51);
            this.dataGridViewIncidents.Name = "dataGridViewIncidents";
            this.dataGridViewIncidents.ReadOnly = true;
            this.dataGridViewIncidents.RowHeadersWidth = 51;
            this.dataGridViewIncidents.RowTemplate.Height = 24;
            this.dataGridViewIncidents.Size = new System.Drawing.Size(580, 241);
            this.dataGridViewIncidents.TabIndex = 5;
            // 
            // searchIncidentsTab
            // 
            this.searchIncidentsTab.Controls.Add(this.searchUserControl1);
            this.searchIncidentsTab.Location = new System.Drawing.Point(4, 25);
            this.searchIncidentsTab.Name = "searchIncidentsTab";
            this.searchIncidentsTab.Size = new System.Drawing.Size(792, 342);
            this.searchIncidentsTab.TabIndex = 2;
            this.searchIncidentsTab.Text = "Search Incident";
            this.searchIncidentsTab.UseVisualStyleBackColor = true;
            // 
            // searchUserControl1
            // 
            this.searchUserControl1.Location = new System.Drawing.Point(8, 12);
            this.searchUserControl1.Name = "searchUserControl1";
            this.searchUserControl1.Size = new System.Drawing.Size(788, 345);
            this.searchUserControl1.TabIndex = 0;
            // 
            // Display_Open_Incidents
            // 
            this.Display_Open_Incidents.Controls.Add(this.displayOpenIncidentUserControl1);
            this.Display_Open_Incidents.Location = new System.Drawing.Point(4, 25);
            this.Display_Open_Incidents.Name = "Display_Open_Incidents";
            this.Display_Open_Incidents.Padding = new System.Windows.Forms.Padding(3);
            this.Display_Open_Incidents.Size = new System.Drawing.Size(966, 342);
            this.Display_Open_Incidents.TabIndex = 3;
            this.Display_Open_Incidents.Text = "Display Open Incidents";
            this.Display_Open_Incidents.UseVisualStyleBackColor = true;
            // 
            // displayOpenIncidentUserControl1
            // 
            this.displayOpenIncidentUserControl1.Location = new System.Drawing.Point(-4, 3);
            this.displayOpenIncidentUserControl1.Name = "displayOpenIncidentUserControl1";
            this.displayOpenIncidentUserControl1.Size = new System.Drawing.Size(964, 343);
            this.displayOpenIncidentUserControl1.TabIndex = 0;
            // 
            // userNameLabelMainDash
            // 
            this.userNameLabelMainDash.AutoSize = true;
            this.userNameLabelMainDash.Location = new System.Drawing.Point(693, 21);
            this.userNameLabelMainDash.Name = "userNameLabelMainDash";
            this.userNameLabelMainDash.Size = new System.Drawing.Size(0, 17);
            this.userNameLabelMainDash.TabIndex = 0;
            // 
            // Logout
            // 
            this.Logout.AutoSize = true;
            this.Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Logout.Location = new System.Drawing.Point(676, 51);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(72, 25);
            this.Logout.TabIndex = 1;
            this.Logout.TabStop = true;
            this.Logout.Text = "Logout";
            this.Logout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Logout_LinkClicked);
            // 
            // MainDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 515);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.userNameLabelMainDash);
            this.Controls.Add(this.tabControlMainForm);
            this.Name = "MainDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Dashboard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDashboard_FormClosing);
            this.Load += new System.EventHandler(this.MainDashboard_Load);
            this.tabControlMainForm.ResumeLayout(false);
            this.addIncidentTab.ResumeLayout(false);
            this.loadAllIncidentsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIncidents)).EndInit();
            this.searchIncidentsTab.ResumeLayout(false);
            this.Display_Open_Incidents.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMainForm;
        private System.Windows.Forms.TabPage addIncidentTab;
        private System.Windows.Forms.TabPage loadAllIncidentsTab;
        private System.Windows.Forms.TabPage searchIncidentsTab;
        private System.Windows.Forms.Label userNameLabelMainDash;
        private System.Windows.Forms.LinkLabel Logout;
      
        private UserControls.AddIncidentUserControl addIncidentUserControl1;
        private System.Windows.Forms.DataGridView dataGridViewIncidents;
        private UserControls.SearchUserControl searchUserControl1;
        private System.Windows.Forms.TabPage Display_Open_Incidents;
        private UserControls.DisplayOpenIncidentUserControl displayOpenIncidentUserControl1;
    }
}