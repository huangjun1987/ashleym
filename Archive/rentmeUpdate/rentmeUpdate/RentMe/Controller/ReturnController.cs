﻿using RentMe.DAL;
using RentMe.Model;
using System;
using System.Collections.Generic;

namespace RentMe.Controller
{
    class ReturnController
    {
        private ReturnDAL returnDAL;

        public ReturnController()
        {
            this.returnDAL = new ReturnDAL();
        }

        

       

        internal void CreateReturnItem(ReturnItem returnItem)
        {
            this.returnDAL.CreateReturnItem(returnItem);
        }

        internal List<ReturnableItem> GetReturnableItemList(int rentalID)
        {
            return returnDAL.GetReturnableItemList(rentalID);
        }
    }
}
