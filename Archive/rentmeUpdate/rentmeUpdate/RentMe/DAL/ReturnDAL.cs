﻿using RentMe.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace RentMe.DAL
{
    class ReturnDAL
    {
        public void CreateReturnItem(ReturnItem returnItem)

        {

            string insertStatement =
           @"
INSERT INTO dbo.Returns
            (Serial#
             , RentalID
             , ReturnID
             , Quantity
             , FineDue
             , RefundDue) 
			 select         
           @serial#
           ,@rentalID
           ,@returnID
           ,@quantity
           ,@fineDue
           ,@refundDue
            where NOT EXISTS ((select 1
from RentedItem i
where i.RentalID = @rentalID and i.Serial# = @serial# and @quantity>
i.Quantity- coalesce(
(select sum(Quantity) from Returns
where Serial#=i.Serial# and RentalID=i.RentalID)
,0)
))
";


            using (SqlConnection connection = RentMeDBConnection.GetConnection())

            {

                connection.Open();

                using (SqlCommand cmd = new SqlCommand(insertStatement, connection))

                {
                    cmd.Parameters.AddWithValue("@serial#", returnItem.SerialNumber);
                    cmd.Parameters.AddWithValue("@rentalID", returnItem.RentalID);
                    cmd.Parameters.AddWithValue("@returnID", returnItem.ReturnID);
                    cmd.Parameters.AddWithValue("@quantity", returnItem.Quantity);
                    cmd.Parameters.AddWithValue("@fineDue", returnItem.FineDue);
                    cmd.Parameters.AddWithValue("@refundDue", returnItem.RefundDue);
                    
                    cmd.ExecuteNonQuery();
                    connection.Close();

                }
            }

        }

        internal List<ReturnableItem> GetReturnableItemList(int rentalID)
        {
            var list = new List<ReturnableItem>();
            string statement =
          @"select i.Serial#
,i.Quantity- coalesce(
(select sum(Quantity) from Returns
where Serial#=i.Serial# and RentalID=i.RentalID)
,0) as Quantity
from RentedItem i
where i.RentalID = @rentalID
";
            using (SqlConnection connection = RentMeDBConnection.GetConnection())
            {

                connection.Open();

                using (SqlCommand cmd = new SqlCommand(statement, connection))
                {
                    cmd.Parameters.AddWithValue("@rentalID", rentalID);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var item = new ReturnableItem();
                            item.Quantity = (int)reader["Quantity"];
                            item.SerialNumber = reader["Serial#"].ToString();
                            list.Add(item);
                        }
                    }

                    connection.Close();
                }
            }
            return list;
        }
    }
}

