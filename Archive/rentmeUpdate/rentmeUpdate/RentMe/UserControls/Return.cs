﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RentMe.Controller;
using RentMe.Model;

namespace RentMe.UserControls
{
    public partial class Return : UserControl
    {
        ReturnController returnController;
        ReturnTransactionController returnTransactionController;
        FurnitureController furnitureController;
        RentalTransactionController rentalTransactionController;
        EmployeeController employeeController;
        int rentalID;
        List<ReturnableItem> returnableItemList;
        List<ReturnableItem> toReturnItemList;
        public Return()
        {
            InitializeComponent();
            this.returnController = new ReturnController();
            this.returnTransactionController = new ReturnTransactionController();
            this.furnitureController = new FurnitureController();
            rentalTransactionController = new RentalTransactionController();
            employeeController = new EmployeeController();
            var employeeList = new List<Employee>();
            var emptyEmployee = new Employee();
            emptyEmployee.Fullname = "--select--";
            employeeList.Add(emptyEmployee);
            employeeList.AddRange(employeeController.ListEmployees());
            comboBoxReturnEmployee.DataSource = employeeList;
            comboBoxReturnEmployee.DisplayMember = "Fullname";
            comboBoxReturnEmployee.ValueMember = "EmployeeID";
        }
        private void buttonReturn_Click(object sender, EventArgs e)
        {
            var employeeID  = int.Parse(comboBoxReturnEmployee.SelectedValue.ToString());
            if (employeeID <1)
            {
                MessageBox.Show("Please select an employee");
                return;
            }
            var returnItem = new ReturnItem();
            returnItem.ReturnID = 1001;
            returnItem.RentalID = 1001;
            returnItem.SerialNumber = "Bgeilv5c";
            returnItem.Quantity = employeeID;
            returnController.CreateReturnItem(returnItem);
            MessageBox.Show("Successfully Returned Furniture Items");
            ///this.Clear();
        }
        private void buttonReturn_Click2(object sender, EventArgs e)
        {
            returnableItemList = returnController.GetReturnableItemList(rentalID);
            foreach (var item in toReturnItemList)
            {
                var maxQuantity = returnableItemList.Where(r => r.SerialNumber.Equals(item.SerialNumber)).Select(r => r.Quantity).FirstOrDefault();
                if (item.Quantity > maxQuantity)
                {
                    MessageBox.Show("Invliad quantity.");
                    return;
                }
            }
            ReturnTransaction transaction = new ReturnTransaction();
            transaction.EmployeeID = int.Parse(comboBoxReturnEmployee.SelectedValue.ToString());
            transaction.ReturnDate = DateTime.Now;
            int returnID = returnTransactionController.CreateReturnTransaction(transaction);


            foreach (var toReturnItem in toReturnItemList)
            {
                var returnItem = new ReturnItem();
                returnItem.ReturnID = returnID;
                returnItem.RentalID = rentalID;

                var furnitures = furnitureController.GetFurnituresBySerialNumber(returnItem.SerialNumber);
                var itemScheduledReturn = rentalTransactionController.GetRentalTransactionByRentalID(returnItem.RentalID);

                var daysLate = DayNum(DateTime.Now.ToString(), itemScheduledReturn.ScheduledReturn.ToString());
                var daysEarly = DayNum(itemScheduledReturn.ScheduledReturn.ToString(), DateTime.Now.ToString());

                if (furnitures.Count > 0)
                {
                    var furniture = furnitures.FirstOrDefault();
                    int resultDiff = DateTime.Compare(DateTime.Now, itemScheduledReturn.ScheduledReturn);
                    if (resultDiff < 0)
                    {
                        returnItem.RefundDue = Convert.ToDecimal(furniture.DailyRentalRate * daysEarly);
                        returnItem.FineDue = 0;
                    }
                    else if (resultDiff == 0)
                    {
                        returnItem.RefundDue = 0;
                        returnItem.FineDue = 0;
                    }
                    else
                    {
                        returnItem.FineDue = Convert.ToDecimal((furniture.FineRate + furniture.DailyRentalRate) * daysLate);
                        returnItem.RefundDue = 0;
                    }


                }
                returnController.CreateReturnItem(returnItem);
            }


            ///When add more items need to change the logic for more than one item.
            //transaction.FineDueTotal = returnItem.FineDue;
            //transaction.RefundDueTotal = returnItem.RefundDue;
            returnTransactionController.UpdateTransaction(transaction);
            MessageBox.Show("Successfully Returned Furniture Items");
            ///this.Clear();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Clear();
        }

        static int DayNum(string date, string startDate)
        {
            DateTime start = DateTime.Parse(startDate);
            DateTime dt = DateTime.Parse(date);
            TimeSpan t = dt - start;
            return (int)t.TotalDays;
        }

        public void Clear()
        {
            textBoxRentalID.Text = "";
            
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            rentalID = int.Parse(textBoxRentalID.Text);
            toReturnItemList = returnController.GetReturnableItemList(rentalID);
            dataGridViewReturnableItems.DataSource = toReturnItemList;
            dataGridViewReturnableItems.Columns[0].ReadOnly = true;
            dataGridViewReturnableItems.Refresh();
        }

        private void Return_Load(object sender, EventArgs e)
        {

        }
    }
}
