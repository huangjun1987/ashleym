﻿namespace RentMe.UserControls
{
    partial class EditStoreMember
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label7 = new System.Windows.Forms.Label();
            this.CustomerIDSearchTextBox = new System.Windows.Forms.TextBox();
            this.CustomerIDSearchButton = new System.Windows.Forms.Button();
            this.PhoneNumberSearchTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PhoneNumberSearchButton = new System.Windows.Forms.Button();
            this.FirstNameSearchTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LastNameSearchTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NameSearchButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.CustomerIDLabel = new System.Windows.Forms.Label();
            this.LastNameTextBox = new System.Windows.Forms.TextBox();
            this.FirstNameTextBox = new System.Windows.Forms.TextBox();
            this.PhoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.Address1TextBox = new System.Windows.Forms.TextBox();
            this.Address2TextBox = new System.Windows.Forms.TextBox();
            this.DateOfBirthPicker = new System.Windows.Forms.DateTimePicker();
            this.DateOfBirthLabel = new System.Windows.Forms.Label();
            this.ZipCodeTextBox = new System.Windows.Forms.TextBox();
            this.ZipCodeLabel = new System.Windows.Forms.Label();
            this.StateComboBox = new System.Windows.Forms.ComboBox();
            this.StateLabel = new System.Windows.Forms.Label();
            this.CityTextBox = new System.Windows.Forms.TextBox();
            this.CityLabel = new System.Windows.Forms.Label();
            this.Address2Label = new System.Windows.Forms.Label();
            this.Address1Label = new System.Windows.Forms.Label();
            this.PhoneLabel = new System.Windows.Forms.Label();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.FirstNameLabel = new System.Windows.Forms.Label();
            this.SaveChangesButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.rentalsAndReturnsTabControl = new System.Windows.Forms.TabControl();
            this.rentalsTabPage = new System.Windows.Forms.TabPage();
            this.returnsTabPage = new System.Windows.Forms.TabPage();
            this.viewRentalsUserControl = new RentMe.UserControls.ViewRentals();
            this.viewReturnsUserControl = new RentMe.UserControls.ViewReturns();
            this.rentalsAndReturnsTabControl.SuspendLayout();
            this.rentalsTabPage.SuspendLayout();
            this.returnsTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(107, 26);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(68, 13);
            this.Label7.TabIndex = 0;
            this.Label7.Text = "Customer ID:";
            // 
            // CustomerIDSearchTextBox
            // 
            this.CustomerIDSearchTextBox.Location = new System.Drawing.Point(191, 23);
            this.CustomerIDSearchTextBox.Name = "CustomerIDSearchTextBox";
            this.CustomerIDSearchTextBox.Size = new System.Drawing.Size(187, 20);
            this.CustomerIDSearchTextBox.TabIndex = 1;
            // 
            // CustomerIDSearchButton
            // 
            this.CustomerIDSearchButton.Location = new System.Drawing.Point(384, 21);
            this.CustomerIDSearchButton.Name = "CustomerIDSearchButton";
            this.CustomerIDSearchButton.Size = new System.Drawing.Size(96, 23);
            this.CustomerIDSearchButton.TabIndex = 2;
            this.CustomerIDSearchButton.Text = "Search";
            this.CustomerIDSearchButton.UseVisualStyleBackColor = true;
            this.CustomerIDSearchButton.Click += new System.EventHandler(this.CustomerIDSearchButton_Click);
            // 
            // PhoneNumberSearchTextBox
            // 
            this.PhoneNumberSearchTextBox.Location = new System.Drawing.Point(191, 49);
            this.PhoneNumberSearchTextBox.Name = "PhoneNumberSearchTextBox";
            this.PhoneNumberSearchTextBox.Size = new System.Drawing.Size(187, 20);
            this.PhoneNumberSearchTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(107, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Phone No:";
            // 
            // PhoneNumberSearchButton
            // 
            this.PhoneNumberSearchButton.Location = new System.Drawing.Point(384, 47);
            this.PhoneNumberSearchButton.Name = "PhoneNumberSearchButton";
            this.PhoneNumberSearchButton.Size = new System.Drawing.Size(96, 23);
            this.PhoneNumberSearchButton.TabIndex = 4;
            this.PhoneNumberSearchButton.Text = "Search";
            this.PhoneNumberSearchButton.UseVisualStyleBackColor = true;
            this.PhoneNumberSearchButton.Click += new System.EventHandler(this.PhoneNumberSearchButton_Click);
            // 
            // FirstNameSearchTextBox
            // 
            this.FirstNameSearchTextBox.Location = new System.Drawing.Point(191, 75);
            this.FirstNameSearchTextBox.Name = "FirstNameSearchTextBox";
            this.FirstNameSearchTextBox.Size = new System.Drawing.Size(187, 20);
            this.FirstNameSearchTextBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "First Name:";
            // 
            // LastNameSearchTextBox
            // 
            this.LastNameSearchTextBox.Location = new System.Drawing.Point(191, 101);
            this.LastNameSearchTextBox.Name = "LastNameSearchTextBox";
            this.LastNameSearchTextBox.Size = new System.Drawing.Size(187, 20);
            this.LastNameSearchTextBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Last Name:";
            // 
            // NameSearchButton
            // 
            this.NameSearchButton.Location = new System.Drawing.Point(384, 99);
            this.NameSearchButton.Name = "NameSearchButton";
            this.NameSearchButton.Size = new System.Drawing.Size(96, 23);
            this.NameSearchButton.TabIndex = 7;
            this.NameSearchButton.Text = "Search";
            this.NameSearchButton.UseVisualStyleBackColor = true;
            this.NameSearchButton.Click += new System.EventHandler(this.NameSearchButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(107, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Customer ID:";
            // 
            // CustomerIDLabel
            // 
            this.CustomerIDLabel.AutoSize = true;
            this.CustomerIDLabel.Location = new System.Drawing.Point(188, 145);
            this.CustomerIDLabel.Name = "CustomerIDLabel";
            this.CustomerIDLabel.Size = new System.Drawing.Size(0, 13);
            this.CustomerIDLabel.TabIndex = 12;
            // 
            // LastNameTextBox
            // 
            this.LastNameTextBox.Location = new System.Drawing.Point(191, 194);
            this.LastNameTextBox.Name = "LastNameTextBox";
            this.LastNameTextBox.Size = new System.Drawing.Size(187, 20);
            this.LastNameTextBox.TabIndex = 9;
            // 
            // FirstNameTextBox
            // 
            this.FirstNameTextBox.Location = new System.Drawing.Point(191, 168);
            this.FirstNameTextBox.Name = "FirstNameTextBox";
            this.FirstNameTextBox.Size = new System.Drawing.Size(187, 20);
            this.FirstNameTextBox.TabIndex = 8;
            // 
            // PhoneNumberTextBox
            // 
            this.PhoneNumberTextBox.Location = new System.Drawing.Point(191, 220);
            this.PhoneNumberTextBox.Name = "PhoneNumberTextBox";
            this.PhoneNumberTextBox.Size = new System.Drawing.Size(187, 20);
            this.PhoneNumberTextBox.TabIndex = 10;
            // 
            // Address1TextBox
            // 
            this.Address1TextBox.Location = new System.Drawing.Point(479, 142);
            this.Address1TextBox.Name = "Address1TextBox";
            this.Address1TextBox.Size = new System.Drawing.Size(187, 20);
            this.Address1TextBox.TabIndex = 12;
            // 
            // Address2TextBox
            // 
            this.Address2TextBox.Location = new System.Drawing.Point(479, 168);
            this.Address2TextBox.Name = "Address2TextBox";
            this.Address2TextBox.Size = new System.Drawing.Size(187, 20);
            this.Address2TextBox.TabIndex = 13;
            // 
            // DateOfBirthPicker
            // 
            this.DateOfBirthPicker.Location = new System.Drawing.Point(191, 247);
            this.DateOfBirthPicker.Name = "DateOfBirthPicker";
            this.DateOfBirthPicker.Size = new System.Drawing.Size(187, 20);
            this.DateOfBirthPicker.TabIndex = 11;
            // 
            // DateOfBirthLabel
            // 
            this.DateOfBirthLabel.AutoSize = true;
            this.DateOfBirthLabel.Location = new System.Drawing.Point(107, 250);
            this.DateOfBirthLabel.Name = "DateOfBirthLabel";
            this.DateOfBirthLabel.Size = new System.Drawing.Size(78, 13);
            this.DateOfBirthLabel.TabIndex = 29;
            this.DateOfBirthLabel.Text = "Date Of Birth: *";
            // 
            // ZipCodeTextBox
            // 
            this.ZipCodeTextBox.Location = new System.Drawing.Point(479, 247);
            this.ZipCodeTextBox.Name = "ZipCodeTextBox";
            this.ZipCodeTextBox.Size = new System.Drawing.Size(187, 20);
            this.ZipCodeTextBox.TabIndex = 16;
            // 
            // ZipCodeLabel
            // 
            this.ZipCodeLabel.AutoSize = true;
            this.ZipCodeLabel.Location = new System.Drawing.Point(409, 250);
            this.ZipCodeLabel.Name = "ZipCodeLabel";
            this.ZipCodeLabel.Size = new System.Drawing.Size(60, 13);
            this.ZipCodeLabel.TabIndex = 28;
            this.ZipCodeLabel.Text = "Zip Code: *";
            // 
            // StateComboBox
            // 
            this.StateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StateComboBox.FormattingEnabled = true;
            this.StateComboBox.Location = new System.Drawing.Point(479, 220);
            this.StateComboBox.Name = "StateComboBox";
            this.StateComboBox.Size = new System.Drawing.Size(187, 21);
            this.StateComboBox.TabIndex = 15;
            // 
            // StateLabel
            // 
            this.StateLabel.AutoSize = true;
            this.StateLabel.Location = new System.Drawing.Point(409, 223);
            this.StateLabel.Name = "StateLabel";
            this.StateLabel.Size = new System.Drawing.Size(42, 13);
            this.StateLabel.TabIndex = 27;
            this.StateLabel.Text = "State: *";
            // 
            // CityTextBox
            // 
            this.CityTextBox.Location = new System.Drawing.Point(479, 194);
            this.CityTextBox.Name = "CityTextBox";
            this.CityTextBox.Size = new System.Drawing.Size(187, 20);
            this.CityTextBox.TabIndex = 14;
            // 
            // CityLabel
            // 
            this.CityLabel.AutoSize = true;
            this.CityLabel.Location = new System.Drawing.Point(409, 197);
            this.CityLabel.Name = "CityLabel";
            this.CityLabel.Size = new System.Drawing.Size(34, 13);
            this.CityLabel.TabIndex = 26;
            this.CityLabel.Text = "City: *";
            // 
            // Address2Label
            // 
            this.Address2Label.AutoSize = true;
            this.Address2Label.Location = new System.Drawing.Point(409, 171);
            this.Address2Label.Name = "Address2Label";
            this.Address2Label.Size = new System.Drawing.Size(57, 13);
            this.Address2Label.TabIndex = 35;
            this.Address2Label.Text = "Address 2:";
            // 
            // Address1Label
            // 
            this.Address1Label.AutoSize = true;
            this.Address1Label.Location = new System.Drawing.Point(409, 144);
            this.Address1Label.Name = "Address1Label";
            this.Address1Label.Size = new System.Drawing.Size(64, 13);
            this.Address1Label.TabIndex = 34;
            this.Address1Label.Text = "Address 1: *";
            // 
            // PhoneLabel
            // 
            this.PhoneLabel.AutoSize = true;
            this.PhoneLabel.Location = new System.Drawing.Point(107, 223);
            this.PhoneLabel.Name = "PhoneLabel";
            this.PhoneLabel.Size = new System.Drawing.Size(48, 13);
            this.PhoneLabel.TabIndex = 33;
            this.PhoneLabel.Text = "Phone: *";
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.AutoSize = true;
            this.LastNameLabel.Location = new System.Drawing.Point(107, 197);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(68, 13);
            this.LastNameLabel.TabIndex = 32;
            this.LastNameLabel.Text = "Last Name: *";
            // 
            // FirstNameLabel
            // 
            this.FirstNameLabel.AutoSize = true;
            this.FirstNameLabel.Location = new System.Drawing.Point(107, 171);
            this.FirstNameLabel.Name = "FirstNameLabel";
            this.FirstNameLabel.Size = new System.Drawing.Size(67, 13);
            this.FirstNameLabel.TabIndex = 31;
            this.FirstNameLabel.Text = "First Name: *";
            // 
            // SaveChangesButton
            // 
            this.SaveChangesButton.Location = new System.Drawing.Point(479, 273);
            this.SaveChangesButton.Name = "SaveChangesButton";
            this.SaveChangesButton.Size = new System.Drawing.Size(90, 26);
            this.SaveChangesButton.TabIndex = 17;
            this.SaveChangesButton.Text = "Save Changes";
            this.SaveChangesButton.UseVisualStyleBackColor = true;
            this.SaveChangesButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(576, 273);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(90, 26);
            this.ClearButton.TabIndex = 18;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // rentalsAndReturnsTabControl
            // 
            this.rentalsAndReturnsTabControl.Controls.Add(this.rentalsTabPage);
            this.rentalsAndReturnsTabControl.Controls.Add(this.returnsTabPage);
            this.rentalsAndReturnsTabControl.Location = new System.Drawing.Point(3, 305);
            this.rentalsAndReturnsTabControl.Name = "rentalsAndReturnsTabControl";
            this.rentalsAndReturnsTabControl.SelectedIndex = 0;
            this.rentalsAndReturnsTabControl.Size = new System.Drawing.Size(766, 314);
            this.rentalsAndReturnsTabControl.TabIndex = 36;
            this.rentalsAndReturnsTabControl.TabStop = false;
            // 
            // rentalsTabPage
            // 
            this.rentalsTabPage.Controls.Add(this.viewRentalsUserControl);
            this.rentalsTabPage.Location = new System.Drawing.Point(4, 22);
            this.rentalsTabPage.Name = "rentalsTabPage";
            this.rentalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.rentalsTabPage.Size = new System.Drawing.Size(758, 288);
            this.rentalsTabPage.TabIndex = 0;
            this.rentalsTabPage.Text = "Rentals";
            this.rentalsTabPage.UseVisualStyleBackColor = true;
            // 
            // returnsTabPage
            // 
            this.returnsTabPage.Controls.Add(this.viewReturnsUserControl);
            this.returnsTabPage.Location = new System.Drawing.Point(4, 22);
            this.returnsTabPage.Name = "returnsTabPage";
            this.returnsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.returnsTabPage.Size = new System.Drawing.Size(758, 288);
            this.returnsTabPage.TabIndex = 1;
            this.returnsTabPage.Text = "Returns";
            this.returnsTabPage.UseVisualStyleBackColor = true;
            // 
            // viewRentalsUserControl
            // 
            this.viewRentalsUserControl.Location = new System.Drawing.Point(0, 0);
            this.viewRentalsUserControl.Name = "viewRentalsUserControl";
            this.viewRentalsUserControl.Size = new System.Drawing.Size(758, 288);
            this.viewRentalsUserControl.TabIndex = 0;
            // 
            // viewReturnsUserControl
            // 
            this.viewReturnsUserControl.Location = new System.Drawing.Point(0, 0);
            this.viewReturnsUserControl.Name = "viewReturnsUserControl";
            this.viewReturnsUserControl.Size = new System.Drawing.Size(758, 288);
            this.viewReturnsUserControl.TabIndex = 0;
            // 
            // EditStoreMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rentalsAndReturnsTabControl);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.SaveChangesButton);
            this.Controls.Add(this.Address2Label);
            this.Controls.Add(this.Address1Label);
            this.Controls.Add(this.PhoneLabel);
            this.Controls.Add(this.LastNameLabel);
            this.Controls.Add(this.FirstNameLabel);
            this.Controls.Add(this.DateOfBirthPicker);
            this.Controls.Add(this.DateOfBirthLabel);
            this.Controls.Add(this.ZipCodeTextBox);
            this.Controls.Add(this.ZipCodeLabel);
            this.Controls.Add(this.StateComboBox);
            this.Controls.Add(this.StateLabel);
            this.Controls.Add(this.CityTextBox);
            this.Controls.Add(this.CityLabel);
            this.Controls.Add(this.Address2TextBox);
            this.Controls.Add(this.Address1TextBox);
            this.Controls.Add(this.PhoneNumberTextBox);
            this.Controls.Add(this.LastNameTextBox);
            this.Controls.Add(this.FirstNameTextBox);
            this.Controls.Add(this.CustomerIDLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.NameSearchButton);
            this.Controls.Add(this.LastNameSearchTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.FirstNameSearchTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PhoneNumberSearchButton);
            this.Controls.Add(this.PhoneNumberSearchTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CustomerIDSearchButton);
            this.Controls.Add(this.CustomerIDSearchTextBox);
            this.Controls.Add(this.Label7);
            this.Name = "EditStoreMember";
            this.Size = new System.Drawing.Size(772, 622);
            this.rentalsAndReturnsTabControl.ResumeLayout(false);
            this.rentalsTabPage.ResumeLayout(false);
            this.returnsTabPage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label7;
        private System.Windows.Forms.TextBox CustomerIDSearchTextBox;
        private System.Windows.Forms.Button CustomerIDSearchButton;
        private System.Windows.Forms.TextBox PhoneNumberSearchTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button PhoneNumberSearchButton;
        private System.Windows.Forms.TextBox FirstNameSearchTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LastNameSearchTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button NameSearchButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label CustomerIDLabel;
        private System.Windows.Forms.TextBox LastNameTextBox;
        private System.Windows.Forms.TextBox FirstNameTextBox;
        private System.Windows.Forms.TextBox PhoneNumberTextBox;
        private System.Windows.Forms.TextBox Address1TextBox;
        private System.Windows.Forms.TextBox Address2TextBox;
        private System.Windows.Forms.DateTimePicker DateOfBirthPicker;
        private System.Windows.Forms.Label DateOfBirthLabel;
        private System.Windows.Forms.TextBox ZipCodeTextBox;
        private System.Windows.Forms.Label ZipCodeLabel;
        private System.Windows.Forms.ComboBox StateComboBox;
        private System.Windows.Forms.Label StateLabel;
        private System.Windows.Forms.TextBox CityTextBox;
        private System.Windows.Forms.Label CityLabel;
        private System.Windows.Forms.Label Address2Label;
        private System.Windows.Forms.Label Address1Label;
        private System.Windows.Forms.Label PhoneLabel;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.Label FirstNameLabel;
        private System.Windows.Forms.Button SaveChangesButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.TabControl rentalsAndReturnsTabControl;
        private System.Windows.Forms.TabPage rentalsTabPage;
        private System.Windows.Forms.TabPage returnsTabPage;
        private ViewRentals viewRentalsUserControl;
        private ViewReturns viewReturnsUserControl;
    }
}
