﻿namespace RentMe.UserControls
{
    partial class Return
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxRentalID = new System.Windows.Forms.TextBox();
            this.buttonReturn = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelReturns = new System.Windows.Forms.Label();
            this.labelRentalTransaction = new System.Windows.Forms.Label();
            this.labelReturnEmployee = new System.Windows.Forms.Label();
            this.dataGridViewReturnableItems = new System.Windows.Forms.DataGridView();
            this.comboBoxReturnEmployee = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReturnableItems)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxRentalID
            // 
            this.textBoxRentalID.Location = new System.Drawing.Point(400, 130);
            this.textBoxRentalID.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxRentalID.Name = "textBoxRentalID";
            this.textBoxRentalID.Size = new System.Drawing.Size(640, 38);
            this.textBoxRentalID.TabIndex = 0;
            // 
            // buttonReturn
            // 
            this.buttonReturn.Location = new System.Drawing.Point(757, 794);
            this.buttonReturn.Margin = new System.Windows.Forms.Padding(6);
            this.buttonReturn.Name = "buttonReturn";
            this.buttonReturn.Size = new System.Drawing.Size(230, 45);
            this.buttonReturn.TabIndex = 5;
            this.buttonReturn.Text = "Return";
            this.buttonReturn.UseVisualStyleBackColor = true;
            this.buttonReturn.Click += new System.EventHandler(this.buttonReturn_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(1080, 130);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(6);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(174, 45);
            this.buttonSearch.TabIndex = 6;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(1023, 794);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(6);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(150, 45);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelReturns
            // 
            this.labelReturns.AutoSize = true;
            this.labelReturns.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReturns.Location = new System.Drawing.Point(624, 33);
            this.labelReturns.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelReturns.Name = "labelReturns";
            this.labelReturns.Size = new System.Drawing.Size(221, 63);
            this.labelReturns.TabIndex = 8;
            this.labelReturns.Text = "Returns";
            // 
            // labelRentalTransaction
            // 
            this.labelRentalTransaction.AutoSize = true;
            this.labelRentalTransaction.Location = new System.Drawing.Point(98, 130);
            this.labelRentalTransaction.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelRentalTransaction.Name = "labelRentalTransaction";
            this.labelRentalTransaction.Size = new System.Drawing.Size(296, 32);
            this.labelRentalTransaction.TabIndex = 9;
            this.labelRentalTransaction.Text = "Rental Transaction ID:";
            // 
            // labelReturnEmployee
            // 
            this.labelReturnEmployee.AutoSize = true;
            this.labelReturnEmployee.Location = new System.Drawing.Point(221, 676);
            this.labelReturnEmployee.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelReturnEmployee.Name = "labelReturnEmployee";
            this.labelReturnEmployee.Size = new System.Drawing.Size(234, 32);
            this.labelReturnEmployee.TabIndex = 13;
            this.labelReturnEmployee.Text = "Return Employee";
            // 
            // dataGridViewReturnableItems
            // 
            this.dataGridViewReturnableItems.AllowUserToAddRows = false;
            this.dataGridViewReturnableItems.AllowUserToDeleteRows = false;
            this.dataGridViewReturnableItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReturnableItems.Location = new System.Drawing.Point(104, 203);
            this.dataGridViewReturnableItems.Name = "dataGridViewReturnableItems";
            this.dataGridViewReturnableItems.RowHeadersWidth = 102;
            this.dataGridViewReturnableItems.RowTemplate.Height = 40;
            this.dataGridViewReturnableItems.Size = new System.Drawing.Size(1330, 422);
            this.dataGridViewReturnableItems.TabIndex = 14;
            // 
            // comboBoxReturnEmployee
            // 
            this.comboBoxReturnEmployee.FormattingEnabled = true;
            this.comboBoxReturnEmployee.Location = new System.Drawing.Point(590, 676);
            this.comboBoxReturnEmployee.Name = "comboBoxReturnEmployee";
            this.comboBoxReturnEmployee.Size = new System.Drawing.Size(352, 39);
            this.comboBoxReturnEmployee.TabIndex = 15;
            // 
            // Return
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBoxReturnEmployee);
            this.Controls.Add(this.dataGridViewReturnableItems);
            this.Controls.Add(this.labelReturnEmployee);
            this.Controls.Add(this.labelRentalTransaction);
            this.Controls.Add(this.labelReturns);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.buttonReturn);
            this.Controls.Add(this.textBoxRentalID);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Return";
            this.Size = new System.Drawing.Size(1472, 887);
            this.Load += new System.EventHandler(this.Return_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReturnableItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxRentalID;
        private System.Windows.Forms.Button buttonReturn;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelReturns;
        private System.Windows.Forms.Label labelRentalTransaction;
        private System.Windows.Forms.Label labelReturnEmployee;
        private System.Windows.Forms.DataGridView dataGridViewReturnableItems;
        private System.Windows.Forms.ComboBox comboBoxReturnEmployee;
    }
}
