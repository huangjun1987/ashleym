﻿namespace TechSupport
{
    partial class TEst
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.TechSupportDataSet = new TechSupport.TechSupportDataSet();
            this.OpenIncidentsAssignedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.OpenIncidentsAssignedTableAdapter = new TechSupport.TechSupportDataSetTableAdapters.OpenIncidentsAssignedTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.TechSupportDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpenIncidentsAssignedBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "OpenIncidentReport";
            reportDataSource1.Value = this.OpenIncidentsAssignedBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "TechSupport.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(146, 41);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(396, 246);
            this.reportViewer1.TabIndex = 0;
            // 
            // TechSupportDataSet
            // 
            this.TechSupportDataSet.DataSetName = "TechSupportDataSet";
            this.TechSupportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // OpenIncidentsAssignedBindingSource
            // 
            this.OpenIncidentsAssignedBindingSource.DataMember = "OpenIncidentsAssigned";
            this.OpenIncidentsAssignedBindingSource.DataSource = this.TechSupportDataSet;
            // 
            // OpenIncidentsAssignedTableAdapter
            // 
            this.OpenIncidentsAssignedTableAdapter.ClearBeforeFill = true;
            // 
            // TEst
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.reportViewer1);
            this.Name = "TEst";
            this.Text = "TEst";
            this.Load += new System.EventHandler(this.TEst_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TechSupportDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpenIncidentsAssignedBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource OpenIncidentsAssignedBindingSource;
        private TechSupportDataSet TechSupportDataSet;
        private TechSupportDataSetTableAdapters.OpenIncidentsAssignedTableAdapter OpenIncidentsAssignedTableAdapter;
    }
}