﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechSupport
{
    public partial class TEst : Form
    {
        public TEst()
        {
            InitializeComponent();
        }

        private void TEst_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'TechSupportDataSet.OpenIncidentsAssigned' table. You can move, or remove it, as needed.
            this.OpenIncidentsAssignedTableAdapter.Fill(this.TechSupportDataSet.OpenIncidentsAssigned);

            this.reportViewer1.RefreshReport();
        }
    }
}
