﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TechSupport.View
{
    /// <summary>
    /// This is the view class for the login form
    /// </summary>
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (userNameTextBox.Text == "" && passwordTextBox.Text == "")
            {
                this.Visible = false;
                //MainForm mainForm = new MainForm(userNameTextBox.Text, this);
                //mainForm.Show();
                MainDashboard mainDashboard = new MainDashboard(userNameTextBox.Text, this);
                mainDashboard.Show();
                userNameTextBox.Text = "";
                passwordTextBox.Text = "";

            }
            else
            {
                messageLabel.Text = "invalid username/password";
                messageLabel.ForeColor = Color.Red;
            }
        }

        private void userNameTextBox_TextChanged(object sender, EventArgs e)
        {
            messageLabel.Text = "";
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            messageLabel.Text = "";
        }
    }
}
