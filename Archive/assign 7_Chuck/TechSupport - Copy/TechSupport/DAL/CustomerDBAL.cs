﻿using System.Collections.Generic;
using System.Data.SqlClient;
using TechSupport.Model;


namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database customers.
    public class CustomerDBAL
    {
        /// <summary>
        /// This method gets a list from the database of customers.
        /// </summary>
        /// <returns>A list of customers</returns>
        public List<Customer> GetCustomers()
        {
            List<Customer> customerList = new List<Customer>();

            string selectStatement =
                "SELECT c.CustomerID, c.Name " +
                "FROM dbo.Customers c " +
                "ORDER BY c.Name ASC ";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Customer customer = new Customer();
                            customer.Name = reader["Name"].ToString();
                            customer.CustomerID = int.Parse(reader["CustomerID"].ToString());

                            customerList.Add(customer);
                        }
                    }
                }

            }
            return customerList;
        }
    }
}
