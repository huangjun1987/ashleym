﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;
using TechSupport.Model;

namespace TechSupport.DAL
{
    /// This the DAL class that deals with the database incidents.
    public class IncidentDBAL
    {
        /// <summary>
        /// This method gets a list from the database of incidents that are open.
        /// </summary>
        /// <returns>A list of open incidents</returns>
        public List<OpenIncident> GetOpenIncidents()
        {
            List<OpenIncident> incidentList = new List<OpenIncident>();

            string selectStatement =
                "SELECT i.ProductCode, Convert(varchar(10), i.DateOpened, 120) AS 'Date', c.Name AS 'Customer', t.Name AS 'Technician', i.Title " +
                "FROM dbo.Incidents i " +
                "INNER JOIN dbo.Customers c ON c.CustomerID = i.CustomerID " +
                "LEFT JOIN dbo.Technicians t ON t.TechID = i.TechID " +
                "WHERE i.DateClosed IS NULL";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            OpenIncident openIncident = new OpenIncident();
                            openIncident.ProductCode = reader["ProductCode"].ToString();
                            openIncident.DateOpened = reader["Date"].ToString();

                            openIncident.Customer = reader["Customer"].ToString(); ;
                            openIncident.Technician = reader["Technician"].ToString();
                            openIncident.Title = reader["Title"].ToString();

                            incidentList.Add(openIncident);

                        }
                    }
                }

            }
            return incidentList;
        }

        internal List<OpenIncidentReportRow> GetOpenIncidentReport()
        {
            List<OpenIncidentReportRow> incidentList = new List<OpenIncidentReportRow>();

            string selectStatement =
                @"SELECT p.Name as 'ProductName', i.Title, Convert(varchar(10), i.DateOpened, 120) AS 'DateOpened', c.Name AS 'CustomerName', t.Name AS 'TechnicianName'
                FROM dbo.Incidents i
				INNER JOIN dbo.Products p ON p.ProductCode = i.ProductCode
                INNER JOIN dbo.Customers c ON c.CustomerID = i.CustomerID
                INNER JOIN dbo.Technicians t ON t.TechID = i.TechID 
                WHERE i.DateClosed IS NULL
				";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var openIncident = new OpenIncidentReportRow();
                            openIncident.ProductName = reader["ProductName"].ToString();
                            openIncident.DateOpened = reader["DateOpened"].ToString();

                            openIncident.CustomerName = reader["CustomerName"].ToString(); ;
                            openIncident.TechnicianName = reader["TechnicianName"].ToString();
                            openIncident.IncidentTitle = reader["Title"].ToString();

                            incidentList.Add(openIncident);

                        }
                    }
                }

            }
            return incidentList;
        }

        /// <summary>
        /// This method gets a list of open incidents by technician.
        /// </summary>
        /// <param name="techID">ID number of the tech</param>
        /// <returns>a list of open incidents</returns>
        public List<OpenIncident> GetIncidentByTechnician(int techID)
        {
            List<OpenIncident> incidentList = new List<OpenIncident>();

            string selectStatement =
                "SELECT i.ProductCode, Convert(varchar(10), i.DateOpened, 120) AS 'Date', c.Name AS 'Customer', i.Title " +
                "FROM dbo.Incidents i " +
                "INNER JOIN dbo.Customers c ON c.CustomerID = i.CustomerID " +
                "LEFT JOIN dbo.Technicians t ON t.TechID = i.TechID " +
                "WHERE TechID = @techID";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {

                    selectCommand.Parameters.AddWithValue("@techID", techID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            OpenIncident openIncident = new OpenIncident();
                            openIncident.ProductCode = reader["ProductCode"].ToString();
                            openIncident.DateOpened = reader["Date"].ToString();

                            openIncident.Customer = reader["Customer"].ToString(); ;

                            openIncident.Title = reader["Title"].ToString();

                            incidentList.Add(openIncident);

                        }
                    }
                }

            }
            return incidentList;
        }

        /// <summary>
        /// This method checks if the customerID is registered for the product.
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="productCode"></param>
        /// <returns>boolean</returns>
        public bool IsRegistered(int customerID, string productCode)
        {
            string selectStatement =
                @"SELECT *
                FROM dbo.Registrations
                WHERE CustomerID = @customerID and ProductCode = @productCode";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@customerID", customerID);
                    selectCommand.Parameters.AddWithValue("@productCode", productCode);

                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// This method gets the incident to be updated.
        /// </summary>
        /// <param name="incidentID"></param>
        /// <returns>The model Update</returns>
        public Update GetUpdateIncident(int incidentID)
        {

            Update matchingUpdate = null;
            string selectUpdate =
                @"SELECT i.incidentID, c.Name AS 'Customer', i.ProductCode, t.Name AS 'Technician', i.Title, Convert(varchar(10), i.DateOpened, 120) AS 'DateOpened', Convert(varchar(10), i.DateClosed, 120) AS 'DateClosed', i.Description
                FROM dbo.Incidents i
                INNER JOIN dbo.Products p ON p.ProductCode = i.ProductCode
                INNER JOIN dbo.Customers c ON c.CustomerID = i.CustomerID
                LEFT JOIN dbo.Technicians t ON t.TechID = i.TechID
                WHERE i.IncidentID = @incidentID ";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())

            {

                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectUpdate, connection))
                {
                    selectCommand.Parameters.AddWithValue("@incidentID", incidentID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            matchingUpdate = new Update();
                            matchingUpdate.IncidentID = int.Parse(reader["IncidentID"].ToString());
                            matchingUpdate.Customer = reader["Customer"].ToString();
                            matchingUpdate.ProductCode = reader["ProductCode"].ToString();
                            matchingUpdate.Technician = reader["Technician"].ToString();
                            matchingUpdate.Title = reader["Title"].ToString();
                            matchingUpdate.DateOpened = reader["DateOpened"].ToString();
                            matchingUpdate.DateClosed = reader["DateClosed"].ToString();
                            matchingUpdate.Description = reader["Description"].ToString();

                        }
                    }
                }
            }

            return matchingUpdate;
        }

        /// <summary>
        /// This method gets a list of openincidentsbytechnician
        /// </summary>
        /// <param name="techID">ID of the technician</param>
        /// <returns>a list of openIncidentByTechnician</returns>
        public List<OpenIncidentByTechnician> GetOpenIncidentsByTechID(int techID)
        {
            var incidentList = new List<OpenIncidentByTechnician>();

            string selectStatement =
                @"SELECT p.Name, Convert(varchar(10), i.DateOpened, 120) AS 'Date', c.Name AS 'Customer', i.Title 
                FROM dbo.Incidents i 
                INNER JOIN dbo.Customers c ON c.CustomerID = i.CustomerID 
                INNER JOIN dbo.Products p ON p.ProductCode = i.ProductCode 
                WHERE i.DateClosed IS NULL and i.TechID = @techID ";

            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@techID", techID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var openIncident = new OpenIncidentByTechnician();
                            openIncident.Product = reader["Name"].ToString();
                            openIncident.DateOpened = reader["Date"].ToString();
                            openIncident.Customer = reader["Customer"].ToString(); ;
                            openIncident.Title = reader["Title"].ToString();
                            incidentList.Add(openIncident);
                        }
                    }
                }

            }
            return incidentList;
        }


        /// <summary>
        /// This method updates the incident in the DB.
        /// </summary>
        /// <param name="incidentID"></param>
        /// <param name="techID"></param>
        /// <param name="description"></param>
        /// <param name="closeDate"></param>
        public void UpdateIncident(int incidentID, int? techID, string description, DateTime? closeDate, Update update)
        {
            var openIncidentInDb = GetUpdateIncident(incidentID);
            if (update.Description != openIncidentInDb.Description || update.DateClosed != openIncidentInDb.DateClosed)
            {
                MessageBox.Show("This incident has been update by someone else. Please click Get again before update.");
                return;
            }

            string statement =
               @"Update [dbo].[Incidents] 
                SET [TechID] = @techID,
                [Description]=@description,
                [DateClosed]=@dateClosed
                Where incidentID = @incidentID";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(statement, connection))
                {
                    cmd.Parameters.AddWithValue("@incidentID", incidentID);
                    if (techID == null)
                    {
                        cmd.Parameters.AddWithValue("@techID", DBNull.Value);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@techID", techID);
                    }
                    cmd.Parameters.AddWithValue("@description", description);
                    if (closeDate == null)
                    {
                        cmd.Parameters.AddWithValue("@dateClosed", DBNull.Value);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@dateClosed", closeDate);
                    }

                    cmd.ExecuteNonQuery();

                    connection.Close();
                }

            }
        }

        /// <summary>
        /// This adds a new incident to the database.
        /// </summary>
        /// <param name="incident">incident to add</param>
        public void AddNewIncident(Incident incident)

        {

            string insertStatement =
           @"INSERT INTO dbo.Incidents
            (CustomerID
             , ProductCode
             , TechID
             , DateOpened
             , DateClosed
             , Title
             , Description) 
            VALUES
           (@customerID
           ,@productCode
           ,@techID
           ,@dateOpened
           ,@dateClosed
           ,@title
           ,@description)";


            using (SqlConnection connection = TechSupportDBConnection.GetConnection())

            {

                connection.Open();

                using (SqlCommand cmd = new SqlCommand(insertStatement, connection))

                {
                    cmd.Parameters.AddWithValue("@customerID", incident.CustomerID);
                    cmd.Parameters.AddWithValue("@productCode", incident.ProductCode);
                    cmd.Parameters.AddWithValue("@techID", DBNull.Value);
                    cmd.Parameters.AddWithValue("@dateOpened", DateTime.Now);
                    cmd.Parameters.AddWithValue("@dateClosed", DBNull.Value);
                    cmd.Parameters.AddWithValue("@title", incident.Title);
                    cmd.Parameters.AddWithValue("@description", incident.Description);
                    cmd.ExecuteNonQuery();
                    connection.Close();

                }
            }

        }

    }
}

