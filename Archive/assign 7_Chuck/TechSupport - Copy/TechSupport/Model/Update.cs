﻿using System;

namespace TechSupport.Model
{
    /// <summary>
    /// This is a model class for update.
    /// </summary>
    public class Update
    {
        public String ProductCode { get; set; }
        public String DateOpened { get; set; }
        public String DateClosed { get; set; }
        public String Customer { get; set; }
        public String Technician { get; set; }
        public String Title { get; set; }
        public int IncidentID { get; set; }
        public String Description { get; set; }

    }
}
