﻿namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for customer
    /// </summary>
    public class Customer
    {
        public string Name { get; set; }
        public int CustomerID { get; set; }
    }
}
