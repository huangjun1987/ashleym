﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupport.Model
{
   public class OpenIncidentReportRow
    {
        public string ProductName { get; set; }
        public string IncidentTitle { get; set; }
        public string  CustomerName { get; set; }
        public string DateOpened { get; set; }
        public string TechnicianName { get; set; }
    }
}
