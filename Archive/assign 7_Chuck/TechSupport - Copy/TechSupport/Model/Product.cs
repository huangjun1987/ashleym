﻿namespace TechSupport.Model
{
    /// <summary>
    /// This is the model class for product
    /// </summary>
    public class Product
    {
        public string Name { get; set; }
        public string ProductCode { get; set; }

    }
}
