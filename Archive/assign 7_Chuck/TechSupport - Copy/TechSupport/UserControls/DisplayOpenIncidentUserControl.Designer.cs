﻿namespace TechSupport.UserControls
{
    partial class DisplayOpenIncidentUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvOpenIncidents = new System.Windows.Forms.ListView();
            this.productCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dateOpened = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.customer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.technician = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.title = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lvOpenIncidents
            // 
            this.lvOpenIncidents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.productCode,
            this.dateOpened,
            this.customer,
            this.technician,
            this.title});
            this.lvOpenIncidents.HideSelection = false;
            this.lvOpenIncidents.Location = new System.Drawing.Point(14, 18);
            this.lvOpenIncidents.Name = "lvOpenIncidents";
            this.lvOpenIncidents.Size = new System.Drawing.Size(1153, 327);
            this.lvOpenIncidents.TabIndex = 0;
            this.lvOpenIncidents.UseCompatibleStateImageBehavior = false;
            this.lvOpenIncidents.View = System.Windows.Forms.View.Details;
            // 
            // productCode
            // 
            this.productCode.Text = "Product Code";
            this.productCode.Width = 116;
            // 
            // dateOpened
            // 
            this.dateOpened.Text = "Date Opened";
            this.dateOpened.Width = 116;
            // 
            // customer
            // 
            this.customer.Text = "customer";
            this.customer.Width = 139;
            // 
            // technician
            // 
            this.technician.Text = "Technician";
            this.technician.Width = 145;
            // 
            // title
            // 
            this.title.Text = "Title";
            this.title.Width = 632;
            // 
            // DisplayOpenIncidentUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvOpenIncidents);
            this.Name = "DisplayOpenIncidentUserControl";
            this.Size = new System.Drawing.Size(1178, 385);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvOpenIncidents;
        private System.Windows.Forms.ColumnHeader productCode;
        private System.Windows.Forms.ColumnHeader dateOpened;
        private System.Windows.Forms.ColumnHeader customer;
        private System.Windows.Forms.ColumnHeader technician;
        private System.Windows.Forms.ColumnHeader title;
    }
}
