﻿using System.Windows.Forms;
using System;
using TechSupport.Controller;

namespace TechSupport.UserControls
{
    public partial class ReportUserControl : UserControl
    {
        private readonly TechSupportController controller;
        public ReportUserControl()
        {
            InitializeComponent();
            controller = new TechSupportController();
        }

        private void ReportUserControl_Load(object sender, System.EventArgs e)
        {
            OpenIncidentReportRowBindingSource.DataSource = controller.GetOpenIncidentReport();
            reportViewer1.RefreshReport();
        }
    }
}
