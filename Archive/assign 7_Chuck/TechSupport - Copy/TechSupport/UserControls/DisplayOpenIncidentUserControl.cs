﻿using System;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupport.Model;


namespace TechSupport.UserControls
{
    /// <summary>
    /// This class is the user control for displaying an open incident.
    /// </summary>
    public partial class DisplayOpenIncidentUserControl : UserControl
    {
        private readonly TechSupportController controller;
        public DisplayOpenIncidentUserControl()
        {
            InitializeComponent();
            this.controller = new TechSupportController();

        }

        public void Display()
        {

            try
            {

                var incidentList = this.controller.GetOpenIncidents();
                lvOpenIncidents.Clear();
                lvOpenIncidents.View = System.Windows.Forms.View.Details;
                lvOpenIncidents.Columns.Add("ProductCode", 100);
                lvOpenIncidents.Columns.Add("Title", 250);
                lvOpenIncidents.Columns.Add("Customer", 100);
                lvOpenIncidents.Columns.Add("DateOpened", 100);
                lvOpenIncidents.Columns.Add("Technician", 100);


                if (incidentList.Count > 0)
                {
                    OpenIncident incident;
                    for (int i = 0; i < incidentList.Count; i++)
                    {

                        incident = incidentList[i];
                        lvOpenIncidents.Items.Add(incident.ProductCode);
                        lvOpenIncidents.Items[i].SubItems.Add(incident.Title.ToString());

                        lvOpenIncidents.Items[i].SubItems.Add(incident.Customer.ToString());
                        lvOpenIncidents.Items[i].SubItems.Add(incident.DateOpened.ToString());
                        lvOpenIncidents.Items[i].SubItems.Add(incident.Technician.ToString());

                    }

                }
                else
                {
                    MessageBox.Show("Incidents cannot be shown.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

        }
    }
}





