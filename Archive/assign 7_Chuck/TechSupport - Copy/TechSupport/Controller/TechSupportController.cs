﻿using TechSupport.DAL;
using System;
using System.Collections.Generic;
using TechSupport.Model;
using System.Windows.Forms;


namespace TechSupport.Controller
{
    /// The controller class deals with the DAL and delegates the work to DAL
    public class TechSupportController
    {
        private IncidentDAL incidentSource;
        private IncidentDBAL incidentDBAL;
        private ProductDBAL productDBAL;
        private TechnicianDBAL technicianDBAL;
        private CustomerDBAL customerDBAL;

        internal List<OpenIncidentReportRow> GetOpenIncidentReport()
        {
            return incidentDBAL.GetOpenIncidentReport();
        }

        private RegistrationDBAL registrationDBAL;

        public TechSupportController()
        {
            this.incidentSource = new IncidentDAL();
            this.incidentDBAL = new IncidentDBAL();
            this.productDBAL = new ProductDBAL();
            this.technicianDBAL = new TechnicianDBAL();
            this.customerDBAL = new CustomerDBAL();
            this.registrationDBAL = new RegistrationDBAL();
        }

        /// <summary>
        /// This method delegates getting a list of incidents to the DBAL.
        /// </summary>
        /// <returns>A list of all incidents</returns>
        public List<Incident> GetIncidents()
        {
            return this.incidentSource.GetIncidents();
        }

        /// <summary>
        /// This method gets an incident by techID
        /// </summary>
        /// <param name="techID"></param>
        /// <returns></returns>
        public List<OpenIncident> GetIncidentByTechnician(int techID)
        {
            return incidentDBAL.GetIncidentByTechnician(techID);
        }

        /// <summary>
        /// This method delegates updates to the incident database to the DBAL.
        /// </summary>
        /// <param name="incidentID"></param>
        /// <param name="techID"></param>
        /// <param name="description"></param>
        /// <param name="closeDate"></param>
        public void UpdateIncident(int incidentID, int? techID, string description, DateTime? closeDate, Update update)
        {
            incidentDBAL.UpdateIncident(incidentID, techID, description, closeDate, update);
        }

        /// <summary>
        /// This method delegates registration verification to the DBAL
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public bool IsRegistered(int customerID, string productCode)
        {
            return incidentDBAL.IsRegistered(customerID, productCode);
        }

        public List<OpenIncidentByTechnician> GetOpenIncidentsByTechID(int techID)
        {
            return this.incidentDBAL.GetOpenIncidentsByTechID(techID);
        }

        /// <summary>
        /// This method delegates getting a list of all the open incidents to the database DAL.
        /// </summary>
        /// <returns>A list of open incidents</returns>
        public List<OpenIncident> GetOpenIncidents()
        {
            return this.incidentDBAL.GetOpenIncidents();
        }

        /// <summary>
        /// This method gets the update incident.
        /// </summary>
        /// <param name="incidentID">ID of the incident</param>
        /// <returns>returns an Update</returns>
        public Update GetUpdateIncident(int incidentID)
        {
            return this.incidentDBAL.GetUpdateIncident(incidentID);
        }

        /// <summary>
        /// This method gets a list of technicians who have incidents.
        /// </summary>
        /// <returns>returns a list of technicians</returns>
        public List<Technician> GetTechniciansWithIncidents()
        {
            return technicianDBAL.GetTechniciansWithIncidents();

        }

        /// <summary>
        /// This method gets a list of all technicians.
        /// </summary>
        /// <returns>A list of all technicians</returns>
        public List<Technician> GetTechnicians()
        {
            return this.technicianDBAL.GetTechnicians();
        }

        /// <summary>
        /// This method delegates getting a list of all the customers to the database DAL.
        /// </summary>
        ///  /// <returns>A list of customers</returns>

        public List<Customer> GetCustomers()
        {
            return this.customerDBAL.GetCustomers();
        }

        /// <summary>
        /// This method delegates getting a list of all the products to the database DAL.
        /// </summary>
        ///  /// <returns>A list of products</returns>
        public List<Product> GetProducts()
        {
            return this.productDBAL.GetProducts();
        }

        /// <summary>
        /// This method delegates getting a list of all the products to the database DAL.
        /// </summary>
        ///  /// <returns>A list of products</returns>
        public List<Registration> GetRegistration()
        {
            return this.registrationDBAL.GetRegistration();
        }

        /// <summary>
        /// This method delegates adding an incident to the DAL.
        /// </summary>
        /// <param name="incident"></param>
        public void Add(Incident incident)
        {

            if (incident == null)
            {
                throw new ArgumentNullException("Incident cannot be null");
            }

            this.incidentDBAL.AddNewIncident(incident);


        }

        /// <summary>
        /// This method delegates getting a list of customers by customerID to the DAL.
        /// </summary>
        /// <param name="customerID">ID of the customer</param>
        /// <returns>A list of customers</returns>
        public List<Incident> GetIncidentsByCustomerID(int customerID)
        {
            if (this.incidentSource.GetIncidentsByCustomerID(customerID).Count == 0)
            {
                MessageBox.Show("Customer does not exist",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return this.incidentSource.GetIncidentsByCustomerID(customerID);

        }
    }
}



