﻿using Solvent.Model;
using System;
using System.Windows.Forms;

namespace Solvent.Util
{
    /// <summary>
    /// Utility class to validate fields.
    /// </summary>
    class Validator
    {
        /// <summary>
        /// This method provides user validation.
        /// </summary>
        /// <param name="user">The user to be validated.</param>
        /// <returns>False if and only if all parameters are valid.</returns>
        public static Boolean ValidateUser(SolventUser user)
        {
            if (user.AnnualIncome < 0 || user.DesiredRetirementIncome < 0 || user.SpouseAnnualIncome < 0)
            {
                MessageBox.Show("Currency fields cannot be negative.",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
            if (user.DesiredRetirementAge < 0)
            {
                MessageBox.Show("Desired retirement age cannot be negative.",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
            if (user.DesiredRetirementAge < GetAge(user.DOB))
            {
                MessageBox.Show("Retirement age cannot be less than current age.",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
            if (user.DOB > DateTime.Today || user.SpouseDOB > DateTime.Today)
            {
                MessageBox.Show("Dates of birth cannot be in the future.",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
            return false;
        }

        /// <summary>
        /// This helper method calculates the age for the given DOB.
        /// </summary>
        /// <param name="dob">The given DOB.</param>
        /// <returns>The calculated age.</returns>
        private static int GetAge(DateTime dob)
        {
            int age = DateTime.Today.Year - dob.Year;
            if ((DateTime.Today.Month == dob.Month && DateTime.Today.Day < dob.Day) || DateTime.Today.Month < dob.Month)
            {
                age--;
            }
            return age;
        }
    }
}
