﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Solvent.Controller;
using Solvent.Util;
using Solvent.View;

namespace Solvent.UserControls
{
    /// <summary>
    /// This class models a user control for adding a transaction.
    /// </summary>
    public partial class AddTransactionUserControl : UserControl
    {
        private readonly AccountController accountController;
        private readonly TransactionController transactionController;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public AddTransactionUserControl()
        {
            InitializeComponent();
            this.accountController = new AccountController();
            this.transactionController = new TransactionController();
            this.PopulateComboBox();
        }

        /// <summary>
        /// This method refreshes the accounts combobox.
        /// </summary>
        public void RefreshAccountsComboBox()
        {
            this.AccountComboBox.Items.Clear();

            try
            {
                List<Solvent.Model.AccountDTO> accounts = this.accountController.GetAccountsByUserID(MainDashboard.UserID);

                foreach (var item in accounts)
                {
                    this.AccountComboBox.Items.Add(new { Text = item.AccountTypeName, Value = item.AccountID });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while fetching user accounts!!!! - " + ex.Message,
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// This method populates the category combobox.
        /// </summary>
        private void PopulateComboBox()
        {
            ComboBoxUtil.UpdateTransactionTypeComboBox(this.TransactionTypeComboBox);

            try
            {
                List<Solvent.Model.CategoryType> categoryTypes = this.transactionController.GetCategoryTypes();

                foreach (var item in categoryTypes)
                {
                    this.CategoryComboBox.Items.Add(new { Text = item.CategoryName, Value = item.CategoryId });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while fetching category Types!!!! - " + ex.Message,
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.RefreshAccountsComboBox();

        }

        /// <summary>
        /// This method handles when the clear button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButton_Click(object sender, EventArgs e)
        {
            this.ClearAll();
        }

        /// <summary>
        /// This method clears the form.
        /// </summary>
        private void ClearAll()
        {
            this.TransactionTypeComboBox.SelectedIndex = -1;
            this.TransactionDateTimePicker.Value = DateTime.Now;
            this.CategoryComboBox.SelectedIndex = -1;
            this.AccountComboBox.SelectedIndex = -1;
            this.AmountTextBox.Text = "";
        }

        /// <summary>
        /// This method handles when the add transaction button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddTransactionButton_Click(object sender, EventArgs e)
        {
            // Validation
            if (this.TransactionTypeComboBox.SelectedItem == null)
            {
                MessageBox.Show("Transaction Type is required!!!! - ",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.CategoryComboBox.SelectedItem == null)
            {
                MessageBox.Show("Category is required!!!! - ",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.AccountComboBox.SelectedItem == null)
            {
                MessageBox.Show("Account is required!!!! - ",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int amount;

            try
            {
                amount = Int32.Parse(this.AmountTextBox.Text);
                if (amount < 0)
                {
                    throw new Exception("Invalid Number");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("Amount should be a positive number!!!!",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Solvent.Model.Transaction transaction = new Solvent.Model.Transaction
            {
                AccountId = (this.AccountComboBox.SelectedItem as dynamic).Value,
                IsIncome = this.TransactionTypeComboBox.SelectedItem.ToString() == "Income" ? 1 : 0,
                CategoryId = (this.CategoryComboBox.SelectedItem as dynamic).Value,
                Date = this.TransactionDateTimePicker.Value,
                Amount = amount
            };

            try
            {

                int accountId = this.transactionController.AddTransaction(transaction);

                MessageBox.Show("Transaction has been successfully created.",
                   "Information!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.ClearAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while creating Transaction. Please contact support!!!! - " + ex.Message,
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
