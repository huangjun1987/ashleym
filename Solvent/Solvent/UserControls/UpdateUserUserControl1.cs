﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solvent.View;
using Solvent.Controller;
using Solvent.Util;

namespace Solvent.UserControls
{
    public partial class UpdateUserUserControl1 : UserControl
    {
        private readonly RegisterController registerController;
        public UpdateUserUserControl1()
        {
            InitializeComponent();
            registerController = new RegisterController();
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            var username = textBoxUsername.Text.Trim();
            var password = textBoxPassword.Text.Trim();
            var name = textBoxName.Text.Trim();
            var user = registerController.UpdateUser(MainDashboard.UserID, username, password, name);
        }

        private void UpdateUserUserControl1_Load(object sender, EventArgs e)
        {
            var user = registerController.GetUserByID(MainDashboard.UserID);
            if (user != null)
            {
                textBoxName.Text = user.Name;
                textBoxUsername.Text = user.Username;
                textBoxPassword.Text = Security.Decrypt(user.Password);
            }
          
        }
    }
}
