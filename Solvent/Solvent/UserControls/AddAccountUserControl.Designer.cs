﻿namespace Solvent.UserControls
{
    partial class AddAccountUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AccountTypeComboBox = new System.Windows.Forms.ComboBox();
            this.accountTypeLabel = new System.Windows.Forms.Label();
            this.isTraditionalComboBox = new System.Windows.Forms.ComboBox();
            this.isTraditionalLabel = new System.Windows.Forms.Label();
            this.traditionalWarningMessage = new System.Windows.Forms.Label();
            this.OwnerComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AssetLiabilityComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CurrentBalanceTextBox = new System.Windows.Forms.TextBox();
            this.CurrentBalanceLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.addAccountButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AccountTypeComboBox
            // 
            this.AccountTypeComboBox.DisplayMember = "Text";
            this.AccountTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AccountTypeComboBox.FormattingEnabled = true;
            this.AccountTypeComboBox.Location = new System.Drawing.Point(178, 41);
            this.AccountTypeComboBox.Name = "AccountTypeComboBox";
            this.AccountTypeComboBox.Size = new System.Drawing.Size(187, 21);
            this.AccountTypeComboBox.TabIndex = 9;
            this.AccountTypeComboBox.ValueMember = "Value";
            // 
            // accountTypeLabel
            // 
            this.accountTypeLabel.AutoSize = true;
            this.accountTypeLabel.Location = new System.Drawing.Point(70, 44);
            this.accountTypeLabel.Name = "accountTypeLabel";
            this.accountTypeLabel.Size = new System.Drawing.Size(77, 13);
            this.accountTypeLabel.TabIndex = 10;
            this.accountTypeLabel.Text = "Account Type:";
            // 
            // isTraditionalComboBox
            // 
            this.isTraditionalComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.isTraditionalComboBox.FormattingEnabled = true;
            this.isTraditionalComboBox.Location = new System.Drawing.Point(178, 89);
            this.isTraditionalComboBox.Name = "isTraditionalComboBox";
            this.isTraditionalComboBox.Size = new System.Drawing.Size(187, 21);
            this.isTraditionalComboBox.TabIndex = 11;
            // 
            // isTraditionalLabel
            // 
            this.isTraditionalLabel.AutoSize = true;
            this.isTraditionalLabel.Location = new System.Drawing.Point(70, 92);
            this.isTraditionalLabel.Name = "isTraditionalLabel";
            this.isTraditionalLabel.Size = new System.Drawing.Size(76, 13);
            this.isTraditionalLabel.TabIndex = 12;
            this.isTraditionalLabel.Text = "Is Traditional?:";
            // 
            // traditionalWarningMessage
            // 
            this.traditionalWarningMessage.AutoSize = true;
            this.traditionalWarningMessage.Location = new System.Drawing.Point(373, 92);
            this.traditionalWarningMessage.Name = "traditionalWarningMessage";
            this.traditionalWarningMessage.Size = new System.Drawing.Size(256, 13);
            this.traditionalWarningMessage.TabIndex = 13;
            this.traditionalWarningMessage.Text = "(Select \'Not Applicable\' if not an investment account)";
            // 
            // OwnerComboBox
            // 
            this.OwnerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OwnerComboBox.FormattingEnabled = true;
            this.OwnerComboBox.Location = new System.Drawing.Point(178, 137);
            this.OwnerComboBox.Name = "OwnerComboBox";
            this.OwnerComboBox.Size = new System.Drawing.Size(187, 21);
            this.OwnerComboBox.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Owner:";
            // 
            // AssetLiabilityComboBox
            // 
            this.AssetLiabilityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AssetLiabilityComboBox.FormattingEnabled = true;
            this.AssetLiabilityComboBox.Location = new System.Drawing.Point(178, 185);
            this.AssetLiabilityComboBox.Name = "AssetLiabilityComboBox";
            this.AssetLiabilityComboBox.Size = new System.Drawing.Size(187, 21);
            this.AssetLiabilityComboBox.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Asset or Liability?:";
            // 
            // CurrentBalanceTextBox
            // 
            this.CurrentBalanceTextBox.Location = new System.Drawing.Point(178, 232);
            this.CurrentBalanceTextBox.MaxLength = 50;
            this.CurrentBalanceTextBox.Name = "CurrentBalanceTextBox";
            this.CurrentBalanceTextBox.Size = new System.Drawing.Size(187, 20);
            this.CurrentBalanceTextBox.TabIndex = 19;
            // 
            // CurrentBalanceLabel
            // 
            this.CurrentBalanceLabel.AutoSize = true;
            this.CurrentBalanceLabel.Location = new System.Drawing.Point(70, 235);
            this.CurrentBalanceLabel.Name = "CurrentBalanceLabel";
            this.CurrentBalanceLabel.Size = new System.Drawing.Size(86, 13);
            this.CurrentBalanceLabel.TabIndex = 18;
            this.CurrentBalanceLabel.Text = "Current Balance:";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(295, 299);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(70, 23);
            this.clearButton.TabIndex = 21;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // addAccountButton
            // 
            this.addAccountButton.Location = new System.Drawing.Point(146, 299);
            this.addAccountButton.Name = "addAccountButton";
            this.addAccountButton.Size = new System.Drawing.Size(111, 23);
            this.addAccountButton.TabIndex = 20;
            this.addAccountButton.Text = "Add Account";
            this.addAccountButton.UseVisualStyleBackColor = true;
            this.addAccountButton.Click += new System.EventHandler(this.AddAccountButton_Click);
            // 
            // AddAccountUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.addAccountButton);
            this.Controls.Add(this.CurrentBalanceTextBox);
            this.Controls.Add(this.CurrentBalanceLabel);
            this.Controls.Add(this.AssetLiabilityComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.OwnerComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.traditionalWarningMessage);
            this.Controls.Add(this.isTraditionalComboBox);
            this.Controls.Add(this.isTraditionalLabel);
            this.Controls.Add(this.AccountTypeComboBox);
            this.Controls.Add(this.accountTypeLabel);
            this.Name = "AddAccountUserControl";
            this.Size = new System.Drawing.Size(646, 402);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox AccountTypeComboBox;
        private System.Windows.Forms.Label accountTypeLabel;
        private System.Windows.Forms.ComboBox isTraditionalComboBox;
        private System.Windows.Forms.Label isTraditionalLabel;
        private System.Windows.Forms.Label traditionalWarningMessage;
        private System.Windows.Forms.ComboBox OwnerComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox AssetLiabilityComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CurrentBalanceTextBox;
        private System.Windows.Forms.Label CurrentBalanceLabel;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button addAccountButton;
    }
}
