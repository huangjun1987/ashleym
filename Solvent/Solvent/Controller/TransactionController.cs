﻿using Solvent.DAL;
using System.Collections.Generic;

namespace Solvent.Controller
{
    /// <summary>
    /// Controller class to mediate between TransactionDAL and View.
    /// </summary>
    public class TransactionController
    {
        private readonly TransactionDAL transactionDataSource;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public TransactionController()
        {
            this.transactionDataSource = new TransactionDAL();
        }

        /// <summary>
        /// This method returns all available category types.
        /// </summary>
        /// <returns>The list of category types.</returns>
        public List<Solvent.Model.CategoryType> GetCategoryTypes()
        {
            return this.transactionDataSource.GetCategoryTypes();
        }

        /// <summary>
        /// Adds a new transaction
        /// </summary>
        /// <param name="Transaction">Transaction Object</param>
        /// <returns>Transaction Id of the created transaction</returns>
        public int AddTransaction(Solvent.Model.Transaction transaction)
        {
            return this.transactionDataSource.AddTransaction(transaction);
        }

        /// <summary>
        /// This method returns the list of transactions for the given user.
        /// </summary>
        /// <param name="userID">The user whose transactions will be returned.</param>
        /// <returns>The list of transactions for the given user.</returns>
        public List<Solvent.Model.Transaction> GetTransactionsByUserID(int userID)
        {
            return this.transactionDataSource.GetTransactionsByUserID(userID);
        }

    }
}
