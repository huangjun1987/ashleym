﻿using Solvent.DAL;
using Solvent.Model;

namespace Solvent.Controller
{
    /// <summary>
    /// This class is the controller for login.
    /// </summary>
   public class LoginController
    {
        private readonly UserDAL loginDAL;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public LoginController()
        {
            this.loginDAL = new UserDAL();
        }

        /// <summary>
        /// This class references the employee login.
        /// </summary>
        /// <param name="username">String of entered username</param>
        /// <param name="password">String of entered password</param>
        /// <returns></returns>
        public LoginResult Login(string username, string password)
        {
            return loginDAL.Login(username, password);
        }
    }
}
