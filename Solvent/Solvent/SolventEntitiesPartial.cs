﻿using System.Data.Entity;

namespace Solvent
{
    /// <summary>
    /// This class is the partial class with the connection string
    /// </summary>
    public partial class SolventEntities : DbContext
    {
        public SolventEntities(bool overrideflag)
         : base(@"metadata=res://*/SolventModel.csdl|res://*/SolventModel.ssdl|res://*/SolventModel.msl;"+
              @"provider=System.Data.SqlClient;provider connection string=';"+
               @"data source=tcp:solvent.database.windows.net;"+
              @"initial catalog=Solvent;"+
               @"persist security info=True;"+
               @"user id=ashley;password=Password123!;"+
               @"MultipleActiveResultSets=True;"+
              @"application name=EntityFramework';")      

        {

        }
    }
}
