﻿namespace Solvent.Model
{
    /// <summary>
    /// This class models an account.
    /// </summary>
   public class AccountDTO
    {
        /// <summary>
        /// Getters and setters for account instance variables.
        /// </summary>
        public int AccountID { get; set; }
        public int AccountTypeID { get; set; }
        public string AccountTypeName { get; set; }
        public int UserID { get; set; }
        public string TraditionalOrRoth { get; set; }
        public bool IsAsset { get; set; }
        public string Owner { get; set; }
        public decimal Balance { get; set; }
    }
}
