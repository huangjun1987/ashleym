﻿using System;

namespace Solvent.Model
{
    /// <summary>
    /// This class models a Transaction.
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Getters and setters for transaction instance variables.
        /// </summary>
        public int IsIncome { get; set; }

        public string IncomeOrExpense { get; set; }

        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int AccountId { get; set; }

        public string AccountName { get; set; }
    }
}
