﻿namespace Solvent.Model
{
    /// <summary>
    /// This class models an account type.
    /// </summary>
    public class AccountTypeDTO
    {
        /// <summary>
        /// Getters and setters for the account type instance variables.
        /// </summary>
        public int AccountTypeID { get; set; }
        public string AccountTypeName { get; set; }
    }
}
