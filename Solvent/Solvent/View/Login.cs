﻿using Solvent.Controller;
using Solvent.View;
using System;
using System.Windows.Forms;

namespace Solvent
{
    /// <summary>
    /// This class if for the login form view
    /// </summary>
    public partial class Login : Form
    {
        private readonly LoginController controller;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public Login()
        {
            InitializeComponent();
            this.controller = new LoginController();
        }

        /// <summary>
        /// This method checks the user's entered data when the login button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(usernameTextBox.Text))
                {
                    MessageBox.Show("Please enter username.");
                    return;
                }
                if (String.IsNullOrWhiteSpace(passwordTextBox.Text))
                {
                    MessageBox.Show("Please enter password.");
                    return;
                }
                var loginResult = controller.Login(usernameTextBox.Text, passwordTextBox.Text);
                if (loginResult == null)
                {
                    MessageBox.Show("Incorrect username or password.");
                    usernameTextBox.Text = "";
                    passwordTextBox.Text = "";
                }
                else
                {
                    this.Visible = false;
                    MainDashboard mainDashboard = new MainDashboard(usernameTextBox.Text, loginResult.UserID, this);
                    mainDashboard.Show();
                    usernameTextBox.Text = "";
                    passwordTextBox.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("User Does Not Exist. " + ex);
            }
        }

        /// <summary>
        /// This method handles when the register button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonRegister_Click(object sender, EventArgs e)
        {
            var registerForm = new Register(this);
            this.Hide();
            registerForm.Show();
        }
    }
}
