﻿namespace Solvent.View
{
    partial class MainDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDashboard));
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.TransactionsTab = new System.Windows.Forms.TabPage();
            this.ViewTransactionsUserControl = new Solvent.UserControls.ViewTransactionsUserControl();
            this.retirementTabPage = new System.Windows.Forms.TabPage();
            this.retirementUserControl = new Solvent.UserControls.RetirementUserControl();
            this.AddTransactionTab = new System.Windows.Forms.TabPage();
            this.addTransactionUserControl1 = new Solvent.UserControls.AddTransactionUserControl();
            this.AddAccountTab = new System.Windows.Forms.TabPage();
            this.addAccountUserControl1 = new Solvent.UserControls.AddAccountUserControl();
            this.UpdateUser = new System.Windows.Forms.TabPage();
            this.updateUserUserControl11 = new Solvent.UserControls.UpdateUserUserControl1();
            this.logoutButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.currentUserLabel = new System.Windows.Forms.Label();
            this.currentUserIDLabel = new System.Windows.Forms.Label();
            this.mainTabControl.SuspendLayout();
            this.TransactionsTab.SuspendLayout();
            this.retirementTabPage.SuspendLayout();
            this.AddTransactionTab.SuspendLayout();
            this.AddAccountTab.SuspendLayout();
            this.UpdateUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.TransactionsTab);
            this.mainTabControl.Controls.Add(this.retirementTabPage);
            this.mainTabControl.Controls.Add(this.AddTransactionTab);
            this.mainTabControl.Controls.Add(this.AddAccountTab);
            this.mainTabControl.Controls.Add(this.UpdateUser);
            this.mainTabControl.Location = new System.Drawing.Point(32, 97);
            this.mainTabControl.Margin = new System.Windows.Forms.Padding(8);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(1704, 1089);
            this.mainTabControl.TabIndex = 0;
            // 
            // TransactionsTab
            // 
            this.TransactionsTab.Controls.Add(this.ViewTransactionsUserControl);
            this.TransactionsTab.Location = new System.Drawing.Point(10, 48);
            this.TransactionsTab.Margin = new System.Windows.Forms.Padding(8);
            this.TransactionsTab.Name = "TransactionsTab";
            this.TransactionsTab.Padding = new System.Windows.Forms.Padding(8);
            this.TransactionsTab.Size = new System.Drawing.Size(1684, 1031);
            this.TransactionsTab.TabIndex = 0;
            this.TransactionsTab.Text = "Transactions";
            this.TransactionsTab.UseVisualStyleBackColor = true;
            this.TransactionsTab.Enter += new System.EventHandler(this.ViewTransaction_Enter);
            // 
            // ViewTransactionsUserControl
            // 
            this.ViewTransactionsUserControl.Location = new System.Drawing.Point(0, 0);
            this.ViewTransactionsUserControl.Margin = new System.Windows.Forms.Padding(10);
            this.ViewTransactionsUserControl.Name = "ViewTransactionsUserControl";
            this.ViewTransactionsUserControl.Size = new System.Drawing.Size(1674, 1013);
            this.ViewTransactionsUserControl.TabIndex = 1;
            this.ViewTransactionsUserControl.Enter += new System.EventHandler(this.ViewTransaction_Enter);
            // 
            // retirementTabPage
            // 
            this.retirementTabPage.Controls.Add(this.retirementUserControl);
            this.retirementTabPage.Location = new System.Drawing.Point(10, 48);
            this.retirementTabPage.Margin = new System.Windows.Forms.Padding(8);
            this.retirementTabPage.Name = "retirementTabPage";
            this.retirementTabPage.Padding = new System.Windows.Forms.Padding(8);
            this.retirementTabPage.Size = new System.Drawing.Size(1684, 1031);
            this.retirementTabPage.TabIndex = 2;
            this.retirementTabPage.Text = "Retirement";
            this.retirementTabPage.UseVisualStyleBackColor = true;
            this.retirementTabPage.Enter += new System.EventHandler(this.RetirementTabPage_Enter);
            // 
            // retirementUserControl
            // 
            this.retirementUserControl.Location = new System.Drawing.Point(0, 0);
            this.retirementUserControl.Margin = new System.Windows.Forms.Padding(10);
            this.retirementUserControl.Name = "retirementUserControl";
            this.retirementUserControl.Size = new System.Drawing.Size(1682, 1027);
            this.retirementUserControl.TabIndex = 0;
            // 
            // AddTransactionTab
            // 
            this.AddTransactionTab.Controls.Add(this.addTransactionUserControl1);
            this.AddTransactionTab.Location = new System.Drawing.Point(10, 48);
            this.AddTransactionTab.Margin = new System.Windows.Forms.Padding(8);
            this.AddTransactionTab.Name = "AddTransactionTab";
            this.AddTransactionTab.Size = new System.Drawing.Size(1684, 1031);
            this.AddTransactionTab.TabIndex = 3;
            this.AddTransactionTab.Text = "Add Transaction";
            this.AddTransactionTab.UseVisualStyleBackColor = true;
            // 
            // addTransactionUserControl1
            // 
            this.addTransactionUserControl1.Location = new System.Drawing.Point(74, 39);
            this.addTransactionUserControl1.Margin = new System.Windows.Forms.Padding(10);
            this.addTransactionUserControl1.Name = "addTransactionUserControl1";
            this.addTransactionUserControl1.Size = new System.Drawing.Size(1518, 953);
            this.addTransactionUserControl1.TabIndex = 0;
            this.addTransactionUserControl1.Enter += new System.EventHandler(this.AddTransaction_Enter);
            // 
            // AddAccountTab
            // 
            this.AddAccountTab.Controls.Add(this.addAccountUserControl1);
            this.AddAccountTab.Location = new System.Drawing.Point(10, 48);
            this.AddAccountTab.Margin = new System.Windows.Forms.Padding(8);
            this.AddAccountTab.Name = "AddAccountTab";
            this.AddAccountTab.Padding = new System.Windows.Forms.Padding(8);
            this.AddAccountTab.Size = new System.Drawing.Size(1684, 1031);
            this.AddAccountTab.TabIndex = 1;
            this.AddAccountTab.Text = "Add Account";
            this.AddAccountTab.UseVisualStyleBackColor = true;
            // 
            // addAccountUserControl1
            // 
            this.addAccountUserControl1.Location = new System.Drawing.Point(0, 14);
            this.addAccountUserControl1.Margin = new System.Windows.Forms.Padding(10);
            this.addAccountUserControl1.Name = "addAccountUserControl1";
            this.addAccountUserControl1.Size = new System.Drawing.Size(1682, 959);
            this.addAccountUserControl1.TabIndex = 0;
            // 
            // UpdateUser
            // 
            this.UpdateUser.Controls.Add(this.updateUserUserControl11);
            this.UpdateUser.Location = new System.Drawing.Point(10, 48);
            this.UpdateUser.Margin = new System.Windows.Forms.Padding(6);
            this.UpdateUser.Name = "UpdateUser";
            this.UpdateUser.Size = new System.Drawing.Size(1684, 1031);
            this.UpdateUser.TabIndex = 4;
            this.UpdateUser.Text = "Update User";
            this.UpdateUser.UseVisualStyleBackColor = true;
            // 
            // updateUserUserControl11
            // 
            this.updateUserUserControl11.Location = new System.Drawing.Point(6, 49);
            this.updateUserUserControl11.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.updateUserUserControl11.Name = "updateUserUserControl11";
            this.updateUserUserControl11.Size = new System.Drawing.Size(1640, 665);
            this.updateUserUserControl11.TabIndex = 0;
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(1526, 29);
            this.logoutButton.Margin = new System.Windows.Forms.Padding(8);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(200, 54);
            this.logoutButton.TabIndex = 1;
            this.logoutButton.Text = "Logout";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(32, 29);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(210, 54);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // currentUserLabel
            // 
            this.currentUserLabel.Location = new System.Drawing.Point(258, 29);
            this.currentUserLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.currentUserLabel.Name = "currentUserLabel";
            this.currentUserLabel.Size = new System.Drawing.Size(1250, 54);
            this.currentUserLabel.TabIndex = 3;
            this.currentUserLabel.Text = "Current User";
            this.currentUserLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // currentUserIDLabel
            // 
            this.currentUserIDLabel.AutoSize = true;
            this.currentUserIDLabel.Location = new System.Drawing.Point(1512, 91);
            this.currentUserIDLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.currentUserIDLabel.Name = "currentUserIDLabel";
            this.currentUserIDLabel.Size = new System.Drawing.Size(209, 32);
            this.currentUserIDLabel.TabIndex = 4;
            this.currentUserIDLabel.Text = "Current User ID";
            this.currentUserIDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.currentUserIDLabel.Visible = false;
            // 
            // MainDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1768, 1217);
            this.Controls.Add(this.currentUserIDLabel);
            this.Controls.Add(this.currentUserLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.mainTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "$olvent Dashboard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDashboard_FormClosing);
            this.mainTabControl.ResumeLayout(false);
            this.TransactionsTab.ResumeLayout(false);
            this.retirementTabPage.ResumeLayout(false);
            this.AddTransactionTab.ResumeLayout(false);
            this.AddAccountTab.ResumeLayout(false);
            this.UpdateUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage TransactionsTab;
        private System.Windows.Forms.TabPage AddAccountTab;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage retirementTabPage;
        private UserControls.RetirementUserControl retirementUserControl;
        private System.Windows.Forms.Label currentUserLabel;
        private System.Windows.Forms.Label currentUserIDLabel;
        private UserControls.AddAccountUserControl addAccountUserControl1;
        private System.Windows.Forms.TabPage AddTransactionTab;
        private UserControls.AddTransactionUserControl addTransactionUserControl1;
        private UserControls.ViewTransactionsUserControl ViewTransactionsUserControl;
        private System.Windows.Forms.TabPage UpdateUser;
        private UserControls.UpdateUserUserControl1 updateUserUserControl11;
    }
}