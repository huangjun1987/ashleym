﻿using System.Data.SqlClient;

namespace Solvent.DAL
{
    /// <summary>
    /// This class models a connection to the database.
    /// </summary>
    public class SolventDBConnection
    {
        /// <summary>
        /// Creates and returns new connection to database
        /// </summary>
        /// <returns>SQL Connection object</returns>
        public static SqlConnection GetConnection()
        {
            string connectionString =
                "Data Source=tcp:solvent.database.windows.net;Initial Catalog=Solvent;" +
                "User Id = ashley; Password = Password123!";
            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }
    }
}
