﻿using Solvent.Model;
using Solvent.Util;
using System;
using System.Data.SqlClient;

namespace Solvent.DAL
{
    /// <summary>
    /// This class is the DAL for the User
    /// </summary>
    class UserDAL
    {
        /// <summary>
        /// This method checks the entered data against the database.
        /// </summary>
        /// <param name="username">String username entered</param>
        /// <param name="password">String password entered</param>
        /// <returns></returns>
        public LoginResult Login(String username, String password)
        {
            var encryptedPassword = Security.Encrypt(password);
            string selectStatement =
                @"SELECT *
                FROM dbo.[User]
                WHERE Username = @username and Password = @password COLLATE Latin1_General_CS_AS";
            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();
                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@username", username);
                    selectCommand.Parameters.AddWithValue("@password", encryptedPassword);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LoginResult loginResult = new LoginResult
                            {
                                UserID = reader.GetInt32(0),
                                Name = reader["Name"].ToString()
                            };
                            return loginResult;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// This method returns the details for the given user.
        /// </summary>
        /// <param name="userID">The user whose details will be returned.</param>
        /// <returns>The details for the given user.</returns>
        public SolventUser GetUserByUserID(int userID)
        {
            SolventUser user = new SolventUser();
            string selectStatement =
                "SELECT UserID, DOB, DesiredRetirementAge, DesiredRetirementIncome, AnnualIncome, [401kContribution] AS UserContribution, SpouseDOB, SpouseAnnualIncome, Spouse401kContribution " +
                "FROM [User] " +
                "WHERE UserID=@userID; ";
            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();
                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@userID", userID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user.UserID = int.Parse(reader["UserID"].ToString());
                            user.DOB = (DateTime)reader["DOB"];
                            user.DesiredRetirementAge = int.Parse(reader["DesiredRetirementAge"].ToString());
                            user.DesiredRetirementIncome = decimal.Parse(reader["DesiredRetirementIncome"].ToString());
                            user.AnnualIncome = decimal.Parse(reader["AnnualIncome"].ToString());
                            user.UserContribution = decimal.Parse(reader["UserContribution"].ToString());
                            if (!string.IsNullOrEmpty(reader["SpouseDOB"].ToString()))
                            {
                                user.SpouseDOB = (DateTime)reader["SpouseDOB"];
                            }
                            else
                            {
                                user.SpouseDOB = DateTime.Today;
                            }
                            if (decimal.TryParse(reader["SpouseAnnualIncome"].ToString(), out decimal spouseIncomeResult)) {
                                user.SpouseAnnualIncome = spouseIncomeResult;
                            }
                            else
                            {
                                user.SpouseAnnualIncome = 0.0m;
                            }
                            if (decimal.TryParse(reader["Spouse401kContribution"].ToString(), out decimal spouseContributionResult)) {
                                user.SpouseContribution = spouseContributionResult;
                            }
                            else
                            {
                                user.SpouseContribution = 0.0m;
                            }
                        }
                    }
                }
            }
            return user;
        }

        /// <summary>
        /// This method updates the specified user in the database.
        /// </summary>
        /// <param name="oldUser">The user to be updated in the database.</param>
        /// <param name="newUser">The new details of the user.</param>
        public bool UpdateUser(SolventUser oldUser, SolventUser newUser)
        {
            string updateStatement =
                        "UPDATE [User] " +
                        "SET DOB=@DOB, DesiredRetirementAge=@desiredRetirementAge, DesiredRetirementIncome=@desiredRetirementIncome, AnnualIncome=@annualIncome, " +
                            "[401kContribution]=@userContribution, SpouseDOB=@spouseDOB, SpouseAnnualIncome=@spouseAnnualIncome, Spouse401kContribution=@spouseContribution " +
                        "WHERE UserID=@userID AND DOB=@oldDOB AND DesiredRetirementAge=@oldDesiredRetirementAge AND DesiredRetirementIncome=@oldDesiredRetirementIncome " +
                            "AND AnnualIncome=@oldAnnualIncome AND [401kContribution]=@oldUserContribution; ";
            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();
                using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection))
                {
                    updateCommand.Parameters.AddWithValue("@userID", oldUser.UserID);
                    updateCommand.Parameters.AddWithValue("@oldDOB", oldUser.DOB.ToString("yyyy'-'MM'-'dd"));
                    updateCommand.Parameters.AddWithValue("@oldDesiredRetirementAge", oldUser.DesiredRetirementAge);
                    updateCommand.Parameters.AddWithValue("@oldDesiredRetirementIncome", oldUser.DesiredRetirementIncome);
                    updateCommand.Parameters.AddWithValue("@oldAnnualIncome", oldUser.AnnualIncome);
                    updateCommand.Parameters.AddWithValue("@oldUserContribution", oldUser.UserContribution);
                    updateCommand.Parameters.AddWithValue("@DOB", newUser.DOB.ToString("yyyy'-'MM'-'dd"));
                    updateCommand.Parameters.AddWithValue("@desiredRetirementAge", newUser.DesiredRetirementAge);
                    updateCommand.Parameters.AddWithValue("@desiredRetirementIncome", newUser.DesiredRetirementIncome);
                    updateCommand.Parameters.AddWithValue("@annualIncome", newUser.AnnualIncome);
                    updateCommand.Parameters.AddWithValue("@userContribution", newUser.UserContribution);
                    updateCommand.Parameters.AddWithValue("@spouseDOB", newUser.SpouseDOB);
                    updateCommand.Parameters.AddWithValue("@spouseAnnualIncome", newUser.SpouseAnnualIncome);
                    updateCommand.Parameters.AddWithValue("@spouseContribution", newUser.SpouseContribution);
                    int success = updateCommand.ExecuteNonQuery();
                    if (success > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}

