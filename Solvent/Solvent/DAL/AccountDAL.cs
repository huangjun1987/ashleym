﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Solvent.DAL
{
    /// <summary>
    /// DAL class to mediate between Accounts table in the database and Controller.
    /// </summary>
    class AccountDAL
    {
        /// <summary>
        /// This method returns the list of retirement accounts for the given user.
        /// </summary>
        /// <param name="userID">The user whose retirement accounts will be returned.</param>
        /// <returns>The list of retirement accounts for the given user.</returns>
        public List<Solvent.Model.AccountDTO> GetRetirementAccountsByUserID(int userID)
        {
            List<Solvent.Model.AccountDTO> accountList = new List<Solvent.Model.AccountDTO>();
            string selectStatement =
                "SELECT Account.AccountID AS ID, Account.AccountTypeID AS TypeID, AccountType.Name AS TypeName, Account.UserID AS UserID, Account.IsTraditional AS Tax, Account.Owner AS Owner, Account.Balance AS Balance " +
                "FROM Account " +
                "LEFT JOIN AccountType ON Account.AccountTypeID=AccountType.AccountTypeID " +
                "WHERE (Account.AccountTypeID=1000 OR Account.AccountTypeID=1001) AND Account.UserID=@userID " +
                "ORDER BY Account.AccountID ASC; ";
            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();
                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@userID", userID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Solvent.Model.AccountDTO account = new Solvent.Model.AccountDTO
                            {
                                AccountID = int.Parse(reader["ID"].ToString()),
                                AccountTypeID = int.Parse(reader["TypeID"].ToString()),
                                AccountTypeName = reader["TypeName"].ToString(),
                                UserID = int.Parse(reader["UserID"].ToString()),
                                TraditionalOrRoth = (bool)reader["Tax"] ? "Traditional" : "Roth",
                                Owner = (bool)reader["Owner"] ? "User" : "Spouse",
                                Balance = decimal.Parse(reader["Balance"].ToString())
                            };
                            accountList.Add(account);
                        }
                    }
                }
            }
            return accountList;
        }

        /// <summary>
        /// This method returns the list of all accountid and corresponding type for the given user.
        /// </summary>
        /// <param name="userID">The user whose accounts will be returned.</param>
        /// <returns>The list of accounts for the given user.</returns>
        public List<Solvent.Model.AccountDTO> GetAccountsByUserID(int userID)
        {
            List<Solvent.Model.AccountDTO> accountList = new List<Solvent.Model.AccountDTO>();
            string selectStatement =
                "SELECT Account.AccountID AS ID,  AccountType.Name AS TypeName " +
                "FROM Account " +
                "LEFT JOIN AccountType ON Account.AccountTypeID=AccountType.AccountTypeID " +
                "WHERE Account.UserID=@userID " +
                "ORDER BY Account.AccountID ASC; ";
            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();
                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@userID", userID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Solvent.Model.AccountDTO account = new Solvent.Model.AccountDTO
                            {
                                AccountID = int.Parse(reader["ID"].ToString()),
                                AccountTypeName = reader["TypeName"].ToString()
                            };
                            accountList.Add(account);
                        }
                    }
                }
            }
            return accountList;
        }

        /// <summary>
        /// This method returns all available account types.
        /// </summary>
        /// <returns>The list of account types.</returns>
        public List<Solvent.Model.AccountTypeDTO> GetAccountTypes()
        {
            List<Solvent.Model.AccountTypeDTO> accountTypeList = new List<Solvent.Model.AccountTypeDTO>();

            string selectStatement =
                "SELECT AccountTypeID AS ID, Name FROM AccountType ";
            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();
                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Solvent.Model.AccountTypeDTO accountType = new Solvent.Model.AccountTypeDTO
                            {
                                AccountTypeID = int.Parse(reader["ID"].ToString()),
                                AccountTypeName = reader["Name"].ToString()
                            };
                            accountTypeList.Add(accountType);
                        }
                    }
                }
            }
            return accountTypeList;
        }

        /// <summary>
        /// Adds Account to user
        /// </summary>
        /// <param name="Account">Account Object</param>
        /// <returns>Account Id of the created account</returns>
        public int AddAccount(Solvent.Model.AccountDTO account)
        {
            string sqlStatement = "INSERT INTO Account (AccountTypeId, userId, isTraditional, isAsset, owner, balance) " +
                "VALUES (@AccountTypeID, @UserID, @TraditionalOrRoth, @IsAsset, @Owner, @Balance); SELECT SCOPE_IDENTITY() ";

            int accountId = 0;

            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand sqlCommand = new SqlCommand(sqlStatement, connection))
                {
                    sqlCommand.Parameters.AddWithValue("@AccountTypeID", account.AccountTypeID);
                    sqlCommand.Parameters.AddWithValue("@UserID", account.UserID);
                    sqlCommand.Parameters.AddWithValue("@TraditionalOrRoth", account.TraditionalOrRoth == "Traditional"?1:0);
                    sqlCommand.Parameters.AddWithValue("@IsAsset", account.IsAsset);
                    sqlCommand.Parameters.AddWithValue("@Owner", account.Owner == "Self"? 1:0);
                    sqlCommand.Parameters.AddWithValue("@Balance", account.Balance);
                    accountId = Convert.ToInt32(sqlCommand.ExecuteScalar());

                }
            }

            return accountId;
        }
    }
}
