﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Solvent.DAL
{
    /// <summary>
    /// DAL class to mediate between Transaction table in the database and Controller.
    /// </summary>
    class TransactionDAL
    {

        /// <summary>
        /// This method returns all available category types.
        /// </summary>
        /// <returns>The list of category types.</returns>
        public List<Solvent.Model.CategoryType> GetCategoryTypes()
        {
            List<Solvent.Model.CategoryType> categoryTypeList = new List<Solvent.Model.CategoryType>();

            string selectStatement =
                "SELECT CategoryID AS ID, Name FROM Category ";
            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();
                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Solvent.Model.CategoryType categoryType = new Solvent.Model.CategoryType
                            {
                                CategoryId = int.Parse(reader["ID"].ToString()),
                                CategoryName = reader["Name"].ToString()
                            };
                            categoryTypeList.Add(categoryType);
                        }
                    }
                }
            }
            return categoryTypeList;
        }

        /// <summary>
        /// Adds a new transaction
        /// </summary>
        /// <param name="Transaction">Transaction Object</param>
        /// <returns>Transaction Id of the created transaction</returns>
        public int AddTransaction(Solvent.Model.Transaction transaction)
        {
            string sqlStatement = "INSERT INTO \"Transaction\" (isIncome, amount, date, categoryId, accountId) " +
                "VALUES (@isIncome, @amount, @date, @categoryId, @accountId); SELECT SCOPE_IDENTITY() ";

            int transactionId = 0;

            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();

                using (SqlCommand sqlCommand = new SqlCommand(sqlStatement, connection))
                {
                    sqlCommand.Parameters.AddWithValue("@isIncome", transaction.IsIncome);
                    sqlCommand.Parameters.AddWithValue("@amount", transaction.Amount);
                    sqlCommand.Parameters.AddWithValue("@date", transaction.Date);
                    sqlCommand.Parameters.AddWithValue("@categoryId", transaction.CategoryId);
                    sqlCommand.Parameters.AddWithValue("@accountId", transaction.AccountId);
                    transactionId = Convert.ToInt32(sqlCommand.ExecuteScalar());

                }
            }

            return transactionId;
        }

        /// <summary>
        /// This method returns the list of transactions for the given user.
        /// </summary>
        /// <param name="userID">The user whose transactions will be returned.</param>
        /// <returns>The list of transactions for the given user.</returns>
        public List<Solvent.Model.Transaction> GetTransactionsByUserID(int userID)
        {
            List<Solvent.Model.Transaction> transactionList = new List<Solvent.Model.Transaction>();
            string selectStatement =
                "SELECT t.IsIncome, t.Amount, t.Date, Category.Name AS CategoryName, AccountType.Name as AccountTypeName " +
                "FROM \"Transaction\" t " +
                "LEFT JOIN Category ON t.CategoryId=Category.CategoryId " +
                "LEFT JOIN Account ON Account.AccountId=t.AccountId " +
                "LEFT JOIN AccountType ON Account.AccountTypeID=AccountType.AccountTypeID " +
                "WHERE Account.UserID=@userID " +
                "ORDER BY t.Date DESC; ";
            using (SqlConnection connection = SolventDBConnection.GetConnection())
            {
                connection.Open();
                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                    selectCommand.Parameters.AddWithValue("@userID", userID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Solvent.Model.Transaction transaction = new Solvent.Model.Transaction
                            {
                                IncomeOrExpense = reader["IsIncome"].ToString() == "true" ? "Income" : "Expense",
                                Amount = decimal.Parse(reader["Amount"].ToString()),
                                Date = (DateTime)reader["Date"],
                                CategoryName = reader["CategoryName"].ToString(),
                                AccountName = reader["AccountTypeName"].ToString()
                            };
                            transactionList.Add(transaction);
                        }
                    }
                }
            }
            return transactionList;
        }

    }
}
