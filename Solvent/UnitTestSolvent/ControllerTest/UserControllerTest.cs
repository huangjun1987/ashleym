﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestSolvent.Controller
{
    [TestClass]
    public class UserControllerTest
    {
        private Solvent.Controller.UserController userController;

        [TestInitialize]
        public void TestInitialize()
        {
            this.userController = new Solvent.Controller.UserController();
        }

        [TestMethod]
        public void TestCalculateAnnualRetirementIncomeNoSpouse()
        {
            Solvent.Model.SolventUser testUser = new Solvent.Model.SolventUser
            {
                UserID = 1000,
                DOB = new DateTime(1995, 5, 16),
                DesiredRetirementAge = 56,
                DesiredRetirementIncome = 2000000.00m,
                AnnualIncome = 54000.00m,
                UserContribution = 5.00m,
                SpouseDOB = DateTime.Today,
                SpouseAnnualIncome = 0.00m,
                SpouseContribution = 0.00m
            };
            Assert.AreEqual(124478.80m, Math.Round(this.userController.CalculateAnnualRetirementIncome(testUser, 95000.00m, 0.08m), 2));
        }

        [TestMethod]
        public void TestCalculateAnnualRetirementIncomeWithSpouse()
        {
            Solvent.Model.SolventUser testUser = new Solvent.Model.SolventUser
            {
                UserID = 1000,
                DOB = new DateTime(1995, 5, 16),
                DesiredRetirementAge = 56,
                DesiredRetirementIncome = 2000000.00m,
                AnnualIncome = 54000.00m,
                UserContribution = 5.00m,
                SpouseDOB = DateTime.Today,
                SpouseAnnualIncome = 54000.00m,
                SpouseContribution = 7.00m
            };
            Assert.AreEqual(166983.01m, Math.Round(this.userController.CalculateAnnualRetirementIncome(testUser, 95000.00m, 0.08m), 2));
        }

        [TestCleanup]
        public void TestCleanup()
        {
            this.userController = null;
        }
    }
}
