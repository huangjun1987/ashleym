﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestSolvent.Controller
{
    /// <summary>
    /// Tests for Register
    /// </summary>
    [TestClass]
    public class RegisterControllerTest
    {
        private Solvent.Controller.RegisterController registerController;

        [TestInitialize]
        public void TestInitialize()
        {
            this.registerController = new Solvent.Controller.RegisterController();
        }

    /*    [TestMethod]
        public void Register_Should_Success_With_Valid_Username_and_Password_and_Name()
        {
            var username = $"jDoe{DateTime.Now.Year}{DateTime.Now.Month}{DateTime.Now.Day}{DateTime.Now.Hour}{DateTime.Now.Minute}{DateTime.Now.Second}{DateTime.Now.Millisecond}";
            var password = "test";
            var name = "Joe Doe";
            var result = registerController.AddUser(username, password, name);

            Assert.IsNotNull(result);
        } */

        [TestMethod]
        public void Register_Should_Fail_With_Duplicate_Username()
        {
            var username = "jDoe";
            var password = "test1";
            var name = "Joe Doe";
            var result = registerController.AddUser(username, password, name);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void Update_Should_Fail_With_Duplicate_Username()
        {
            var userID = 1001;
            var username = "jDoe";
            var password = "test";
            var name = "Joe Doe";
            var result = registerController.UpdateUser(userID, username, password, name);

            Assert.IsNull(result);
        }



        [TestCleanup]
        public void TestCleanup()
        {
            this.registerController = null;
        }
    }
}
