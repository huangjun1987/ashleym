﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solvent.Model;

namespace UnitTestSolvent.Controller
{
    /// <summary>
    /// Tests for Login
    /// </summary>
    [TestClass]
    public class LoginControllerTest
    {
        private Solvent.Controller.LoginController loginController;

        [TestInitialize]
        public void TestInitialize()
        {
            this.loginController = new Solvent.Controller.LoginController();
        }

        [TestMethod]
        public void Login_Should_Success_With_Valid_Username_and_Password()
        {
            var username = "jDoe";
            var password = "test";
            LoginResult result = loginController.Login(username, password);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Login_Should_Fail_With_Valid_Username_Invalid_Password()
        {
            var username = "jDoe";
            var password = "test1";
            LoginResult result = loginController.Login(username, password);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void Login_Should_Fail_With_InValid_Username_Valid_Password()
        {
            var username = "jDoe1";
            var password = "test";
            LoginResult result = loginController.Login(username, password);

            Assert.IsNull(result);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            this.loginController = null;
        }
    }
}
